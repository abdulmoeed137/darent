<?php

/**
 * StartingCities Model
 *
 * StartingCities Model manages StartingCities operation.
 *
 * @category   StartingCities
 * @package    darent
 * @author     Techvillage Dev Team
 * @copyright  2020 Techvillage
 * @license
 * @version    2.7
 * @link       http://techvill.net
 * @since      Version 1.3
 * @deprecated None
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;


class StartingCities extends Model
{
    protected $table   = 'starting_cities';
    public $timestamps = false;
    public $appends    = ['image_url'];

	public static function getAll()
	{
		$data = parent::where('status', 'Active')->select('name', 'image', 'name_ar')->get();
	
// 		echo "<pre>";
// 		print_r($data);
// 		exit;
		return $data;
	}

    public function getImageUrlAttribute()
    {
        return url('/').'/public/front/images/starting_cities/'.$this->attributes['image'];
    }
}
