<?php

namespace App\DataTables;

use App\Models\ContactUs;
use Yajra\DataTables\Services\DataTable;

class ContactUsDataTable extends DataTable
{
    public function ajax()
    {
        return datatables()
            ->eloquent($this->query())
            ->addColumn('action', function ($listing) {

                return '<a href="' . url('admin/status-change-contact/' . $listing->id.'/'.$listing->status) . '" class="btn btn-xs btn-info status-change-confirmation"><i class="glyphicon glyphicon-off"></i></a>';
            })
            ->addColumn('status', function ($listing) {
                   $labelclass = $listing->status == 'close' ? 'label-danger' : 'label-success'; 
                return '<span class="label  '.$labelclass.'">'.$listing->status.'</span>';
            })
            ->rawColumns(['action', 'status'])
            ->make(true);
    }

    public function query()
    {
        $query = ContactUs::select();
        return $this->applyScopes($query);
    }

    public function html()
    {
        return $this->builder()
            ->addColumn(['data' => 'id',   'name' => 'id',   'title' => 'id'])
            ->addColumn(['data' => 'name',   'name' => 'name',   'title' => 'Name'])
            ->addColumn(['data' => 'phone',    'name' => 'phone',    'title' => 'Phone'])
            ->addColumn(['data' => 'email', 'name' => 'email', 'title' => 'Email'])
            ->addColumn(['data' => 'message', 'name' => 'message', 'title' => 'Message'])
            ->addColumn(['data' => 'status', 'name' => 'status', 'title' => 'Status'])
            ->addColumn(['data' => 'action', 'name' => 'action',       'title' => 'Action', 'orderable' => false, 'searchable' => false])
             ->parameters(dataTableOptions());
    }


    protected function filename()
    {
        return 'membersdatatables_' . time();
    }
}
