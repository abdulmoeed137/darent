<?php
namespace App\Http\Controllers;


use Auth;
use Illuminate\Support\Facades\Validator;
use Session;
use DateTime;

use App\Http\{
    Requests,
    Helpers\Common,
    Controllers\EmailController
};

use App\Models\{
    Bank,
    BankDate,
    Payouts,
    Currency,
    Country,
    Settings,
    Payment,
    Photo,
    Withdraw,
    Messages,
    Wallet,
    Properties,
    Bookings,
    PaymentMethods,
    BookingDetails,
    PropertyDates,
    PropertyPrice,
    PaymentResponse,
    PropertyFees};
use Omnipay\Omnipay;
use Illuminate\Http\Request;


class PaymentController extends Controller{

    public  $myfatoorah_apiURL;
    public  $myfatoorah_apikey;

    public  $hyperpay_apiURL;
    public  $hyperpay_entity_id;
    public  $hyperpay_access_token;

    public  $paymentMethods;
    public  $paymentMethodId;
    public  $callbackUrl;
    public  $errorUrl;


    protected $helper;

    public function __construct()
    {
        /* ------------------------ Configurations ---------------------------------- */

        //Test
        if(env('MYFATOORAH_MODE') == 'test'){
            $this->myfatoorah_apiURL = 'https://apitest.myfatoorah.com/';
            $this->myfatoorah_apikey = env('MYFATOORAH_API_KEY_TEST'); //Test token value to be placed here: https://myfatoorah.readme.io/docs/test-token

        }else{
            $this->myfatoorah_apiURL = 'https://api.myfatoorah.com/';
            $this->myfatoorah_apikey = env('MYFATOORAH_API_KEY_LIVE'); //Live token value to be placed here: https://myfatoorah.readme.io/docs/live-token
        }

        if(env('HYPERPAY_MODE') == 'test'){
            $this->hyperpay_apiURL = 'https://eu-test.oppwa.com/';
            $this->hyperpay_entity_id = env('HYPERPAY_ENTITY_ID');
            $this->hyperpay_access_token = env('HYPERPAY_ACCESS_TOKEN');

        }else{
            $this->hyperpay_apiURL = 'https://oppwa.com/';
            $this->hyperpay_entity_id = env('HYPERPAY_ENTITY_ID');
            $this->hyperpay_access_token = env('HYPERPAY_ACCESS_TOKEN');
        }


        $this->callbackUrl = route('paymentCallback');
        $this->errorUrl = route('paymentCancel');
        $this->paymentMethodId = 9;
        $this->helper = new Common;

    }

    public function index(Request $request){

        if ($request->isMethod('post')) {
                Session::put('payment_property_id', $request->id);
                Session::put('payment_property_slug', $request->property_slug);
                Session::put('payment_checkin', $request->checkin);
                Session::put('payment_checkout', $request->checkout);
                Session::put('payment_number_of_guests', $request->number_of_guests);
                Session::put('payment_booking_type', $request->booking_type);
                Session::put('payment_booking_status', $request->booking_status);
                Session::put('payment_booking_id', $request->booking_id);

                $id               = Session::get('payment_property_id');
                $property_slug    = Session::get('payment_property_slug');
                $checkin          = Session::get('payment_checkin');
                $checkout         = Session::get('payment_checkout');
                $number_of_guests = Session::get('payment_number_of_guests');
                $booking_type     = Session::get('payment_booking_type');
                $booking_status   = Session::get('payment_booking_status');
                $booking_id       = Session::get('payment_booking_id');

        } else {
                $id               = Session::get('payment_property_id');
                $property_slug    = Session::get('payment_property_slug');
                $number_of_guests = Session::get('payment_number_of_guests');
                $checkin          = Session::get('payment_checkin');
                $checkout         = Session::get('payment_checkout');
                $booking_type     = Session::get('payment_booking_type');
                $booking_status   = Session::get('payment_booking_status');
        }

        if ( !$request->isMethod('post') && ! $checkin) {
            return redirect('properties/'.$request->property_slug);
        }


            $data['result']           = Properties::find($id);
            $data['property_id']      = $id;
            $data['number_of_guests'] = $number_of_guests;
            $data['booking_type']     = $booking_type;
            $data['checkin']          = setDateForDb($checkin);
            $data['checkout']         = setDateForDb($checkout);
            $data['status']           = $booking_status ?? "";
            $data['booking_id']       = $booking_id ?? "";

            $from                     = new DateTime(setDateForDb($checkin));
            $to                       = new DateTime(setDateForDb($checkout));
            $data['nights']           = $to->diff($from)->format("%a");

            $data['price_list']    = json_decode($this->helper->getPrice($data['property_id'], $data['checkin'], $data['checkout'], $data['number_of_guests']));
            Session::put('payment_price_list', $data['price_list']);

        if (((isset($data['price_list']->status) && ! empty($data['price_list']->status)) ? $data['price_list']->status : '') == 'Not available') {
            $this->helper->one_time_message('success', trans('messages.error.property_available_error'));
            return redirect('properties/'.$property_slug);
        }

        $data['currencyDefault']  = $currencyDefault = Currency::getAll()->firstWhere('default',1);

        $data['price_eur']        = numberFormat($this->helper->convert_currency($data['result']->property_price->code, $currencyDefault->code, $data['price_list']->total),2);
        $data['price_rate']       = $this->helper->currency_rate($data['result']->property_price->currency_code, $this->helper->getCurrentCurrencycode());
        $data['country']          = Country::where('short_name','SA')->pluck('name', 'short_name');
        $data['title']            = 'Pay for your reservation';
        $data['currentCurrency'] = $this->helper->getCurrentCurrency();
        

        // $mr_id = 'DR'.date('dmyhis');
//////
        $price_list = json_decode($this->helper->getPrice($data['property_id'], $data['checkin'], $data['checkout'], $data['number_of_guests']));

        $datastore = [
            'property_id'      => $data['property_id'],
            'checkin'          => $data['checkin'],
            'checkout'         => $data['checkout'],
            'number_of_guests' => $data['number_of_guests'],
            // 'transaction_id'   => $request->paymentid_hyperpay,
            'price_list'       => $price_list,
            'paymode'          => '',
            'first_name'       => auth()->user()->first_name,
            'last_name'        => auth()->user()->last_name,
            'postal_code'      => '',
            'country'          => '',
            'status'           => 'Unpaid',
            // 'message_to_host'  => $request->message_to_host
        ];


        $code = $this->store($datastore);
        
        Session::put('bookingcode', $code);




        // $mrid = 'TR-'.Auth::user()->id.'-'.time();
        $billingcity = $data['result']->property_address->city;
        $billingpostcode = 23434;
        $customeremail = auth()->user()->email;
        // $billingaddress = 'Jameel Square 2091 Prince Mohammed Bin Abdulaziz St, Al Andalus, Jeddah 23326';
        
        //HYPERPAY PAYMENT FORM (CARD)
        $data['apikey'] = $this->hyperpay_apiURL;
            // $amount =  round($data['price_list']->total, 2);
            $amount = number_format($data['price_list']->total, 2);
            $amount = str_replace( ',', '', $amount);
            $url    = $this->hyperpay_apiURL."v1/checkouts";
            $dataa  = "entityId=".$this->hyperpay_entity_id.
                        "&amount=$amount".
                        "&currency=SAR".
                        "&paymentType=DB".
                        "&merchantTransactionId=$code".
                        "&customer.email=$customeremail".
                        "&billing.city=$billingcity".
                        "&billing.street1=JEDDAH".
                        "&billing.state=JEDDAH".
                        "&billing.country=SA".
                        "&billing.postcode=$billingpostcode".
                        "&customer.givenName=moaid".
                        "&customer.surname=hassan";

                        // dd($dataa);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Authorization:Bearer '.$this->hyperpay_access_token));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataa);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);// this should be set to true in production
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            
            $data['responseData'] = json_decode(curl_exec($ch));
            if(curl_errno($ch)) {
                return curl_error($ch);
            }
            curl_close($ch);
            // dd($data['responseData']);
            return view('payment.payment', $data);

    
    }




    
    /* ------------------------ Functions --------------------------------------- */

    public function createBooking(Request $request)
    {
        // dd('createbooking', $request->all());
            $currencyDefault    = Currency::getAll()->where('default', 1)->first();
            $price_list         = json_decode($this->helper->getPrice($request->property_id, $request->checkin, $request->checkout, $request->number_of_guests));
            $amount             = round($this->helper->convert_currency($request->currency, $currencyDefault->code, $price_list->total),2);
            $country            = $request->payment_country;
            $message_to_host    = $request->message_to_host;

            Session::put('amount', $amount);
            Session::put('payment_country', $country);
            Session::put('message_to_host_'.Auth::user()->id, $message_to_host);
            Session::save();

        if($request->payment_method == 'myfatoorah'){

                /* ------------------------ Call InitiatePayment Endpoint ------------------- */
            //Fill POST fields array
            $ipPostFields = ['InvoiceAmount' => $amount, 'CurrencyIso' => 'KWD'];
            
            //Call endpoint

            $this->paymentMethods = $this->initiatePayment($this->myfatoorah_apiURL, $this->myfatoorah_apikey, $ipPostFields);
            
            /* ------------------------ Call ExecutePayment Endpoint -------------------- */

            //Fill POST fields array
            $postFields = [
                //Fill required data
                'paymentMethodId' => $this->paymentMethodId,
                'InvoiceValue'    =>  $amount,
                'CallBackUrl'     => $this->callbackUrl,
                'ErrorUrl'        => $this->errorUrl,   
                    //Fill optional data
                    'CustomerName'       => auth()->user()->FullName,
                    // 'DisplayCurrencyIso' => 'KWD',
                    //'MobileCountryCode'  => '+965',
                    //'CustomerMobile'     => '1234567890',
                    'CustomerEmail'      => auth()->user()->email,

            ];
            
            //Call endpoint
            $data = $this->executePayment($this->myfatoorah_apiURL, $this->myfatoorah_apikey, $postFields);
            
            //You can save payment data in database as per your needs
            $invoiceId  = $data->InvoiceId;
            $paymentURL = $data->PaymentURL;
            
            
            /* ------------------------ Call DirectPayment Endpoint --------------------- */
            //Fill POST fields array
                $cardInfo = [
                    'PaymentType' => 'card',
                    'Bypass3DS'   => false,
                    'Card'        => [
                        'Number'         => $request->cardNumber,
                        'ExpiryMonth'    => $request->ExpiryMonth,
                        'ExpiryYear'     => $request->ExpiryYear,
                        'SecurityCode'   => $request->cvv,
                        'CardHolderName' => auth()->user()->FullName
                    ]
                ];
                
                //Call endpoint
                $directData = $this->directPayment($paymentURL, $this->myfatoorah_apikey, $cardInfo);
                
                //You can save payment data in database as per your needs
                $paymentId   = $directData->PaymentId;
                $paymentLink = $directData->PaymentURL;

                $data = [
                    'property_id'      => $request->property_id,
                    'checkin'          => $request->checkin,
                    'checkout'         => $request->checkout,
                    'number_of_guests' => $request->number_of_guests,
                    'transaction_id'   => $paymentId,
                    'price_list'       => $price_list,
                    'paymode'          => '',
                    'first_name'       => auth()->user()->first_name,
                    'last_name'        => auth()->user()->last_name,
                    'postal_code'      => '',
                    'country'          => '',
                    'status'           => 'Unpaid',
                    'message_to_host'  => $request->message_to_host
                ];

                $code = $this->store($data);
                Session::put('bookingcode', $code);
                Session::put('message_host', $request->message_to_host);

                return redirect()->to($paymentLink);
        }
        else {
            $data = [
                'property_id'      => $request->property_id,
                'checkin'          => $request->checkin,
                'checkout'         => $request->checkout,
                'number_of_guests' => $request->number_of_guests,
                'transaction_id'   => '',
                'price_list'       => $price_list,
                'paymode'          => '',
                'first_name'       => $request->first_name,
                'last_name'        => $request->last_name,
                'postal_code'      => '',
                'country'          => '',
                'status'          => 'Pending',

                'message_to_host'  => $message_to_host
            ];

            $code = $this->store($data);
            $this->helper->one_time_message('success', trans('messages.booking_request.request_has_sent'));
            return redirect('booking/requested?code='.$code);
        }
    }

    // public function data_store_ajax(Request $request){
    //     // dd($request->all());
    //     $price_list = json_decode($this->helper->getPrice($request->property_id, $request->checkin, $request->checkout, $request->number_of_guests));

    //     $data = [
    //         'property_id'      => $request->property_id,
    //         'checkin'          => $request->checkin,
    //         'checkout'         => $request->checkout,
    //         'number_of_guests' => $request->number_of_guests,
    //         'transaction_id'   => $request->paymentid_hyperpay,
    //         'price_list'       => $price_list,
    //         'paymode'          => '',
    //         'first_name'       => auth()->user()->first_name,
    //         'last_name'        => auth()->user()->last_name,
    //         'postal_code'      => '',
    //         'country'          => '',
    //         'status'           => 'Unpaid',
    //         'message_to_host'  => $request->message_to_host
    //     ];


    //     $code = $this->store($data);
    //     if($code){
    //         Session::put('bookingcode', $code);
    //         return 1;
    //     }else{
    //         return 'Error';
    //     }
    // }
    public function store($data)
    {
        Session::put('pricedata',[$data['price_list']->date_with_price]);
        // foreach (Session::get('pricedata') as $dp) {
        //     foreach($dp as $price){
        //         dd($price->date);
        //     }
        //     dd($dp);
        // }
        // foreach($data['price_list'] as $pricedata){
        //     dd($pricedata);
        // }

        // dd(Session::get('data'));
        // $property_id = $data['property_id'];
        // Session::put('property_id',$property_id);
        // dd(Session::get('property_id'));
        
        $currencyDefault = Currency::getAll()->where('default', 1)->first();
        $booking = new Bookings;
        $booking->property_id       = $data['property_id'];
        $booking->host_id           = properties::find($data['property_id'])->host_id;
        $booking->user_id           = Auth::user()->id;
        $booking->start_date        = setDateForDb($data['checkin']);
        $checkinDate                = onlyFormat($booking->start_date);
        $booking->end_date          = setDateForDb($data['checkout']);
        $booking->guest             = $data['number_of_guests'];
        $booking->attachment        = $data['attachment'] ?? null;
        $booking->total_night       = $data['price_list']->total_nights;
        $booking->per_night         = $this->helper->convert_currency('', $currencyDefault->code, $data['price_list']->property_price);
        Session::put('checkinDate', $checkinDate);

        $booking->custom_price_dates= isset($data['price_list']->different_price_dates_default_curr) ? json_encode($data['price_list']->different_price_dates_default_curr) : null ;

        $booking->base_price        = $this->helper->convert_currency('', $currencyDefault->code, $data['price_list']->subtotal);
        $booking->cleaning_charge   = $this->helper->convert_currency('', $currencyDefault->code, $data['price_list']->cleaning_fee);
        $booking->guest_charge      = $this->helper->convert_currency('', $currencyDefault->code, $data['price_list']->additional_guest);
        $booking->iva_tax           = $this->helper->convert_currency('', $currencyDefault->code, $data['price_list']->iva_tax);
        $booking->accomodation_tax  = $this->helper->convert_currency('', $currencyDefault->code, $data['price_list']->accomodation_tax);
        $booking->security_money    = $this->helper->convert_currency('', $currencyDefault->code, $data['price_list']->security_fee);
        $booking->service_charge    = $service_fee  = $this->helper->convert_currency('', $currencyDefault->code, $data['price_list']->service_fee);
        $booking->host_fee          = $host_fee     = $this->helper->convert_currency('', $currencyDefault->code, $data['price_list']->host_fee);
        $booking->total             = $total_amount = $this->helper->convert_currency('', $currencyDefault->code, $data['price_list']->total);

        $booking->currency_code     = $currencyDefault->code;
        $booking->transaction_id    = $data['transaction_id'] ?? " ";
        $booking->payment_method_id = $data['payment_method_id'] ?? " ";
        $booking->cancellation      = Properties::find($data['property_id'])->cancellation;
        $booking->status            = $data['status'];
        $booking->booking_type      = Session::get('payment_booking_type');

        foreach ($data['price_list']->date_with_price as $key => $value) {
            $allData[$key]['price'] = $this->helper->convert_currency('', $currencyDefault->code, $value->original_price);
            $allData[$key]['date'] = setDateForDb($value->date);
        }

       $booking->date_with_price   = json_encode($allData);
       $booking->save();

        $booking_details['country']  = $data['country'];

        foreach ($booking_details as $key => $value) {
            $booking_details = new BookingDetails;
            $booking_details->booking_id = $booking->id;
            $booking_details->field = $key;
            $booking_details->value = $value;
            $booking_details->save();
        }

        do {
            $code = $this->helper->randomCode(6);
            $check_code = Bookings::where('code', $code)->get();
        } while (empty($check_code));

        $booking_code = Bookings::find($booking->id);

        $booking_code->code = $code;

        if ($booking->booking_type == "instant") {
            $dates = [];
            $propertyCurrencyCode = PropertyPrice::firstWhere('property_id', $data['property_id'])->currency_code;
            foreach ($data['price_list']->date_with_price as $dp) {
                $tmp_date = setDateForDb($dp->date);
                $property_id = $data['property_id'];
                $price = $this->helper->convert_currency($data['price_list']->currency, $propertyCurrencyCode, $dp->original_price);
                Session::put('propertydatedata',['property_id'=>$property_id,'status'=>"Not available",'price'=>$price,'date'=>$tmp_date]);
                
                // $property_data = [
                //     'property_id' = $data['property_id'],
                //     'status'      => 'Not available',
                //     'price'       => $this->helper->convert_currency($data['price_list']->currency, $propertyCurrencyCode, $dp->original_price),
                //     'date'        => $tmp_date
                // ];

                // PropertyDates::updateOrCreate(['property_id' => $data['property_id'], 'date' => $tmp_date], $property_data);
                // if($data['paymode'] == 'Bank') {
                //     array_push($dates, ['booking_id'=> $booking->id, 'date' => $tmp_date ]);
                // }


            }

            if($data['paymode'] == 'Bank'  && count($dates) > 0) {
                BankDate::insert($dates);
            }
        }
        $booking_code->save();


        return $code;
    }


    public function addBookingPaymentInHostWallet($booking)
    {
        $walletBalance = Wallet::where('user_id',$booking->host_id)->first();
        $default_code = Currency::getAll()->firstWhere('default',1)->code;
        $wallet_code = Currency::getAll()->firstWhere('id', $walletBalance->currency_id)->code;
        $balance = ( $walletBalance->balance + $this->helper->convert_currency($default_code, $wallet_code, $booking->total)  - $this->helper->convert_currency($default_code, $wallet_code, $booking->service_charge) - $this->helper->convert_currency($default_code, $wallet_code, $booking->accomodation_tax) - $this->helper->convert_currency($default_code, $wallet_code, $booking->iva_tax) );
        Wallet::where(['user_id' => $booking->host_id])->update(['balance' => $balance]);
    }
    /* Initiate Payment Endpoint Function */
    
    function initiatePayment($myfatoorah_apiURL, $myfatoorah_apikey, $postFields) {
    
        $json = $this->callAPI("$myfatoorah_apiURL/v2/InitiatePayment", $myfatoorah_apikey, $postFields);
        return $json->Data->PaymentMethods;
    }
    
    //------------------------------------------------------------------------------
    /*
     * Execute Payment Endpoint Function 
     */
    
    function executePayment($myfatoorah_apiURL, $myfatoorah_apikey, $postFields) {
    
        $json = $this->callAPI("$myfatoorah_apiURL/v2/ExecutePayment", $myfatoorah_apikey, $postFields);
        return $json->Data;
    }
    
    //------------------------------------------------------------------------------
    /*
     * Direct Payment Endpoint Function 
     */
    
    function directPayment($paymentURL, $myfatoorah_apikey, $postFields) {
    
        $json = $this->callAPI($paymentURL, $myfatoorah_apikey, $postFields);
        return $json->Data;
    }
    
    //------------------------------------------------------------------------------
    /*
     * Call API Endpoint Function
     */
    
    function callAPI($endpointURL, $myfatoorah_apikey, $postFields = [], $requestType = 'POST') {
    
        $curl = curl_init($endpointURL);
        curl_setopt_array($curl, array(
            CURLOPT_CUSTOMREQUEST  => $requestType,
            CURLOPT_POSTFIELDS     => json_encode($postFields),
            CURLOPT_HTTPHEADER     => array("Authorization: Bearer $myfatoorah_apikey", 'Content-Type: application/json'),
            CURLOPT_RETURNTRANSFER => true,
        ));
    
        $response = curl_exec($curl);
        $curlErr  = curl_error($curl);
    
        curl_close($curl);
    
        if ($curlErr) {
            //Curl is not working in your server
            die("Curl Error: $curlErr");
        }
    
        $error = $this->handleError($response);
        if ($error) {
            die("Error: $error");
        }

        return json_decode($response);
    }
    
    //------------------------------------------------------------------------------
    /*
     * Handle Endpoint Errors Function 
     */
    
    function handleError($response) {
    
        $json = json_decode($response);
        if (isset($json->IsSuccess) && $json->IsSuccess == true) {
            return null;
        }
    
        //Check for the errors
        if (isset($json->ValidationErrors) || isset($json->FieldsErrors)) {
            $errorsObj = isset($json->ValidationErrors) ? $json->ValidationErrors : $json->FieldsErrors;
            $blogDatas = array_column($errorsObj, 'Error', 'Name');
    
            $error = implode(', ', array_map(function ($k, $v) {
                        return "$k: $v";
                    }, array_keys($blogDatas), array_values($blogDatas)));
        } else if (isset($json->Data->ErrorMessage)) {
            $error = $json->Data->ErrorMessage;
        }
    
        if (empty($error)) {
            $error = (isset($json->Message)) ? $json->Message : (!empty($response) ? $response : 'API key or API URL is not correct');
        }
    
        return $error;
    }
    /* -------------------------------------------------------------------------- */
    public function callback(Request $request){

    
        $myfatoorah_apiURL = $this->myfatoorah_apiURL.'v2/GetPaymentStatus';
        $postInput = [
            'Key' => $request->paymentId,
            'KeyType' => 'paymentId',
        ];
                
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $myfatoorah_apiURL, [
            'form_params' => $postInput,
            'headers' => [
                'Accept'     => '*/*',
                'Authorization' => 'bearer '.$this->myfatoorah_apikey

            ]
        ]);
     
        $statusCode = $response->getStatusCode();
        $responseBody = json_decode($response->getBody(), true);
        // CURL END


        if ($responseBody['Data']['InvoiceTransactions'][0]['TransactionStatus'] == 'Succss') {

            Bookings::where('transaction_id', $request->paymentId)->update(['status' => "Accepted"]);
            $booking = Bookings::where('transaction_id', $request->paymentId)->first();

            // if ($booking->status == 'Accepted') {
                $payouts = new Payouts;
                $payouts->booking_id     = $booking->id;
                $payouts->user_id        = $booking->host_id;
                $payouts->property_id    = $booking->property_id;
                $payouts->user_type      = 'host';
                $payouts->amount         = $booking->original_host_payout;
                $payouts->penalty_amount = 0;
                $payouts->currency_code  = $booking->currency_code;
                $payouts->status         = 'Future';
                $payouts->save();
            // }
    
            $message = new Messages;
            $message->property_id    = $booking->property_id;
            $message->booking_id     = $booking->id;
            $message->sender_id      = $booking->user_id;
            $message->receiver_id    = $booking->host_id;
            $message->message        = Session::get('message_host') ?? '';
            $message->type_id        = 4;
            $message->read           = 0;
            $message->save();
    
            // $email_controller = new EmailController;
            // $email_controller->booking($booking->id, Session::get('checkinDate'));
            // $email_controller->booking_user($booking->id, Session::get('checkinDate'));
    
    
            if ($booking->status =='Accepted') {
                $companyName = Settings::getAll()->where('type', 'general')->where('name', 'name')->first()->value;
                $instantBookingConfirm = ($companyName.': ' .'Your booking is confirmed from'.' '. $booking->start_date.' '.'to'.' '.$booking->end_date );
                $instantBookingPaymentConfirm =($companyName.' ' .'Your payment is completed for'.' '.$booking->properties->name);
    
                // twilioSendSms(Auth::user()->formatted_phone, $instantBookingConfirm);
                // twilioSendSms(Auth::user()->formatted_phone, $instantBookingPaymentConfirm);
    
            } else {
                // twilioSendSms(Auth::user()->formatted_phone, 'Your booking is initiated, Wait for confirmation');
    
            }
    
            Session::forget('payment_property_id');
            Session::forget('payment_checkin');
            Session::forget('payment_checkout');
            Session::forget('payment_number_of_guests');
            Session::forget('payment_booking_type');
            Session::forget('message_host');
            Session::forget('checkinDate');
    
            clearCache('.calc.property_price');

            $code = Session::get('bookingcode');
            $this->helper->one_time_message('success', trans('messages.booking_request.request_has_sent'));
            return redirect('booking/requested?code='.$code);

        } else {

            $this->helper->one_time_message('error', $responseBody['Message'] . ' ERROR_CODE (' . $responseBody['Data']['InvoiceTransactions'][0]['ErrorCode'] . ')');
            return redirect('payments/book/'.Session::get('payment_property_id'));
        }
    }

    // public function checkout_hyperpay(Request $request){
        
        //     $url = $this->hyperpay_apiURL."v1/checkouts";
        //     $data = "entityId=$this->hyperpay_entity_id".
        //                 "&amount=147" .
        //                 // "&amount=$amount" .
        //                 "&currency=SAR" .
        //                 "&paymentType=DB";
        
        //     $ch = curl_init();
        //     curl_setopt($ch, CURLOPT_URL, $url);
        //     curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        //                    'Authorization:Bearer '.$this->hyperpay_access_token));
        //     curl_setopt($ch, CURLOPT_POST, 1);
        //     curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
        //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //     $responseData = json_decode(curl_exec($ch));
        //     if(curl_errno($ch)) {
        //         return curl_error($ch);
        //     }
        //     curl_close($ch);
        //     $title = 'HyperPay Payment';
        //     return view('payment.payment2', compact('responseData', 'title'));


    // }

    // Session::put('propertydatedata',['property_id'=>$property_id,'status'=>"Not available",'price'=>$price,'date'=>$tmp_date]);

    public function InsertPropertyDate(){
        foreach(Session::get('pricedata') as $pricedata){
            foreach($pricedata as $datefromprice){
                $tmp_date = setDateForDb($datefromprice->date);
                $recordinserted =  PropertyDates::updateOrCreate(['property_id' => Session::get('propertydatedata')['property_id'], 'date' => $tmp_date], Session::get('propertydatedata'));
            }
        // $price = $this->helper->convert_currency($data['price_list']->currency, $propertyCurrencyCode, $dp->original_price);

        }
           
    }
    
    public function callback_hyperpay(Request $request){
        // dd($request->all(), $this->hyperpay_apiURL.$request->resourcePath, 'https://oppwa.com'.$request->resourcePath);



        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://oppwa.com'.$request->resourcePath."?entityId=".$this->hyperpay_entity_id);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer '.$this->hyperpay_access_token));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData_nojson = curl_exec($ch);
        $responseData = json_decode($responseData_nojson);
        // $staticdata = '{"id":"8acda4a28262af470182a7a82508417b","paymentType":"DB","paymentBrand":"MADA","amount":"6.50","currency":"SAR","descriptor":"0942.6478.8439 2552 - Darentapp MADA","merchantTransactionId":"vKwk93-2921","result":{"cvvResponse":"U","code":"000.000.000","description":"Transaction succeeded"},"resultDetails":{"ExtendedDescription":"Approved or completed successfully","clearingInstituteName":"MADA via Postilion","AuthCode":"018763","ConnectorTxID1":"80000977","ConnectorTxID3":"018763|00|0816|201630|020089875908161716300000043232800000577701|800000000977","ConnectorTxID2":"898759|094264788439|094264788439||","AcquirerResponse":"00","CardholderInitiatedTransactionID":"020089875908161716300000043232800000577701"},"card":{"bin":"468540","binCountry":"SA","last4Digits":"7582","holder":"Hanin Alsubaie","expiryMonth":"12","expiryYear":"2024","issuer":{"bank":"AL BILAD BANK","website":"HTTP://WWW.BANKALBILAD.COM","phone":"9662918884"},"type":"DEBIT","level":"CLASSIC","country":"SA","maxPanLength":"16","binType":"PERSONAL","regulatedFlag":"N"},"customer":{"givenName":"moaid","surname":"hassan","email":"bhattiatmeezo@gmail.com","ip":"119.160.3.220","ipCountry":"PK","browserFingerprint":{"value":"0400CIfeAe15Cx8Nf94lis1ztn8oLF24je6Xme5Mcqa9Mhw1CREMQAxMOFDCCZiXdvzwjCYovi3oYQjssAeGpPGhthaRnaVXVWfTSMKGPMntrtJ1SymuXlKryeXc4KIy3xRrsW3b02b0TTBi8vF6RzScsVa1181UhdbF9Yh8oblZWsZ7HVMsBHJtSvhfflTR5XM9lcABhHdx+C/edkQFDysAsy0AH2WgO40osBx6w1tx0mMZz7HtmSQrKuHt39bvxE86eVaYJEF00M2dmwpFJMf4g7vRQ0OHP2J/v+D6E17oa/RVPDYLihDankoqWDbHA4su0+1vm3fbD4qMyn1mduG9axoJuXaDPZFu0NQVwEuhKWzQLiA3NehVVRzOXBOInHV4Wtj3wiT48lfz3wafI/WYHEsTCs2TKRSUB860/YtAXmzRhLqdI1qchaYiJ/+IO4wJwtQHr2DgT3vFL0ZMZD89ca0DEJaWk/hJwKr0Mf+ogWb3WmfvVlSAmW6SpeYio3lSBzd48+sKmYnU9ek3jOXYhB3+1lLYqHZEFDK5ph52E+CvTiZp4nDNSP8xPG8qs4/gzdfxLylYBibsEVBWO8XQIqpa9NvP9z6v2BaaLz8cTWv+7s6WY27c+cyZh0oClnCJyAfUMZ5LKFoA4BbW41P4v+i7kYvuVLUfBnTiiAFeFNMB84+POrI4MlDvgCJ6XflMpM5YbymrVY7rLMnUY2Yy4xZkFqaUZegb+KaePAdj0dC1DOkZ9ybRxHxfYV3WeA0UYsMZmVY5fSN8i9XsGBPeQjfPafZJpaoVoOXUWxljyPAVpcqq3kfmP7OgSQ0Fzflca1WxiZ1a0z3VhghPLNHC/w9dek5r8WH9DtPq/jlyS0tKn4II0n46KK6xraOOHRWfd6dlY8UTIEiJKSIdwO9O5Z/8KrD4hgXHxKZFk5ZzIuYOJpmCsVW0a0lKRgk1OUYQ0dYkyV3YiP+Huyy7bT1zWcSjl2MSyoUuyA/StvbLuQEq9lmU/Fk58KR7krMf6Kh5z3PppKpPEAQSG7KO51ggW21DwzQdN3QgDQSoXA0OuiTnOIhuIkXMvNciPTQaFQzm7ooP8CnjNfBmlBHKop6a4mpNScFK9Riw3BOJ2d5nlZ52+ncQ4qJLHu4cY6Mt9WpRZpQRyqKemuJqTUnBSvUYsNwTidneZ5Wedvp3EOKiSx7uHGOjLfVqUWaUEcqinpri6UZcguSUMhUEMv5tsr1ng2as/SQfA8KyUxsC4ZV6lF7aTEhJl1nWFiSj9PWz+VNRgQdiRD1HsvKK6/pdx/gqwozKfWZ24b1rGgm5doM9kW7Q1BXAS6EpbNAuIDc16FVVHM5cE4icdXjN3pO8EOaVTw=="}},"billing":{"street1":"JEDDAH","city":"jeddah","state":"JEDDAH","postcode":"23434","country":"SA"},"threeDSecure":{"eci":"05","verificationId":"AAACB2g2hgAAAAAAADaGAAAAAAA=","xid":"bXu7AIbKQEiR52UG/GWF4AAAAAA=","paRes":"eJzVetmSo8qS7a+UVT9q12aejmVlG/MgQAwCBG/Mg5jEKPj6JjOraueuPn26+z5cu1dmMgWuCA/38HBfK4CXf3829ZclHcaya79/hf4Ev35J27hLyjb//tW5Ct/Ir//++nIthjTl7DSeh/T1RUvHMczTL2Xy/WuShTgVItE3Ks6wbygBZd9ICkS+wQlIYARCZClGfn19MWgrHd8HvLf+jBEIJskQ+UbAKfkNjWDkG5USx9g0SkMKwbIQw49hPwx7Pez6E34Bfl4eFgxxEbbT60sYPxhZf0VxiCCRF+DH5UuTDjL3SoI/PhRBvAAfshfgr8HG/NYaD5eeZfIa3WaClqOzyZcWBjsiIHoCSr9/vr8Abz1eknBKX2EQhkESwr9AxD8g/B/IYdi7/KV/U0c33XzoxjHwBfgseDnWbjiWdnvFyWPEr6uX9Nl3bXr0OKS/2i/AX7b1YfsKfvoQ2JuCN+nL9fb6MpXNJ5tg8B+HWTD+ArzLX8YpnObx1X8BfrRe4nBZXg+fWAbO4SL/cJDmQpH+5et7l5c0Ll9B7DDq+H0fRdd5N5RT0byZ+nfBC/BmCvAe3NcXu8zbY7Ih/XLsrnb8/rWYpv4fALCu658r8mc35AB8OAKAFHB0SMYy/7evH6PSRG6z7vWFDduuLeOwLvdwOkKupVPRJV9+TfjPVF6tN60QYPHst0PttxhC229vEhCBsK/AJ7P+J9p+N3AYw29jEUJviqw0S9+il35xLPn713/7X+1prszTcfo/MeHn9B8a3LCej8BX91Zpr7sVjSysFmzX9TysJmCtOEcgP/d8AX6ZfbQ/L/avdfnoeCUYpEa89jJFZ6Ij7udoRpnnTgTyoKyhdYbjmpnFJukwYMNJu9KsJgapAd4VbJ4r/oRN5BQIFXxpT7E+PG8e4uenxjbCeDAuanTu102a6gkBl24Ur09GPKNjcd8ft5NqLZoin6ZHP+Z22vLN7eYtuEbZ7oSe4JV8UjGiqw9JVGu1HqLpWaR3hyM9Bo7RUFrwcx7tdz8Q+7lEICq4qe22zadmj6vMAE1H9p0uw+/MHJTk84kHWdU5z2OW2ZBgYFt730/wMYXyEoNj06KCh3K7EaewLBcT0ksCVRJ6HmrB6YW7tFSwQDjUJamAq5PF4T6VuwkmeApvQQfL2zAvHVUu2LMuCaZSz/T37x+L/mmhX87p9hGBGwZSXDiFHy02HaYyO3b/UVQ0WRZgjmVpks3pVWboXDY9gocNYkEnFr0Z3fUpj1kkc7TO5PdHcS9FagUZ2hwFmmNgzRxX1vQ51zRFflVcZ+d1jR5FGnJ4Nl95915fzSu/aCz4IVvXxm3q3bcZJWqtOm6wIhHrJWqEURb0Om6D3oed3LzpewTrvX+z+ghGn8pO10yuuwytaVId9DHM54GjL5EHHTr0WubrPUbcPmri3AT5XLUZSavoVFjB7fg9vuZ64XhQ54TwXbZ/ljGhJsVPbn/z8W2OWGNqpU894R5yvKHRP2ynV9Fpasz3rO43v998vGmM89PvS+BAlXN3A5l/9pFXg4FnftI/asy9dt789Xf+mKx7H0ev6/XH2lwDT4fixskduG6jpp6D/YiRLa+c6SvnLpCLJdZpk2cYk+bynDfotxiaHXu0GVphnbRFQhbBxH2nz+c+Oxl7X1a8R1dSCHXG6TaXtW3EypFDea7F3YzsWQ/Vs+bfcZ9sXHx6DsfObPN55p2czrKTRahBLC/U7XFDC1/2EJhpaSq/V0QUp6IrPDUIibg6afbLqMQaLOv3oWNHHnSJCQQCHqdgsUbAACtwnT2lNtUrvnMtp4CkB3sSmsOMDJbqmM8nVywqi6UHP1Dv6kRAvFsCHpYLdycARZZcxkoMajkjKpsL5aY28d4eNAs8b6ZwKWjz6ce2lxrkIyxdOeZCJbX4W9CHNkHFuOFpDfjQSOmGrDVVPOhLqCor1VpXEjwXlCeEtJWUZCkhVPOIw86eVHHgd/a0v4Wfp+nwwtIuuea5EGnHfjjilvArzwCryWo0vUrrsScssGKYfBU62lnspDqf7dlVM1kmMkZ35BseB+gNddZg9eXz6h8xdKQjlry3M4HG0SLLHvljOgKz02xRWl0iWeulJJfAw7YIfo5q+7b/hcq3sSqCweVHHkhaiVVxa66K+ZdejRZF0SvARKJxdaMWH9HXj/H87MPUpMJUFSP0auXvducMR67GlX4yeT4wOS8wZszSV7rWWO0vu446IRTJL7sSJEH+phNhhvBKy0wetLmpHbVCXvmPdamZY5Oz+Se/V3nVzff/MoZFV5WjHx1b5krn30PJAmOuW1TYneOGGiMWg8ObXvi/bHe5CIYma5dnH1GOpHrP04TLTe9QTrYpluspERV94xVFjz3mAcAhwcgq+vKWh5JJMnRGHgmjsQxK/55b6kduHTVRGkQ61Fi9yArongKhIHmp3SGK7ig3/0bxeVNGNgIM+zb47iVM9sxiiNqAVHfsranWi+Z+ysjmaXtFsOnwTaD4slTMoSS0Hcvvd108x3v74ITHxQ2X1SV3xpUoEJo0nOiWMIbDHSA8uvE5DnHZpD03AUY3W5O4rE7pZCWYDFY84FqpAHaBr6XBkvVwuuFd4JwKbuDLpYPjQNOhp5hUV1xlGSO9ztrEw61qCqda3FDAx9YHqj2JYauJcrmSWtNDz4uBYpx80aVTLzYNoxNb/2TOMHSCwl4HSNTfCxtvSV7x0UuweBV1U+QoKkOMqAzKDS4Yz1BS9zA4AhuSMfcvrCcOUkKNOtwG2SXeH4AJ089kY9BiLUvXoZanKRvkSmact/mOMU/n03B9poErK2ifIrdiw7PwqWCgnndSllCkOFyllEW04XYeY1a6iRf4RJ8aJ5P8RSD2pqpE8G4+PSnipnSjiqwBOZPZ9Rvul4XRzkwkAhByUUpxSWx4OZTyErgVgtTZd3NbKqUxoSxoBqooCmWo6ktmjZTUT1i2Lqdiuj9P6mD5T7rGBhHiG7fWk+Rhg2wx6FjPoM0mxA8wfCJP6gmy3IGiJHRD+tuksI+GYkHroj04hNvs+LI27GNQ2tuJPYKFt1OrKPhgbgDq7sJ28ajT44b5+yMW2nAFLh2cVaXuSdwoIyIliWvEWF5rvAC/I/w/g3wRvbIsozU/Id9wrrj3Rs6xUJ8K45pgMlO/wR5rP0RbjhDugBp2dWhNJJ/sTisfMOYfhcG9aia4sus7FJ452vJsV2c0m1zVD3hUOTbw3tL2gOg5Ed3tgHow9KjZ99YD3twtaeoqsBnm4F6LD9cfdKDic41Bf8Li7QcsmomnjKGn5U6tM7KgLBFivsH9KhWxrlX8qlf0fpTQ9e3rHTKd41dt55+/ZNX/65TlgPHyv4J5+QPm8zeYP+ImU/y9pwGDHUkr2knAu9bu+XlbKsrHmANmWldyYUjmnTDTfXphHyrUUcU0JEPjVEGLxyXmQzmViVJ6v2ZlxHUzW9zvz610R7jeSaXjxVnJZk4BnMpRLCsbMbWW2MUro0IrbwA9JYK/R65hXiav34wmA7LAsdbwqM1TTfPSgKcD7TxUr2oIot+6zPTiTbpSJyc/yu4o2Ihl0R4uq3p5vrEuFdiSrqqe0NzzaMm2heEJt6GVcPEFOBbkCzsxw40LRheIn8J+iViA19gt6KlLF4KlbwtPNdwm+0n2ayODY1GngiWdu5jAUzfuuivhNxBSDAVCkmKbIywaWKgjz2Gn14WtZk7bCbVucbKx3jfszOLwPBtMgYdSfW60y9iDp/2aPcgZGitt22dhU9o7wEHw/a4a1bWnGXuB9tSt6vNzX+rB1evUyH30oFrLJEdqzLSwL/H1NUX9LjhDsERaYl3MArfrDSp7XPaQhfKolmdf8M49iFM0zos0qVJIY3eGPFI2K68l/EDGwmQuWpWq50KlGUjPLRlaYpUEknTu+3QjxvJcnCDsQoNXdJliVV6nGbGFTVu4XYtL04X8SAyaTfboYBXrxySBR+jNLfXYgV1KOXYDRLvhyXLqLB7yC5mqb4Ec9fciO8l6RHuEm/P7mV6vw0ynTUpcVQZyls5XXKRxWBM8myGdloDlpzw1CfSRzTQtVgdteh71h4mudPYOwbbGixzt5YylXkP/csJpCz+DKnqYeRxIlkEevdNOJx9wjfJCbv5LSsV/0JgrbUrAUdjWg8Ic1OzIFe4/Qz5t+qu3/qA9grk6xzHhN9rTaKz5G+0p3mlPvB+05KA9yfZBSdTmyGubWkP4vrK0LwYorx2056CEucg06cF6jil/o0SS+YMSMf4q0J8pEf9pzoN4ceQ7rZFWE9U4f9XP9NZxrKg2v2hdHbVHLRTXOXmrU94ve97rriO5ZSTW1VvNdBpqSdiDJjbraud/p59GRZMa1/1GP7H/ln6+10aXgfzm2fvQcRRy3+vyGyUD/xmG0Kh8rD3LeG7EFube3Njqsj4op+k4FdCsKmPvBRSeBOrkI4J4ahIZ4SODeoAOleQSPNqmhntCgeiYkdJOAFUPmjwIKNRu9lHfgKi2Ng1eSn731ae5lbRUrzu9LuyOqptEXQZdoVTn1oeoqtfw+U4PpCSxWG057A0oc7rmb3ZqGkChRQSr3EpiiIPKJniz6vKlOc7a8EMCUVGH7049XzwskTPxcX0+z3pDPS95yIob1FjtANFGbCT+BU00Bsj42MQDAbzoGpuOTx1ed+lpmyNLCzMtoRvjzmNmcNGTGO37lRr9ZpwN8Fwh2tO7pfgyZffryVAvGTo0AkFB26NgIgO9nitDpoT7YwNdjOcig74LOo3UZn/QJREeLTK5leFN6daK9RBQ2oWacZbIUS4MJZsr02ezts+L7QY1SaXbhYfxetEjC9wbnrvAEy4E4iUWsNvqlIVf5n08DjM317w1Ec/sxmGcpBgOsLPrAMLWyPiQNB0Yzj23eKGhKHKXoEPTZof53cTcICEedUQWhBs+cFbZpTorz6StKT65rsICxhZSGMTg77Uh6nXmADAvTih3mSmww/I0wDnpyUEjVh4R4qOlVHdjyW8gk2fLLgzHPsb7bcPsvLaxeyyYy6PSWYGihJTCqWnaT4QMn3L88QwvnrSDg253tAkwK7iw1gUn+B7CH4vbbR12xzAtw5EtFZ5CifDm272V/wmtEh4HraIx8T/RKhg0W+Kx58ssyP8/0qqDQsH/hFZBn2jV8n+LVskcnf6kVQL0Qat+lrjD9qtpM3YEU6DMH+sly6xc/X7nihdo+jjF5yT99j+bn482T8MeH91UG8llXL34FBIZxHnH+QulQuatHsj6lF3vihfUiYKb8xpA8YIix8bKqlQhL9hqKyUXuyuR6snFIBA1o6dVqq+t00ZgSsnq9Vr2MRbd9ZZOly4eFJodh740yYqRj+3Zx4+zkyr57qFyCmNbXw7noXM5FI5QHixl9nycCjAz41DG3p02m3qUeRQknfXiIkL1kLIhhyGPYTwywAkTH9DD1BEv90diLJytd5ycDBH/XNsgtAWnqcwrNC7nVXuIHSwKQ66gsaPyParrFTbkIelfrwFx6zAtqqVB7W3j0ijpgoBnrwSYOFomVvd1j7nmhKPtBhKfNRPqRXURYSNfhGtgW4uEmHubK1Y+cL5/UfB7RYVLesK3y0V4VCQYsLME6nM+wY1mIzFy0u9juJ5wCYlNY+QBccH2KhvPdWoVvDDxUL4NiZNDnJbPZAf4xVkjBPpxvzktge5rZwn5E9FK7BKKVzPKT3q9KrmFjq50cEt0pCLSqNpULXZPaxHsYEO0JVBJeyFUjnk8piNX60mZoZ16wkEKOq4eLM+zGG/sXe2SDaYuZEUBmhiQ9sV2ZtTBdWlIo/M8Z5RQTC1uPEy0PxjSho9WektdTHfFzO0P57GAPvPe4+rnmqN6vQkbG8qIvbAzKUhUt/s211bfnZcdQstbSoPdA8JjCuUGwb+BVTN4auqgIXBlA1MV5PHHnSid1Y4s/kEjLgeN4AV+0vucIDpG6gZzKUlDO9PsaSyfEaHR5Oe7VYJGa29UaOU+KJPxQZlM7sgF8V/ANsY41MO0A9+Ic7aHn6AyPzDqrjW3mak38DA65FvjceyPfF3pcrgv5JMETPvyLD2vnE/tkm6zdapwcQcyJXTR+aRgJs9tcXbHQUxbEUmOlUsaLFx+hw5Mag5mZuABwCTmVVeagIIDMolFbPPYp8oMD0Mihg6xx6rwH1Qyy4pDVFm5swqrox459SAH9x6TQgDr6+NqoKI2YpWJnQLzrAMqpgjJ6l5n4mZMZITtWR41jdm7jdOCnbA2yEU+nQI853u98bAoAeDOkYNOpHAzy+AsnF2N9kvPXPRWRTYov1v5fL3YmuuwbClmybLO2JZ3a1nXiwu3UkSc/DIyKR0L4LE3wI4K6mzvwgkmPKHfzihxLZ1kBtCpCwf1LAbGqYhoQECfdasBJJ8Fdx7i0utVButCRy7kxo/SMFk1dJnNAJDTp3QhHdHBq6VFr14HgZKf3vZBjO8q0cm3TLwOlbFnPg5fMYnzjpDzgX2irvey2mHM18DMMHZCTmpePN3RB+qvAQlXBOJTd47XMBRGju3YDQ+AeRAy5pMLqF59SvQkf+j52s9AL7nc6GjMVrO84o/WKrtTTx549Kh9Uplw16VxltuAUjqvcQXxKt9yZuYuVJEm3bNpTfyZAu2ZjPe4nqnCVKwFUwsqv1MmyZ3PcNvk+5GBtnmqh+NUZKeH1otm1fzJOieXg3IX3lRclXNBrP8lbMvjOKeDnQ5lWH+W6GGTvrL6d7ccwy8p2zVNOsTpl7c/yzb/wtJfvn0R4T++XJyPLnI7pUP7/gAxrL8c+pby6E6PYxeX79Kj63dXtuk/vrDfHfvDmk9TvU/9YYY+N1E6vEIwRBIUSaIoDoIYBoMYCVPo0YQRisBJCsVwCkbJD01/G/lZ+WfP7Dmq0nj6m2sfD9q/vD0uOvz643BHvr4J/7h8T7XY4v0vatmUU5r8wX4/jLev361yC5PiD/VH48f8n1R/zHWWX//ljdf8Z0jeen60Ph5LndPtxzMry6b/utC6ZK7n8fWuWHiWdGp2jwyN7h41gAKX5qxZPqslg5cDWIqfXZla73iGs/RzK4lMP7Wehz7La19twe2BHGjyOKcsjh4zMBSASeNUryUApX1WCnkRZb4YczBrG9P0RAPnxu+3ZLy4cKTh63GC8HPYnvPzerVuM3Kl7tcB8cNbZrWN1IEqhqnwvS6syxlqV3Y6g154mAH4qbvaLOSEos7L7FTcZ3NGFsfyVCpqaM+yWtNXt9OmBY1TALMu610ZutyjPo9PyTwogoPzDCcUTyVAga1/KE6x6l2N8dbB7np1gfaO8rfI7vY1yTL2BosdjXVjy8GJYOzmQ4J8t2MEWN3ko1qF/fNRnMExHK+aSmjvtPbnCr/wP18NoE2aeQH4v94U+FtEgL83Px4v/vXg8f31h/e3N94e2X9+q+M/AEppOmo="},"customParameters":{"SHOPPER_EndToEndIdentity":"73f12b15c91c4627d6a04cd924b35990ff42b9e2d2a277f962aa8814dd1baf12","CTPE_DESCRIPTOR_TEMPLATE":""},"risk":{"score":"0"},"buildNumber":"2621780cf1399bc67d0207fab6d754153a4fc274@2022-08-11 07:04:47 +0000","timestamp":"2022-08-16 17:17:32+0000","ndc":"4B8F342F24BD96432BEC7238DDFE2185.prod01-vm-tx04"}';
        // $responseData = json_decode($staticdata);
        if(curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);

        //    dd($responseData);
        // $responseData->merchantTransactionId

        $payment_response = new PaymentResponse;
            $payment_response->response = $responseData_nojson;
            $payment_response->payment_id = $request->id;
            $payment_response->user_id = Auth::user()->id;
            
            // dd($payment_response, $responseData);

        $payment_response->save();


        $success_codes = array('000.000.000', '000.000.100', '000.100.105', '000.100.106', '000.300.000', '000.300.100', '000.300.101'. '000.300.102', '000.310.100' , '000.310.101', '000.310.110', '000.600.000', '000.100.110', '000.100.111', '000.100.112');


        if(in_array($responseData->result->code, $success_codes)) {

            $hyphen_exist = str_contains($responseData->merchantTransactionId, '-');
            if($hyphen_exist){
                $responseData->merchantTransactionId = str_split($responseData->merchantTransactionId,6);
                $responseData->merchantTransactionId = $responseData->merchantTransactionId[0];
            }


            Bookings::where('code', $responseData->merchantTransactionId)->update(['status' => "Accepted"]);
            $booking = Bookings::where('code', $responseData->merchantTransactionId)->first();

            $payouts = new Payouts;
                $payouts->booking_id     = $booking->id;
                $payouts->user_id        = $booking->host_id;
                $payouts->property_id    = $booking->property_id;
                $payouts->user_type      = 'host';
                $payouts->amount         = $booking->original_host_payout;
                $payouts->penalty_amount = 0;
                $payouts->currency_code  = $booking->currency_code;
                $payouts->status         = 'Future';
            $payouts->save();
    
            $message = new Messages;
                $message->property_id    = $booking->property_id;
                $message->booking_id     = $booking->id;
                $message->sender_id      = $booking->user_id;
                $message->receiver_id    = $booking->host_id;
                $message->message        = Session::get('message_host') ?? '';
                $message->type_id        = 4;
                $message->read           = 0;
            $message->save();
    
            $email_controller = new EmailController;

    
            // TWILLIO SMS

            // if ($booking->status =='Accepted') {
            //     $companyName = Settings::getAll()->where('type', 'general')->where('name', 'name')->first()->value;
            //     $instantBookingConfirm = ($companyName.': ' .'Your booking is confirmed from'.' '. $booking->start_date.' '.'to'.' '.$booking->end_date );
            //     $instantBookingPaymentConfirm =($companyName.' ' .'Your payment is completed for'.' '.$booking->properties->name);

            //     twilioSendSms(Auth::user()->formatted_phone, $instantBookingConfirm);
            //     twilioSendSms(Auth::user()->formatted_phone, $instantBookingPaymentConfirm);
    
            // } else {
            //      twilioSendSms(Auth::user()->formatted_phone, 'Your booking is initiated, Wait for confirmation');
    
            // }
            if ($booking->booking_type == "request") {
                $dates = [];
                $price_list   = json_decode($this->helper->getPrice($booking->property_id, $booking->start_date, $booking->end_date, $booking->guest));
                // dd($price_list);

                $propertyCurrencyCode = PropertyPrice::firstWhere('property_id', $booking->property_id)->currency_code;
                foreach ($price_list->date_with_price as $dp) {
                    $tmp_date = setDateForDb($dp->date);
                    $property_id = $booking->property_id;
                    $price = $this->helper->convert_currency($price_list->currency, $propertyCurrencyCode, $dp->original_price);
                    Session::put('propertydatedata',['property_id'=>$property_id,'status'=>"Not available",'price'=>$price,'date'=>$tmp_date]);
                    
                    $email_controller->booking($booking->id, $booking->start_date);
                    $email_controller->booking_user($booking->id, $booking->start_date);
                    $code = $responseData->merchantTransactionId;
                    
                    // $property_data = [
                    //     'property_id' = $data['property_id'],
                    //     'status'      => 'Not available',
                    //     'price'       => $this->helper->convert_currency($data['price_list']->currency, $propertyCurrencyCode, $dp->original_price),
                    //     'date'        => $tmp_date
                    // ];
    
                    // PropertyDates::updateOrCreate(['property_id' => $data['property_id'], 'date' => $tmp_date], $property_data);
                    // if($data['paymode'] == 'Bank') {
                    //     array_push($dates, ['booking_id'=> $booking->id, 'date' => $tmp_date ]);
                    // }
                }
            }else{

                $this->InsertPropertyDate();
                $email_controller->booking($booking->id, Session::get('checkinDate'));
                $email_controller->booking_user($booking->id, Session::get('checkinDate'));
                $code = Session::get('bookingcode');
            }


            Session::forget('payment_property_id');
            Session::forget('payment_checkin');
            Session::forget('payment_checkout');
            Session::forget('payment_number_of_guests');
            Session::forget('payment_booking_type');
            Session::forget('message_host');
            Session::forget('checkinDate');
    
            clearCache('.calc.property_price');

            

            $this->helper->one_time_message('success', trans('messages.booking_request.request_has_sent'));
            // return redirect('booking/requested?code='.$code);
            return redirect(url('booking/receipt?code='.$code));

        } else {
            // dd($responseData);
            $this->helper->one_time_message('error', $responseData->result->description . ' ERROR_CODE (' . $responseData->result->code . ')');
            // return redirect('payments/book/'.Session::get('payment_property_id'));  
            $code = Session::get('bookingcode');
            return redirect('booking/failed?code='.$code);
            // create new page for error

        }
    }

    public function cancel(Request $request)
    {
        $this->helper->one_time_message('success', trans('messages.error.payment_process_error'));
        return redirect('payments/book/'.Session::get('payment_property_id'));
    }

    //for debug
    public function checkstatus_hyperpay(Request $request){

        $url = $this->hyperpay_apiURL."v1/payments/".$request->paymentid;

        $url .= "?entityId=$this->hyperpay_entity_id";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer '.$this->hyperpay_access_token));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = json_decode(curl_exec($ch));
        if(curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);

        dd($responseData);
    }

    // public function testpay(Request $request){

            //     $url    = $this->hyperpay_apiURL."v1/payments";
            //     $dataa  = 
            //                 "entityId=$this->hyperpay_entity_id".
            //                 // "&amount=147" .
            //                 // "&amount=$amount" .
            //                 // "&currency=EUR" .
            //                 // "&paymentType=DB";

            //                 // "&entityId=8ac7a49f6e4a18ae016e4b8b4c4b2146" .
            //                 "&amount=12.00" .
            //                 "&currency=SAR" .
            //                 "&paymentBrand=VISA" .
            //                 "&paymentType=PA" .
            //                 "&merchantTransactionId=order99234" .
            //                 "&transactionCategory=EC" .
            //                 "&card.number=4000000000000010" .
            //                 "&card.expiryMonth=12" .
            //                 "&card.expiryYear=2025" .
            //                 "&card.cvv=123" .
            //                 "&card.holder=John Smith" .
            //                 "&merchant.name=MerchantCo" .
            //                 "&merchant.city=Munich" .
            //                 "&merchant.country=DE" .
            //                 "&merchant.mcc=5399" .
            //                 "&shopperResultUrl=https://merchant.org" .
            //                 "&customer.ip=192.168.0.1" .
            //                 "&customer.browser.acceptHeader=text/html" .
            //                 "&customer.browser.screenColorDepth=48" .
            //                 "&customer.browser.javaEnabled=false" .
            //                 "&customer.browser.javascriptEnabled=true" .
            //                 "&customer.browser.language=de" .
            //                 "&customer.browser.screenHeight=1200" .
            //                 "&customer.browser.screenWidth=1600" .
            //                 "&customer.browser.timezone=60" .
            //                 "&customer.browser.challengeWindow=4" .
            //                 "&customer.browser.userAgent=Mozilla/4.0 (MSIE 6.0; Windows NT 5.0)" .
            //                 "&testMode=EXTERNAL";
            //     $ch = curl_init();
            //     curl_setopt($ch, CURLOPT_URL, $url);
            //     curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            //                 'Authorization:Bearer '.$this->hyperpay_access_token));
            //     curl_setopt($ch, CURLOPT_POST, 1);
            //     curl_setopt($ch, CURLOPT_POSTFIELDS, $dataa);
            //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);// this should be set to true in production
            //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //     $responseData = json_decode(curl_exec($ch));
            //     if(curl_errno($ch)) {
            //         return curl_error($ch);
            //     }
            //     curl_close($ch);
            //     // dd($responseData);

            //     $title = 'HyperPay Payment';
            //     return view('payment.payment2', compact('responseData', 'title'));
    // }

    
}