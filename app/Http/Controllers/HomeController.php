<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Helpers\Common;
use App\Http\Controllers\Controller;
use Twilio\Rest\Client;
use Illuminate\Support\Facades\Cache;
  

use View, Auth, App, Session, Route, DB;

use App\Models\{
    Currency,
    Properties,
    PropertyAddress,
    PropertyPrice,
    PropertySteps,
    PropertyPhotos,
    PropertyDescription,
    Page,
    Settings,
    StartingCities,
    Testimonials,
    language,
    Admin,
    User,
    UserDetails,
    Wallet,
    UsersVerification
};


require base_path() . '/vendor/autoload.php';

class HomeController extends Controller
{
    private $helper;

    public function __construct()
    {
        $this->helper = new Common;
    }

    // public function data_insert_users(){
        //         $query = DB::table('old_user')
        //         ->join('userprofile as up', 'up.userId', '=', 'old_user.id')
        //         ->select('old_user.id', 'old_user.email', 'old_user.password', 'up.firstName', 'up.lastName')
        //         ->where('old_user.email', '!=', '');

        //             $user_data = $query->get();
        //             $c = 0;
        //             $e = 0;
        //             foreach($user_data as $data){

        //                 $c++;
        //                 try {
        //                     $usercreated =  User::insertOrIgnore([
        //                         'first_name' => $data->firstName,
        //                         'last_name' => $data->lastName,
        //                         'email' => $data->email,
        //                         'password' => $data->password,
        //                         'old_id' => $data->id
        //                     ]);

        //                     $userid = DB::getPdo()->lastInsertId();
        //                     $userdetails =  UserDetails::Create([
                                
        //                         'user_id' => $userid,
        //                         'field' => 'date_of_birth',
        //                         'value' => '1992-1-1'
        //                     ]);
                            
        //                     $userwallet =  Wallet::Create([
        //                         'user_id' => $userid,
        //                         'currency_id' => 1,
        //                         'balance' => 0.00,
        //                     ]);

        //                 }
        //                 catch (Exception $e) {
        //                     echo $e++;
        //                     continue;
        //                 }


        //         }

        //         dd('inserted: '.$c, 'errors: '.$e);
    // }
    
    // public function data_insert_properties(){
        // $properties_query = DB::table('listing')
        // ->select('listing.title', 'listing.userId', 'listing.bedrooms','listing.beds', 'listing.bathrooms', 'listing.lat', 'listing.lng', 'listing.city', 'listing.state', 'listing.country', 'listing.zipcode')
        // ->whereNotNull('listing.title')
        // ->get();

        // // dd(count($properties_query));
        // // $data = $properties_query;
        // // $slug = $this->helper->pretty_url($data->title);
        // // $user_new_id = DB::table('users')->select('users.id')->where('old_id', '=', $data->userId)->first()->id;
        // // dd($data);
        // // dd($data->title, $data->userId, $slug, $data->bedrooms, $data->beds, $data->bathrooms, 'NEW ID:'.$user_new_id );


        // $counter = 0;
        // $er = 0;
        // foreach($properties_query as $data){
        //     $counter++;
        //     $slug = $this->helper->pretty_url($data->title);
            
        //     $user_new_id = DB::table('users')->select('users.id')->where('old_id', '=', $data->userId)->first();
        //     if($user_new_id){
        //         $user_id0 = $user_new_id->id;
        //     }else{
        //         $user_id0 = '12345';
        //     }

        //     try {

        //         $propertycreated =  Properties::insertOrIgnore([
        //             'name' => $data->title,
        //             'slug' => $slug,
        //             'host_id' => $user_id0,
        //             'bedrooms' => $data->bedrooms,
        //             'beds' => $data->beds,
        //             'bathrooms' => $data->bathrooms,
        //         ]);

        //         $created_property_id = DB::getPdo()->lastInsertId();

        //         $addresscreated =  PropertyAddress::insertOrIgnore([
        //             'property_id' => $created_property_id,
        //             'latitude' => $data->lat,
        //             'longitude' => $data->lng,
        //             'city' => $data->city,
        //             'state' => $data->state,
        //             'country' => $data->country,
        //             'postal_code' => $data->zipcode,
        //         ]);

        //         $pricecreated =  PropertyPrice::insertOrIgnore([
        //             'property_id' => $created_property_id,
        //             'currency_code' => 'SAR',
        //         ]);

        //         $stepcreated =  PropertySteps::insertOrIgnore([
        //             'property_id' => $created_property_id,
        //         ]);
 
        //         $photoscreated =  PropertyPhotos::insertOrIgnore([
        //             'property_id' => $created_property_id,
        //             'photo' => 'migrated_property.jpg',
        //         ]);

        //         $discriptioncreated =  PropertyDescription::insertOrIgnore([
        //             'property_id' => $created_property_id,
        //         ]);

        //         $path = public_path('images/property/'.$created_property_id.'/');

        //         if (!file_exists($path)) {
        //             mkdir($path, 0777, true);
        //         }


        //     } catch (\Exception $e) {
        //         $er++;
        //     }
        // }

        // dd('inserted: '.$counter, 'errors: '.$er);

    // }

    public function images_insert_properties(){
        

        $getNewIds = DB::table('listing')
        ->select('listing.userid', 'listing.id as listing_id', 'users.id as user_id', 'properties.id as prop_id')
        ->join('users', 'users.old_id', '=', 'listing.userId')
        ->join('properties', 'properties.host_id', '=', 'users.id')
        ->groupBy('properties.id')
        ->get();

        
$outercounter = 0;
$innercounter = 0;
$imagefound = 0;
$noimage = 0;
        foreach($getNewIds as $data){
            $outercounter++;

            $listphotos = DB::table('listphotos')->where('listId', $data->listing_id)->get();
            // dd($listphotos);

            $newPropertyid = $data->prop_id;
            
            foreach($listphotos as $old){
            $innercounter++;

                DB::table('property_photos')
                ->where('property_id', $newPropertyid)
                ->update(['photo' => $old->name]);
                
                if(file_exists(public_path('images/property_temp/'.$old->name))){

                    $movefile =  rename(public_path('images/property_temp/'.$old->name), public_path('images/property/'.$newPropertyid.'/'.$old->name));
                    $imagefound++;
                }else{
                    $noimage++;
                }

                // File::move(public_path('images/property_temp/'.$old->name), public_path('images/property/'.$newPropertyid.'/'.$oldname));


            }
        }



    dd('OUTER: '.$innercounter, 'INNER: '. $innercounter, 'IMAGES NOT FOUND: '. $noimage, 'FOUND AND MOVE IMGAES: '. $imagefound);

    }

    public function user_wallet_entries(){
        $users = DB::table('users')->get();
        
        $exist = 0;
        $not_exist = 0;
        $total = 0;
        
        foreach ($users as $key => $user) {
            $total ++;
            // $users_verifiaction = UsersVerification::Where('user_id', '!=' , $user->id)->exists();
            $wallets = DB::table('wallets')->where('wallets.user_id' ,'=', $user->id)->first();
            if($wallets){
                $exist++;
            }else{
                Wallet::create([
                    'user_id' => $user->id,
                ]);
                $not_exist++;
            }
        }

        dd('WALLETs ALREADY EXISTS: '. $exist, 'NEW WALLET ENTRIES: '. $not_exist, ' OF TOTAL USERS: '. $total);
    }
    
    
    public function user_verification_entries(Request $request){

        $users = DB::table('users')->get();
        
        $exist = 0;
        $not_exist = 0;
        $total = 0;
        
        foreach ($users as $key => $user) {
            $total ++;
            // $users_verifiaction = UsersVerification::Where('user_id', '!=' , $user->id)->exists();
            $users_verifiaction = DB::table('users_verification')->where('users_verification.user_id' ,'=', $user->id)->first();
            if($users_verifiaction){
                $exist++;
            }else{
                UsersVerification::create([
                    'user_id' => $user->id,
                ]);
                $not_exist++;
            }
        }

        dd('ALREADY EXISTS: '. $exist, 'NEW ENTRIES: '. $not_exist, 'TOTAL USERS: '. $total);
    }

    public function index()
    {
        $data['starting_cities'] = StartingCities::getAll();
        $data['properties']          = Properties::recommendedHome();
        $data['testimonials']        = Testimonials::getAll();
        $data['sessionLanguage']     = Session::get('language');
        $language                    = Settings::getAll()->where('name', 'default_language')->where('type', 'general')->first();

        $languageDetails             = language::where(['id' => $language->value])->first();

        if (!($data['sessionLanguage'])) {
            Session::pull('language');
            Session::put('language', $languageDetails->short_name);
            App::setLocale($languageDetails->short_name);
        }

        $pref = Settings::getAll();

        $prefer = [];

        if (!empty($pref)) {
            foreach ($pref as $value) {
                $prefer[$value->name] = $value->value;
            }
            Session::put($prefer);
        }
        $data['date_format'] = Settings::getAll()->firstWhere('name', 'date_format_type')->value;
        // dd($data['']);
        return view('home.home', $data);
    }

    public function phpinfo()
    {
        echo phpinfo();
    }

    public function login()
    {
        return view('home.login');
    }

    public function setSession(Request $request)
    {
        if ($request->currency) {
            Session::put('currency', $request->currency);
            $symbol = Currency::code_to_symbol($request->currency);
            Session::put('symbol', $symbol);
        } elseif ($request->language) {
            Session::put('language', $request->language);
            $name = language::name($request->language);
            Session::put('language_name', $name);
            App::setLocale($request->language);
        }
    }

    public function cancellation_policies()
    {
        return view('home.cancellation_policies');
    }

    public function staticPages(Request $request)
    {
        $pages          = Page::where(['url'=>$request->name, 'status'=>'Active']);
        if (!$pages->count()) {
            abort('404');
        }
        $pages           = $pages->first();
        $data['content'] = str_replace(['SITE_NAME', 'SITE_URL'], [SITE_NAME, url('/')], $pages->content);
        $data['title']   = $pages->url;
        $data['url']     = url('/').'/';
        $data['img']     = $data['url'].'public/images/2222hotel_room2.jpg';

        return view('home.static_pages', $data);
    }

    public function walletUser(Request $request){

        $users = User::all();
        $wallet = Wallet::all();

        // SELECT users.first_name, users.last_name FROM users WHERE id NOT IN (SELECT user_id FROM wallets);

        if (!$users->isEmpty() && $wallet->isEmpty() ) {
            foreach ($users as $key => $user) {

                Wallet::create([
                    'user_id' => $user->id,
                    'currency_id' => 1,
                    'balance' => 0,
                    'is_active' => 0
                ]);
            }
        }

        return redirect('/');

    }

}
