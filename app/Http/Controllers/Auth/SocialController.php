<?php
namespace App\Http\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{
    public function githubredirect(Request $request)
    {
        return Socialite::driver('github')->redirect();
    }
    public function callback(Request $request)
    {
        $user = Socialite::driver('github')->user();
        // $user->token
        dd($user);
    }
}
?>