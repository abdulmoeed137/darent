<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{

  
    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
    
    public function register(Request $request)
    {
        $firstname = $request->input('first_name');
        $lastname = $request->input('last_name');
         $email = $request->input('email');
         $phone = $request->input('phone');
         $password = bcrypt($request->input('password'));
      $users =  DB::table('users')->insert([
    'first_name' => $firstname,
    'last_name' => $lastname,
    'email' => $email,
    'phone' => $phone,
    'password' => $password
]);
        if($users)
        {
           
        return redirect()->route('dashboard');  
        }
        else{
            return redirect('/')->with('error', 'Email and Password Does not Match');
        }
    }
}
