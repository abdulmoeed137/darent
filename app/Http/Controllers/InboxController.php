<?php
namespace App\Http\Controllers;

use Auth, DB, validator;
use Illuminate\Http\Request;
use App\Http\Helpers\Common;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\{Currency, Messages, Properties, Bookings, User};
use Illuminate\Support\Facades\Auth as FacadesAuth;

use function PHPUnit\Framework\isEmpty;

class InboxController extends Controller
{
    private $helper;

    public function __construct()
    {
        $this->helper = new Common;
    }

    /**
    * Inbox Page
    * Conversassion List
    * Message View
    */
    public function MuanualMessage(Request $request){
        // dd($request);
        $checkmessage = Messages::where('property_id' , $request->propertyid)
                ->where(function($q) {
                    $q->where('sender_id', Auth::id())
                      ->orWhere('receiver_id', Auth::id());
                })->get();
    
       if(count($checkmessage) > 0){
        
        return response()->json(["message"=>"success"]);
       }else{
        $GreetingMessage = new Messages();
        $GreetingMessage->property_id =  $request->property_id;
        $GreetingMessage->booking_id =  Auth::id();
        $GreetingMessage->sender_id =  Auth::id();
        $GreetingMessage->receiver_id =  $request->receiver_id;
        $GreetingMessage->message =  "I am interested in this property";
        $GreetingMessage->type_id =  1;
        $GreetingMessage->read =  0;
        $GreetingMessage->archive =  0;
        $GreetingMessage->star =  0;
        $GreetingMessage->host_id =  $request->host_id;
        
        $GreetingMessage->save();
       
        return response()->json(["message"=>"success"]);
       }
    
    }
    public function index(Request $request)
    {
        // dd($request);
        $data['firstmsg'] = Messages::where('property_id' , $request->propertyid)
                ->where(function($q) {
                    $q->where('sender_id', Auth::id())
                      ->orWhere('receiver_id', Auth::id());
                })->first();
                // dd($data['firstmsg']);
        $data['users'] = Properties::join('messages','properties.id','=','messages.property_id')
        ->where("messages.receiver_id","=",Auth::id())->orWhere("messages.sender_id","=",Auth::id())
        ->groupBy('properties.id')
        ->get();
        // dd($data['users']);
        
        $data['messages'] = Messages::with(['properties:id,name,host_id','sender','receiver','User:first_name,id'])
            ->where('sender_id', Auth::id())
            ->orWhere('receiver_id', Auth::id())
            ->orderBy('id', 'desc')
            ->get()->unique('properties_id');
            // dd($data['messages']);
            $data['man'] =Properties::join('users', 'users.id', '=', 'properties.host_id')
        ->where('users.id',Auth::id())
        ->get()->unique('first_name');
        // dd($data['messages']);

        if (count($data['messages']) > 0) {
            $booking_id = $data['messages'][0]->booking_id;
        // dd($booking_id);
        $data['conversation'] = Messages::where('property_id' , $request->propertyid)
        ->where(function($q) {
            $q->where('sender_id', Auth::id())
              ->orWhere('receiver_id', Auth::id());
        })->get();
            $data['properties'] = Properties::where('id', $booking_id)
                ->with('users')
                ->first();
    
         }
        //  dd($data);
        return view('users.inbox', $data);
    }
    public function window(Request $request)
    {
        $data['firstmsg'] = Messages::where('property_id' , $request->propertyid)
                ->where(function($q) {
                    $q->where('sender_id', Auth::id())
                      ->orWhere('receiver_id', Auth::id());
                })->first();
// dd(   $data['firstmsg']);
                if($data['firstmsg'] != null){
                    $data['firstmsgsenderinfo'] = User::where('id',$data['firstmsg']->sender_id)->first();

                }
                // dd($data['firstmsgsenderinfo']);
        // dd($data['firstmsg']->customer_id);
        $id=$request->id; // host id
       
        $data['title'] = 'Inbox';
        $data['users'] = Properties::join('messages','properties.id','=','messages.property_id')
        ->where("messages.receiver_id","=",Auth::id())->orWhere("messages.sender_id","=",Auth::id())
        ->groupBy('properties.id')
        ->get();
        $data['messages'] = Messages::with(['properties:id,name,host_id','sender','receiver','User:first_name,id'])
            ->where('sender_id', $id)
            ->orWhere('receiver_id', $id)
            ->orderBy('id', 'desc')
            ->get()->unique('properties_id');
// dd($data['messages']);
            $data['id'] = $request->id;
            // dd($data);

            $data['man'] =Properties::join('users', 'users.id', '=', 'properties.host_id')
            ->where('users.id',$id)
            ->get()->unique('first_name');
            
            
            if(count($data['messages']) > 0) {
                // dd($data['messages']);
                $host_id = $data['messages'][0]['properties']->host_id;
                $booking_id = $data['messages'][0]->property_id;

            // dd($booking_id);
            
                $data['conversation'] = Messages::where('property_id' , $request->propertyid)
                ->where(function($q) {
                    $q->where('sender_id', Auth::id())
                      ->orWhere('receiver_id', Auth::id());
                })->get();
                // dd($data['conversation']);
                $data['properties'] = Properties::where('id', $request->propertyid)
                ->with('users')
                ->first();       
            }
         
         return view('users.inbox', $data);
    }

    /**
    * Message Read status Change
    * Details pass according to booking message
    */
    public function message(Request $request)
    {
        $booking_id = $request->id;
        $message = Messages::where([['booking_id', '=', $booking_id], ['receiver_id', '=', Auth::id()]])->update(['read' => 1]);

        $data['messages'] = Messages::where('booking_id', $booking_id)->get();
        $data['booking'] = Bookings::where('id', $booking_id)
                          ->with('host')->first();
        $data['symbol'] = Currency::getAll()->firstWhere('code', $data['booking']->currency_code)->symbol;
        return response()->json([
             "inbox"=>view('users.messages', $data)->render(), "booking"=>view('users.booking', $data)->render()
        ]);
    }

    /**
    * Message Reply
    * Message read status change
    */
    public function messageReply(Request $request)
    {
        $messages = Messages::where([['booking_id', '=', $request->booking_id], ['receiver_id', '=', Auth::id()]])->update(['read' => 1]);

        $rules = array(
            'msg'      => 'required|string',
        );

        $validator = Validator::make($request->all(), $rules);

        if (!$validator->fails()) {
            $message = new Messages;
            $message->property_id = $request->property_id;
            $message->booking_id = $request->booking_id;
            $message->receiver_id = $request->receiver_id;
            $message->sender_id = Auth::id();
            $message->message = $request->msg;
            $message->type_id = 1;
            $message->save();
            return 1;
        }
    }
}