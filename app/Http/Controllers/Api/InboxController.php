<?php

namespace App\Http\Controllers\Api;
use App\Http\Helpers\Common;
use function PHPUnit\Framework\isEmpty;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Currency, Messages, Properties, Bookings, User};

use Auth, DB, validator;



class InboxController extends Controller
{
    private $helper;

    public function __construct()
    {
        $this->helper = new Common;
    }

    public function index(Request $request)
    {
        try{
       $firstmsg = Messages::where('property_id' , $request->propertyid)
        
            ->where('sender_id', Auth::id())
              ->orWhere('receiver_id', Auth::id())
        ->first();
        
            $users = Properties::join('messages','properties.id','=','messages.property_id')
            ->where('messages.sender_id', Auth::user()->id)
            ->orWhere('messages.receiver_id', Auth::user()->id)
            ->groupBy('properties.id')
            ->get(['messages.*', 'properties.*']);

            
            // 'properties:id,name,host_id',
            $data['messages'] = Messages::with(['properties:*','sender','receiver','User:first_name,id,profile_image'])
                ->where('sender_id', Auth::id())
                ->orWhere('receiver_id', Auth::id())
                ->orderBy('id', 'desc')
                ->get()->unique('property_id');
                // dd($data['messages']);
               
                
            //     $data['man'] =Properties::join('users', 'users.id', '=', 'properties.host_id')
            // ->where('users.id',Auth::id())
            // ->get()->unique('first_name');
            // dd($data['man']);
       
            if (count($data['messages']) > 0) {
                $booking_id = $data['messages'][0]->booking_id;
            
            $data['conversation'] = Messages::where('property_id' , $request->propertyid)
            ->where(function($q) {
                $q->where('sender_id', Auth::id())
                ->orWhere('receiver_id', Auth::id());
            })->get();
                $data['properties'] = Properties::where('id', $booking_id)
                    ->with('users')
                    ->first();

            }
            
            return response()->json(['message'=>'Success','data'=>$data],200);

        }
        catch (\Throwable $th) {
            return response(['message' => 'failure', 'error' => $th->getMessage()], 500);
        }
        
    }

    public function window(Request $request)
    {
        try{
            $data['firstmsg'] = Messages::where('property_id' , $request->propertyid)
                ->where(function($q) {
                    $q->where('sender_id', Auth::id())
                      ->orWhere('receiver_id', Auth::id());
                })->first();
                if($data['firstmsg'] != null){
                    $data['firstmsgsenderinfo'] = User::where('id',$data['firstmsg']->sender_id)->first();

                }
                
        $id=$request->id; // host id
       
        $data['title'] = 'Inbox';
        $data['users'] = Properties::join('messages','properties.id','=','messages.property_id')
        ->where("messages.receiver_id","=",Auth::id())->orWhere("messages.sender_id","=",Auth::id())
        ->groupBy('properties.id')
        ->get(['messages.*', 'properties.*']);
        // return response()->json(['message'=>'Success','data'=>$data],200);

        $data['messages'] = Messages::with(['properties:*','sender','receiver','User:first_name,id,profile_image'])
            ->where('sender_id', $id)
            ->orWhere('receiver_id', $id)
            ->orderBy('id', 'desc')
            ->get()->unique('properties_id');
            $data['id'] = $request->id;

            $data['man'] =Properties::join('users', 'users.id', '=', 'properties.host_id')
            ->where('users.id',$id)
            ->get()->unique('first_name');
            
            
            if(count($data['messages']) > 0) {
                $host_id = $data['messages'][0]['properties']->host_id;
                $booking_id = $data['messages'][0]->property_id;

            // dd($booking_id);
            
                $data['conversation'] = Messages::where('property_id' , $request->propertyid)
                ->where(function($q) {
                    $q->where('sender_id', Auth::id())
                      ->orWhere('receiver_id', Auth::id());
                })->get();
                $data['properties'] = Properties::where('id', $request->propertyid)
                ->with('users')
                ->first();       
            }
         
         return response()->json(['message'=>'Success','data'=>$data],200);

        }
        catch (\Throwable $th) {
            return response(['message' => 'failure', 'error' => $th->getMessage()], 500);
        }
        
    }

    /**
    * Message Read status Change
    * Details pass according to booking message
    */
    public function message(Request $request)
    {
        try{
            $booking_id = $request->booking_id;
            $message = Messages::where([['booking_id', '=', $booking_id], ['receiver_id', '=', Auth::id()]])->update(['read' => 1]);
    
            $data['messages'] = Messages::where('booking_id', $booking_id)->get();
            $data['booking'] = Bookings::where('id', $booking_id)
                              ->with('host')->first();
            $data['symbol'] = Currency::getAll()->firstWhere('code', $data['booking']->currency_code)->symbol;
            return response()->json([
                 "inbox"=>'users.messages view', "booking"=>'users.booking view','data'=>$data
                //  "inbox"=>view('users.messages', $data)->render(), "booking"=>view('users.booking', $data)->render()
            ],200);
        }
        catch (\Throwable $th) {
            return response(['message' => 'failure', 'error' => $th->getMessage()], 500);
        }

        
    }

    /**
    * Message Reply
    * Message read status change
    */
    public function messageReply(Request $request)
    {
        try{
            $messages = Messages::where([['booking_id', '=', $request->booking_id], ['receiver_id', '=', Auth::id()]])->update(['read' => 1]);

            $rules = array(
                'msg'      => 'required|string',
            );
    
            $validator = Validator::make($request->all(), $rules);
    
            if (!$validator->fails()) {
                $message = new Messages;
                $message->property_id = $request->property_id;
                $message->booking_id = $request->booking_id;
                $message->receiver_id = $request->receiver_id;
                $message->sender_id = Auth::id();
                $message->message = $request->msg;
                $message->type_id = 1;
                $message->save();
                return response()->json(['message'=>'Success','data'=>'1'],200);
            }
        }
        catch (\Throwable $th) {
            return response(['message' => 'failure', 'error' => $th->getMessage()], 500);
        }

        
    }


    public function MuanualMessage(Request $request){
        // dd($request);
        $checkmessage = Messages::where('property_id' , $request->propertyid)
                ->where(function($q) {
                    $q->where('sender_id', Auth::id())
                      ->orWhere('receiver_id', Auth::id());
                })->get();
    
       if(count($checkmessage) > 0){
        
        return response()->json(["message"=>"Success"],200);
       }else{
        $GreetingMessage = new Messages();
        $GreetingMessage->property_id =  $request->property_id;
        $GreetingMessage->booking_id =  Auth::id();
        $GreetingMessage->sender_id =  Auth::id();
        $GreetingMessage->receiver_id =  $request->receiver_id;
        $GreetingMessage->message =  "I am interested in this property";
        $GreetingMessage->type_id =  1;
        $GreetingMessage->read =  0;
        $GreetingMessage->archive =  0;
        $GreetingMessage->star =  0;
        $GreetingMessage->host_id =  $request->host_id;
        
        $GreetingMessage->save();
       
        return response()->json(["message"=>"Success"],200);
       }
    
    }
}
