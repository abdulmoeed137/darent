<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Common;
use App\Models\Bookings;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class BookingController extends Controller
{
    public function __construct()
    {
        $this->helper = new Common;
    }
    public function myBookings(Request $request)
    {
        // return response()->json(["message"=>"Yes"]);
        switch ($request->status) {
            case 'Expired':
                $params  = [['created_at', '<', Carbon::yesterday()], ['status', '!=', 'Accepted']];
                break;
            case 'Current':
                $params  = [['start_date', '<=', date('Y-m-d')], ['end_date', '>=', date('Y-m-d')],['status', 'Accepted']];
                break;
            case 'Upcoming':
                $params  = [['start_date', '>', date('Y-m-d')], ['status', 'Accepted']];
                break;
            case 'Completed':
                $params  = [['end_date', '<', date('Y-m-d')],['status', 'Accepted']];
                break;
            case 'Pending':
                $params           = [['created_at', '>', Carbon::yesterday()], ['status', $request->status]];
                break;
            default:
                $params           = [];
                break;
        }
        $data['yesterday'] = Carbon::yesterday();
        $data['status']  = $request->status;
        $data['title']   = "Bookings";
        $data['bookings'] = Bookings::with(['users','properties'])
            ->where('host_id', Auth::user()->id)
            ->where($params)->orderBy('id', 'desc')
            ->paginate(Session::get('row_per_page'));
            return response()->json(['message'=>'success','data'=>$data],200);
        // return view('booking.my_bookings', $data);
    }
}
