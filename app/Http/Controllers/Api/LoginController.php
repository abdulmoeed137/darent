<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\UserController;
use App\Models\User;
use App\Models\UserDetails;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Auth;

class LoginController extends Controller
{
    public function register(Request $request , UserController $user_controller){

        $firstname = $request->input('first_name');
        $lastname = $request->input('last_name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $password = bcrypt($request->input('password'));


        $validation = Validator::make($request->all(),[
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'phone' => 'required',
            'password' => 'required|min:6'
        ]);
        if ($validation->fails()) { 
            return response()->json(['message'=>'failure','error'=>$validation->errors()], 422);            
        }
         
        
        $users =  DB::table('users')->insert([
        'first_name' => $request->first_name,
        'last_name' => $request->last_name,
        'email' => $request->email,
        'phone' => $request->phone,  
        'password' => bcrypt($request->password)
        ]);
            
            
        $user = User::where('email', $email)->first();
        $user_controller->wallet($user->id);
        $token = $user->createToken('API Token')->accessToken;
        return response()->json(['message'=>'Success','user' => $user, 'token' => $token],200);
    }
    public function test(){
        return "yes hello0";
    }
    public function login(Request $request)
    {
        try{
            $remember = ($request->remember_me) ? true : false;

            $validator = Validator::make($request->all(), [
                'email'    => 'required|email|max:200',
                'password' => 'required',
            ]);
           
    
            if ($validator->fails()) {
                return response()->json(['message' => 'failure','error'=>$validator->errors()], 422);
                
            } else {
               
                $users = User::where('email', $request->email)->first();
    
                if (!empty($users)) {
                    if ($users->status != 'Inactive') {
                        if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $remember)) {
                            $user = Auth::user();
                            $user->dateofbirth= isset(Auth::user()->user_details->where('field','date_of_birth')->first()->value) ?Auth::user()->user_details->where('field','date_of_birth')->first()->value : null;
                           
                            $token = auth()->user()->createToken('API Token')->accessToken;
                            
                            $data = [
                                'user'=>$user,
                                'token' => $token
                            ];
    
                            return response()->json(['message' => 'Success', 'data' => $data],200);
    
                        } else {
                            return response()->json(['message' => 'failure', 'error' => 'Invalid Credentials'],422);
    
                        }
                    }
                    else{
                        return response()->json(['message' => 'failure', "error" => "User is inactive. Please try agin!"],422);
                    }
                }else{
                    return response()->json(['message' => 'failure', "error" => "There isn't an account associated with this email address." ],500);
                }
    
            }
        }
        catch (\Throwable $th) {
            return response(['message' => 'failure', 'error' => $th->getMessage()], 500);
        }
        
    }
    public function logout(){
        try{
            $user = Auth::user()->token();
            $user->revoke();
            return response()->json(['message'=>'Log Out'],200);
        }
        catch (\Throwable $th) {
            return response(['message' => 'failure', 'error' => $th->getMessage()], 500);
        }
        
    }

}