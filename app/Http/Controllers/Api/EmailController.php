<?php

namespace App\Http\Controllers\Api;
use validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Helpers\Common;
use App\Http\Requests;

use App\Models\{Accounts,
    Admin,
    Bookings,
    Currency,
    EmailTemplate,
    PasswordResets,
    PaymentMethods,
    Payouts,
    Rooms,
    Settings,
    User,
    ContactUs};
use Session;
use Auth;
use Config;
use DB;
use DateTime;
use DateTimeZone;
use Mail;

class EmailController extends Controller
{
    private $helper;

    public function __construct(){

        $this->helper = new Common;
    }

    function contactUs(Request $request){
        try{
            $validation = Validator::make($request->all(),[
                'name' => 'required|max:255',
                'phone' => 'required|numeric',
                'email' => 'required|email|max:255',
                'message' => 'required',
            ]);
            if ($validation->fails()) { 
                return response()->json(['message'=>'failure','error'=>$validation->errors()], 422);            
            }else{
                $contact_us = new ContactUs;
                $contact_us->name    = $request->name;
                $contact_us->phone   = $request->phone;
                $contact_us->email   = $request->email;
                $contact_us->message = $request->message;
                $contact_us->save();
                return response()->json(["message"=>"Success","data"=>$contact_us],200);
                // "Submitted Succesfully - Thanks for contacting us!"
            }
                
        }catch (\Throwable $th) {
            return response(['message' => 'failure', 'error' => $th->getMessage()], 500);
        }

        
    }
}
