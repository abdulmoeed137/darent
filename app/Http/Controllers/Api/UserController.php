<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\EmailController;
use Illuminate\Http\Request;
use App\Http\Helpers\Common;

use Auth, Validator, Socialite, Mail, DateTime, Hash, Excel, DB, Image, Session;


use App\Models\{
    User,
    UserDetails,
    Messages,
    Country,
    PasswordResets,
    Payment,
    Notification,
    Timezone,
    Reviews,
    Accounts,
    UsersVerification,
    Properties,
    Payouts,
    Bookings,
    Currency,
    Settings,
    Wallet,
    Withdrawal
};


class UserController extends Controller
{
    protected $helper;

    public function __construct()
    {
        $this->helper = new Common;
    }

    public function dashboard()
    {
        try{
            $data['title'] = 'Dashboard';
        $user_id = Auth::user()->id;
        $data['wallet'] = wallet::where('user_id', $user_id)->first();
        $data['list'] = Properties::where('host_id', $user_id)->count();
        $data['trip'] = Bookings::where(['user_id' => $user_id, 'status' => 'Accepted'])->count();

        $bookings = Bookings::select('payment_method_id', 'currency_code',
            DB::raw('(total - service_charge - iva_tax - accomodation_tax) as total'), 'created_at', DB::raw('1 as type'))
            ->where(['host_id' => $user_id, 'status' => 'Accepted']);

        $trips = Bookings::select('payment_method_id', 'currency_code', 'total', 'created_at', DB::raw('-1 as type'))
            ->where(['user_id' => $user_id, 'status' => 'Accepted']);

        $data['transactions'] = Withdrawal::with('payment_methods','currency')
            ->select('payment_method_id', 'currency_id', 'amount', 'created_at', DB::raw('0 as type'))
            ->where(['user_id' => $user_id, 'status' => 'Success'])->union($bookings)->union($trips)
            ->orderBy('created_at', 'desc')->take(9)->get();

        $data['bookings'] = Bookings::with('users', 'properties')
            ->where(['host_id' => $user_id, 'status' => 'Accepted'])
            ->orderBy('id', 'desc')->take(5)->get();
        $data['currentCurrency'] = $this->helper->getCurrentCurrency();
        return response()->json(['message'=>'Success','data'=>$data],200);
        }
        catch (\Throwable $th) {
            return response(['message' => 'failure', 'error' => $th->getMessage()], 500);
        }
        

    }

    public function profile(Request $request, EmailController $email_controller)
    {
        try{
            $user = User::find(Auth::user()->id);
            // dd($user);
                    // if ($request->isMethod('post')) {
                        $rules = array(
                            'first_name'      => 'required|max:255',
                            'last_name'       => 'required|max:255',
                            'email'           => 'required|max:255|email|unique:users,email,'.Auth::user()->id,
                            'birthday_day'    => 'required',
                            'birthday_month'  => 'required',
                            'birthday_year'   => 'required',
                            // 'phone'           => 'required',
                        );
            
                        $messages = array(
                            'required'                => ':attribute is required.',
                            'birthday_day.required'   => 'Birth date field is required.',
                            'birthday_month.required' => 'Birth date field is required.',
                            'birthday_year.required'  => 'Birth date field is required.',
                        );
            
                        
            
                        $validator = Validator::make($request->all(), $rules, $messages);
                        // $validator->setAttributeNames($fieldNames);
            
                        if ($validator->fails()) {
                            return response()->json(['message'=>'failure','error'=>$validator->errors()],422);
                            // return back()->withErrors($validator)->withInput();
                        } else {
                            $new_email = ($user->email != $request->email) ? 'yes' : 'no';
            
                            $user->first_name      = $request->first_name;
                            $user->last_name       = $request->last_name;
                            $user->email           = $request->email;
                            $formattedPhone        = str_replace('+' . $request->carrier_code, "", $request->formatted_phone);
                            $user->phone           = !empty($request->phone) ? preg_replace("/[\s-]+/", "", $formattedPhone) : NULL;
                            $user->default_country = isset($request->default_country) ? $request->default_country : NULL;
                            $user->carrier_code    = isset($request->carrier_code) ? $request->carrier_code : NULL;
                            $user->formatted_phone = isset($request->formatted_phone) ? $request->formatted_phone : NULL;
                            $user->save();
            
                            $user_verification = UsersVerification::where('user_id', $user->id)->first();
                            $user_verification->email = 'no';
                            $user_verification->save();
            
                            $temp_details = $request->details;
                            $temp_details['date_of_birth'] = $request->birthday_year.'-'.$request->birthday_month.'-'.$request->birthday_day;
                            foreach ($temp_details as $key => $value) {
                                if (!is_null($value) && $value != '') {
                                    UserDetails::updateOrCreate(['user_id' => Auth::user()->id, 'field' => $key], ['value' => $value]);
                                }
                            }
            
                            if ($new_email == 'yes') {
                                $email_controller->change_email_confirmation($user);
                                return response()->json(["message"=>"Success","data"=>"Email Confirmation Success"],200);
            
                                // $this->helper->one_time_message('success', trans('messages.success.email_cofirmation_success'));
                            } else {
                                return response()->json(["message"=>"Success"],200);
            
                                // $this->helper->one_time_message('success', trans('messages.profile.profile_updated'));
                            }
                        }
                    
            
                    // $data['profile']   = User::find(Auth::user()->id);
            
                    // $data['timezone'] = Timezone::get()->pluck('zone', 'value');
                    
            
                    // $data['country'] = Country::get()->pluck('name', 'short_name');
            
            
                    // $data['details']   = $details = UserDetails::where('user_id', Auth::user()->id)->pluck('value', 'field')->toArray();
            
                    // if (isset($details['date_of_birth'])) {
                    //     $data['date_of_birth'] = explode('-', $details['date_of_birth']);
                    // } else {
                    //     $data['date_of_birth'] = [];
                    // }
            
                    // return response()->json(['message'=>'Success','data'=>$data],200);
                    // return view('users.profile', $data);
        }catch (\Throwable $th) {
            return response(['message' => 'failure', 'error' => $th->getMessage()], 500);
        }


       
    }

    public function media()
    {
       
        try{
            $data['result'] = $user = User::find(Auth::user()->id);
            // dd($_FILES["photos"]["name"]);

        if (isset($_FILES["photos"]["name"])) {
            foreach ($_FILES["photos"]["error"] as $key => $error) {
                $tmp_name     = $_FILES["photos"]["tmp_name"][$key];
                $name         = str_replace(' ', '_', $_FILES["photos"]["name"][$key]);
                $ext          = pathinfo($name, PATHINFO_EXTENSION);
                $name         = 'profile_'.time().'.'.$ext;
                $path         = 'public/images/profile/'.Auth::user()->id;
                $oldImagePath =  public_path('images/profile').'/'.Auth::user()->id.'/'.$data['result']->profile_image;
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }

                if ($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'gif') {
                    if(!empty($user->profile_image) && file_exists($oldImagePath)) {
                        unlink($oldImagePath);
                    }
                    if (move_uploaded_file($tmp_name, $path."/".$name)) {
                        $user->profile_image  = $name;
                        $user->save();
                        return response()->json(['message'=>'Success','data'=>"Uploaded"],200);

                        // $this->helper->one_time_message('success', trans('messages.users_media.uploaded'));
                    }
                }

            }
        }
        return response()->json(['message'=>'Success','data'=>$data],200);
        }
            catch (\Throwable $th) {
                return response(['message' => 'failure', 'error' => $th->getMessage()], 500);
            }


        // return view('users.media', $data);
    }
    public function security(Request $request)
    {
        if ($request->isMethod('post')) {
            $rules = array(
                'old_password'          => 'required',
                'new_password'          => 'required|min:6|max:30|different:old_password',
                'password_confirmation' => 'required|same:new_password|different:old_password'
            );

            $fieldNames = array(
                'old_password'          => 'Old Password',
                'new_password'          => 'New Password',
                'password_confirmation' => 'Confirm Password'
            );

            $validator = Validator::make($request->all(), $rules);
            $validator->setAttributeNames($fieldNames);

            if ($validator->fails()) {
                return response()->json(['message'=>'failure','error'=>$validator->errors(),'url'=>'return back'],422);
                // return back()->withErrors($validator)->withInput();
            } else {
                try{
                    $user = User::find(Auth::user()->id);

                    if (!Hash::check($request->old_password, $user->password)) {
                        return response()->json(['message'=>'failure','old_password'=> trans('messages.profile.pwd_not_correct')],422);
                        // return back()->withInput()->withErrors(['old_password' => trans('messages.profile.pwd_not_correct')]);
                    }
    
                    $user->password = bcrypt($request->new_password);
    
                    $user->save();
    
                    // $this->helper->one_time_message('success', trans('messages.profile.pwd_updated'));
                    return response()->json(["message"=>"Success","data"=>"password Updated"],200);
                // return redirect('users/security');

                }
                catch (\Throwable $th) {
                    return response(['message' => 'failure', 'error' => $th->getMessage()], 500);
                }
                
            }
        }
        return response()->json(['message'=>'Success','view'=>'account.security'],200);
        // return view('account.security');
    }

    public function verification(Request $request)
    {
        $data          = [];
        $data['title'] = 'Verify your account';
        return response()->json(['message'=>'Success','data'=>$data],200);
        // return view('users.verification', $data);
    }

    public function confirmEmail(Request $request)
    {
        try{
            $password_resets = PasswordResets::whereToken($request->code);

            if ($password_resets->count() && Auth::user()->email == $password_resets->first()->email) {
                $password_result = $password_resets->first();
                $datetime1 = new DateTime();
                $datetime2 = new DateTime($password_result->created_at);
                $interval  = $datetime1->diff($datetime2);
                $hours     = $interval->format('%h');
                if ($hours >= 1) {
                    $password_resets->delete();
                    $this->helper->one_time_message('success', trans('messages.login.token_expired'));
                    return response()->json(['message'=>'Success','url'=>'login'],200);
                    // return response()->redirect('login');
                }
                $data['result'] = User::whereEmail($password_result->email)->first();
                $data['token']  = $request->code;
                $user = User::find($data['result']->id);
                $user->status = "Active";
                $user->save();
                $user_verification = UsersVerification::where('user_id', $data['result']->id)->first();
    
                $user_verification->email = 'yes';
                $user_verification->save();
                $password_resets->delete();
    
                $this->helper->one_time_message('success', trans('messages.profile.email_confirmed'));
                return response()->json(['message'=>'Success','url'=>'dashboard'],200);

                // return redirect('dashboard');
            } else {
                $this->helper->one_time_message('success', trans('messages.login.invalid_token'));
                return response()->json(['message'=>'Success','url'=>'dashboard'],200);

                // return redirect('dashboard');
            }

        }
        catch (\Throwable $th) {
            return response(['status' => 'failure', "errors" => $th->getMessage()], 500);
        }
       
    }
    public function newConfirmEmail(Request $request, EmailController $emailController)
    {
        try{
            $userInfo = User::find(Auth::user()->id);

            $emailController->new_email_confirmation($userInfo);
    
            $this->helper->one_time_message('success', trans('messages.profile.new_confirm_link_sent', ['email'=>$userInfo->email]));
            if ($request->redirect == 'verification') {
                return response()->json(['message'=>'Success','url'=>'users/edit-verification'],200);

                // return redirect('users/edit-verification');
            } else {
                return response()->json(['message'=>'Success','url'=>'dashboard'],200);

                // return redirect('dashboard');
            }
        }
        catch (\Throwable $th) {
            return response(['status' => 'failure', "errors" => $th->getMessage()], 500);
        }
        
    }

    public function reviews(Request $request)
    {
        try{
            $data['title'] = "Reviews";
            $data['reviewsAboutYou'] = Reviews::where('receiver_id', Auth::user()->id)
            ->orderBy('id', 'desc')
            ->get();
        return response()->json(['message'=>'Success','data'=>$data],200);

        }catch (\Throwable $th) {
            return response(['status' => 'failure', "errors" => $th->getMessage()], 500);
        }
        
    }
    public function reviewsByYou(Request $request)
    {
        try{
            $data['title'] = "Reviews";
        $data['reviewsByYou'] = Reviews::with('properties','bookings')->where('sender_id', Auth::user()->id)
                                ->orderBy('id', 'desc')
                                ->paginate(Session::get('row_per_page'), ['*'], 'you');

        $data['reviewsToWrite'] = Bookings::with('properties','host','users')->whereRaw('DATEDIFF(now(),end_date) <= 14')
            ->whereRaw('DATEDIFF(now(),end_date)>=1')
            ->where('status', 'Accepted')
            ->where(function ($query) {
                return $query->where('user_id', Auth::id())->orWhere('host_id', Auth::id());
            })
            ->whereDoesntHave('reviews')->paginate(Session::get('row_per_page'), ['*'], 'write');

        $data['expiredReviews'] = Bookings::with(['reviews'])->whereRaw('DATEDIFF(now(),end_date) > 14')->where('status', 'Accepted')->where(function ($query) {
            return $query->where('user_id', Auth::user()->id)->orWhere('host_id', Auth::user()->id);
        })->has('reviews', '<', 1)->paginate(Session::get('row_per_page'), ['*'], 'expired');

        if ($request->expired) {
            $data['expired'] = 'active';
        } elseif ($request->you){
            $data['you'] = 'active';
        } else {
            $data['write'] = 'active';
        }

        return response()->json(['message'=>'Success','data'=>$data],200);

        }
        catch (\Throwable $th) {
            return response(['status' => 'failure', "errors" => $th->getMessage()], 500);
        }
        
    }

}
