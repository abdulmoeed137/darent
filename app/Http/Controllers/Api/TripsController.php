<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Helpers\Common;


use View;
use DateTime;
use Auth;
use DB;
use Session;
use Carbon\Carbon;

use App\Models\{
    Bookings,
    BookingDetails,
    Messages,
    Penalty,
    Payouts,
    Properties,
    PayoutPenalties,
    PropertyDates,
    PropertyFees,
    Settings
};

class TripsController extends Controller
{
    private $helper;

    public function __construct()
    {
        $this->helper = new Common;
    }
    public function myTrips(Request $request)
    {
        switch ($request->status) {
            case 'Expired':
                $params  = [['created_at', '<', Carbon::yesterday()], ['status', '!=', 'Accepted']];
                break;
            case 'Current':
                $params  = [['start_date', '<=', date('Y-m-d')], ['end_date', '>=', date('Y-m-d')],['status', 'Accepted']];
                break;
            case 'Upcoming':
                $params  = [['start_date', '>', date('Y-m-d')], ['status', 'Accepted']];
                break;
            case 'Completed':
                $params  = [['end_date', '<', date('Y-m-d')],['status', 'Accepted']];
                break;
            case 'Pending':
                $params           = [['created_at', '>', Carbon::yesterday()], ['status', $request->status]];
                break;
            case 'Unpaid':
                $params           = [['created_at', '>', Carbon::yesterday()], ['status', $request->status]];
                break;
            default:
                $params           = [];
                break;
        }
        $data['yesterday'] = Carbon::yesterday();
        $data['status']    = $request->status;
        $data['bookings']  = Bookings::with('host','properties')
            ->where('user_id', Auth::user()->id)
            ->where($params)->orderBy('id', 'desc')
            ->paginate(Session::get('row_per_page'));

            return response()->json(['message'=>'Success','data'=>$data],200);

    }
}
