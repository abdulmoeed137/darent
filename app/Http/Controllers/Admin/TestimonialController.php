<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\TestimonialsDataTable;
use App\Http\Controllers\Controller;
use App\Http\Helpers\Common;
use App\Models\Amenities;
use App\Models\AmenityType;
use App\Models\Testimonials;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TestimonialController extends Controller
{
    protected $helper;

    public function __construct()
    {
        $this->helper = new Common;
    }

    public function index(TestimonialsDataTable $dataTable)
    {
        return $dataTable->render('admin.testimonial.view');
    }

    public function add(Request $request)
    {
        
        $info = $_POST;
        
        if (! $info) {
            
            return view('admin.testimonial.add');
        } elseif ($info) {
            $lastid = Testimonials::select('id')->orderBy('id','desc')->first();
            
            // dd($request);
            $rules = array(
                    'name'           => 'required|max:50',
                    'designation'    => 'required|max:50',
                    'description'    => 'required|max:100',
                    'rating_1'         => 'required',
                    'status'         => 'required',
                    'image' => 'mimes:jpeg,jpg,png|required'


                    );

            $fieldNames = array(
                        'name'             => 'Name',
                        'designation'             => 'Designation',
                        'description'       => 'Description',
                        'rating_1'       => 'Review',
                        'status'       => 'Status',
                        'image'            => 'Image'
                        );

            $validator = Validator::make($request->all(), $rules);
            $validator->setAttributeNames($fieldNames);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } else {
                
                if(isset($lastid)){
                    $lastidincrement = $lastid->id + 1; 
                }else{
                    $lastidincrement = 1;
                    
                }
                
                $testimonial = new Testimonials();
                $testimonial->name            = $request->name;
                $testimonial->designation            = $request->designation;
                $filename = 'testimonial_'.$lastidincrement.'.jpg';
                $destinationPath = public_path().'/front/images/testimonial' ;
                $request->image->move($destinationPath,$filename);
                $testimonial->image = str_replace('public', 'storage', $filename);
                $testimonial->description    = $request->description;
                $testimonial->review           = $request->rating_1;
                $testimonial->status         = $request->status;
                $testimonial->save();
                $this->helper->one_time_message('success', 'Added Successfully');
                return redirect('admin/testimonials');
            }
        }
    }

    public function update(Request $request)
    {
        
        $info = $_POST;
        if (! $info) {
            $data['result'] = Testimonials::find($request->id);
            return view('admin.testimonial.edit', $data);
        } elseif ($info) {
            $rules = array(
                    'name'           => 'required|max:50',
                    'designation'    => 'required|max:50',
                    'description'    => 'required|max:100',
                    'rating_1'         => 'required',
                    'status'         => 'required',
                    'image' => 'mimes:jpeg,jpg,png|required'


                    );

            $fieldNames = array(
                        'name'             => 'Name',
                        'designation'             => 'Designation',
                        'description'       => 'Description',
                        'rating_1'       => 'Review',
                        'status'       => 'Status',
                        'image'            => 'Image'
                        );

            $validator = Validator::make($request->all(), $rules);
            $validator->setAttributeNames($fieldNames);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } else {
                $testimonial = Testimonials::find($request->id);
                if($request->hasFile('image')){
                    $destinationPath = public_path().'/front/images/testimonial' ;
                    $unlinkexistingfile = $destinationPath.'/'.$testimonial->image;
                //    dd($unlinkexistingfile);
                
                // dd();
                    if(file_exists($unlinkexistingfile)){
                        unlink($unlinkexistingfile);
                    }

                    $request->image->move($destinationPath,$testimonial->image);

                }
                $testimonial->name           = $request->name;
                $testimonial->designation    = $request->designation;
                $testimonial->description    = $request->description;
                $testimonial->review         = $request->rating_1;
                $testimonial->status         = $request->status;
                $testimonial->save();
                $this->helper->one_time_message('success', 'Added Successfully');
                return redirect('admin/testimonials');
            }
        }
    }

    public function delete(Request $request)
    {
        Testimonials::find($request->id)->delete();
        $this->helper->one_time_message('success', 'Deleted Successfully');
        return redirect('admin/testimonials');
    }
}