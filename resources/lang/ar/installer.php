<?php

return [

    /**
     *
     * Welcome page.
     *
     */
    'welcome' => [
        'name' => 'الاسم',
        'version' => 'النسخة 3.1',
        'title' => 'العناون',
        'sub-title' => 'العنوان الفرعي',
        'item1' => 'الصنف 1',
        'item2' => 'الصنف 2',
        'item3' => 'الصنف 3',
        'item4' => 'الصنف 3',
        'message' => 'الرسالة',
        'button' => 'زر',
    ],

    /**
     *
     * Database page.
     *
     */
    'database' => [
        'title' => 'العنوان',
        'sub-title' => 'العنوان الفرعي',
        'dbname-label' => 'الاسم',
        'username-label' => 'اسم المستخدم',
        'password-label' => 'كلمة المرور',
        'host-label' => 'العنوان',
        'button' => 'زر',
        'wait' => 'انتظار',
    ],

    /**
     *
     * Database error page.
     *
     */
    'database-error' => [
        'title' => 'العنوان',
        'sub-title' => 'العنوان الفرعي',
        'item1' => 'الصنف 1',
        'item2' => 'الصنف 2',
        'item3' => 'الصنف 3',
        'message' => 'الرسالة',
        'button' => 'زر',
    ],

    /**
     *
     * Requirement error page.
     *
     */
    'requirement-error' => [
        'title' => 'العنوان',
        'requirement' => 'المتطلبات',
        'php-version' => '',
        'message' => 'الرسالة'
    ],

    /**
     *
     * Permission error page.
     *
     */
    'permission-error' => [
        'title' => 'العنوان',
        'sub-title' => 'العنوان الفرعي',
        'message' => 'الرسالة'
    ],

    /**
     *
     * Register page.
     *
     */
    'register' => [
        'title' => 'العناون',
        'sub-title' => 'العنوان الفرعي',
        'base-label' => '',
        'message' => 'الرسالة',
        'button' => 'زر',
    ],

    /**
     *
     * Register fields for labels.
     *
     */
    'register-fields' => [
        'name' => 'الاسم',
        'email' => 'عنوان البريد الإلكتروني',
        'password' => 'كلمة المرور',
    ],

    /**
     *
     * End page.
     *
     */
    'end' => [
        'title' => 'العنوان',
        'button' => 'زر',
    ],

];
