<!--================ Header Menu Area start =================-->
<?php
$lang = Session::get('language');

use App\Models\Settings;

?>
<link href="//db.onlinewebfonts.com/c/66108f52b32320faf592153ea557ef07?family=DIN+Next+LT+Arabic" rel="stylesheet"
	type="text/css" />
<style>
	@import url(//db.onlinewebfonts.com/c/66108f52b32320faf592153ea557ef07?family=DIN+Next+LT+Arabic);

	@font-face {
		font-family: "DIN Next LT Arabic";
		src: url("//db.onlinewebfonts.com/t/66108f52b32320faf592153ea557ef07.eot");
		src: url("//db.onlinewebfonts.com/t/66108f52b32320faf592153ea557ef07.eot?#iefix") format("embedded-opentype"), url("//db.onlinewebfonts.com/t/66108f52b32320faf592153ea557ef07.woff2") format("woff2"), url("//db.onlinewebfonts.com/t/66108f52b32320faf592153ea557ef07.woff") format("woff"), url("//db.onlinewebfonts.com/t/66108f52b32320faf592153ea557ef07.ttf") format("truetype"), url("//db.onlinewebfonts.com/t/66108f52b32320faf592153ea557ef07.svg#DIN Next LT Arabic") format("svg");
	}

	body {
		font-family: DIN Next LT Arabic;
	}

	select#cars {
		border: 0px;
		padding: 0px;
		margin-right: 25px;
	}

	select#cars:focus {
		outline: 0px;

	}

	.modal-backdrop {
		width: unset !important;
	}

	button.btnbtn {
		color: #2B2B2B;
		text-transform: uppercase;
		font-weight: 700;
		padding: 2.2rem 0;
		display: inline-block;
		font-size: 1.4rem;
		background: 0px;
		border: 0px;
		margin-left: 27px;
	}

	@media (max-width: 991px) {
		button.btnbtn {
			display: none;
		}

		.res-hdn {
			display: none !important;
		}
	}

	.form-control {
		display: block;
		width: 100%;
		padding: 0.48rem 0.81rem;
		font-size: 1.6rem;
		line-height: 1.5;
		color: #495057;
		background-color: #fff;
		background-clip: padding-box;
		border: 1px solid #ced4da;
		border-radius: 0.31rem;
		transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
	}

	button.btn {
		border: 1px solid transparent;
		color: #212529;
	}

	a.btn,
	button.btn.vbtn-outline-success.listing-btn {
		color: #fcb23e;
		border-color: #fcb23e !important;
		background-color: transparent;
	}

	.success-text {
		color: #fdb040 !important;
	}

	a.vbtn-outline-success:hover,
	.vbtn-outline-success:hover {
		color: #ffffff !important;
		border-color: #fdb041 !important;
		background: #fdb956 !important;
	}


	button.btn.vbtn-default.btn-block.listing-btn:hover {
		color: #fdb041 !important;
		background-color: white !important;
		border: 1px solid #fdb041;
	}

	button.btn.vbtn-outline-success.listing-btn:hover {
		color: white !important;
	}

	.secondary-text-color {
		color: #fdb041 !important;
	}

	.dropdown-toggle::after {
		color: white !important;
	}

	.secondary-bg {
		background: #fcb23e !important;
		color: #fff;
	}

	a:hover {
		text-decoration: none;
		color: #fdb750 !important;
	}

	.dropdown-menu.show {
		text-align: center;
		margin-top: -1px;
	}

	button#dropdownMenuButton {
		margin-right: 0 !important;
	}

	button.vbtn-success:hover {
		background: #fdb040;
		color: white;
	}
</style>
<input type="hidden" id="front_date_format_type" value="{{ Session::get('front_date_format_type') }}">
<header class="header_area  animated fadeIn">
	<div class="main_menu">
		<nav class="navbar-expand-lg navbar-light header-custom">
			<div class="container-fluid container-fluid-90 header-space">
				<div class="row">
					<div class="col-lg-3 col-md-6 col-6">
						<a class="navbar-brand logo_h" aria-label="logo" href="{{ url('/') }}"><img
								src="{{ $logo ?? '' }}" alt="logo" class="img-130x32"></a>
					</div>
					<div class="col-lg-6">
						<ul class="head-menu">
							<li>
								<a href="{{ url('/') }}">{{ trans('messages.header.home') }}</a>
							</li>
							<li>
								@if (Request::segment(1) != 'help')
								<a class="nav-link p-0" href="{{ url('property/create') }}"
									aria-label="property-create">
									<p class="mb-0"> {{ trans('messages.header.list_space') }}</p>
								</a>
								@endif
							</li>
							<li>
								<a href="{{ url('emails/contact_us') }}" class="">{{ trans('messages.header.contact_us')
									}}</a>
							</li>
						</ul>
					</div>
					<div class="col-lg-3 col-md-6 col-6">
						<div class=" navbar-collapse offset lang-res" id="navbarSupportedContent">
							<div class="login-btn">
								@if (!Auth::check())
								{{-- <div class="nav-item">
									<a class="nav-link" href="{{ url('signup') }}" aria-label="signup">{{
										trans('messages.sign_up.sign_up') }}</a>
								</div> --}}

								<!-- Button trigger modal -->
								<button style="" type="button" class="head-btn" data-toggle="modal"
									data-target="#exampleModalCenterone">
									{{ trans('messages.sign_up.sign_up') }}
								</button>

								<!-- Modal -->
								<div class="modal fade over-hdn" id="exampleModalCenterone" tabindex="-1" role="dialog"
									aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
									<div style="margin-top:65px;" class="modal-dialog modal-dialog-centered"
										role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLongTitle">
													{{ trans('messages.sign_up.sign_up') }}</h5>
												<button type="button" class="close" data-dismiss="modal"
													aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div style="padding:10px 19px" class="modal-body">
												<div class="">

													<?php
														$googlelogin = Settings::getAll()->where('name','google_login')->where('value','1')->pluck('value','name');
														// dump(count($googlelogin));
														if(count($googlelogin) > 0){
															?>
													<a href="googleLogin">
														<button
															class="btn vbtn-danger mb-4 pt-3 pb-3 text-16 w-100 mt-3">
															<span><i class="fab fa-google-plus-g  mr-2 text-16"></i>
																Sign up
																with Google</span>
														</button>
													</a>
													<?php	}
													?>
													{{-- @if(isset($googlelogin))

													@endif --}}


													<form id="signup_form" name="signup_form" method="post"
														action="{{ url('/userregister') }}"
														class="signup-form login-form" accept-charset="UTF-8">
														<input type="hidden" name="_token" value="{{ csrf_token() }}">
														<!--<input type="hidden" name="_token" value="tPDKJx8RvD57SVTdA6EFoShwtmF1kC6zpXIeHVQu">-->
														<div class="row text-16">
															<input type="hidden" name="email_signup" id="form">
															<input type="hidden" name="default_country"
																id="default_country" class="form-control">
															<input type="hidden" name="carrier_code" id="carrier_code"
																class="form-control">
															<input type="hidden" name="formatted_phone"
																id="formatted_phone" class="form-control">

															<div class="form-group col-sm-12 p-0">
																<label for="first_name">{{
																	trans('messages.sign_up.first_name') }} <span
																		class="text-13 text-danger">*</span></label>
																@if ($errors->has('first_name')) <p class="error-tag">{{
																	$errors->first('first_name') }}</p> @endif
																<input type="text" class='form-control text-14 p-2'
																	value="{{ old('first_name') }}" name='first_name'
																	id='first_name'
																	placeholder="{{ trans('messages.sign_up.first_name') }}">
															</div>

															<div class="form-group col-sm-12 p-0">
																<label
																	for="last_name">{{trans('messages.sign_up.last_name')}}
																	<span class="text-13 text-danger">*</span></label>
																@if ( $errors->has('last_name') ) <p class="error-tag">
																	{{ $errors->first('last_name') }}</p> @endif
																<input type="text" class='form-control text-14 p-2'
																	value="{{ old('last_name') }}" name='last_name'
																	id='last_name'
																	placeholder="{{ trans('messages.sign_up.last_name') }}">
															</div>

															<div class="form-group col-sm-12 p-0">
																<label for="email">{{ trans('messages.login.email') }}
																	<span class="text-13 text-danger">*</span></label>
																<input type="text" class='form-control text-14 p-2'
																	value="{{old('email')}}" name='email' id='email'
																	placeholder="{{ trans('messages.login.email') }}">
																@if ($errors->has('email'))
																<p class="error-tag">
																	{{ $errors->first('email') }}
																</p>
																@endif
																<div id="emailError"></div>
															</div>

															<div class="form-group col-sm-12 p-0">
																<label for="phone">{{
																	trans('messages.users_profile.phone') }}</label>
																<input type="tel" class="form-control text-14 p-2"
																	id="phone" name="phone">
																<span id="tel-error" class="text-13 text-danger"></span>
																<span id="phone-error"
																	class="text-13 text-danger"></span>
															</div>

															<div class="form-group col-sm-12 p-0">
																<label for="password">{{
																	trans('messages.login.password') }} <span
																		class="text-13 text-danger">*</span></label>
																@if ( $errors->has('password') ) <p class="error-tag">{{
																	$errors->first('password') }}</p> @endif
																<input type="password" class='form-control text-14 p-2'
																	name='password' id='password' placeholder="{{ trans('messages.login.password')
																	}}">
															</div>

															<div class="col-sm-12 p-0">
																<label class="l-pad-none text-14">{{
																	trans('messages.sign_up.birth_day') }} <span
																		class="text-13 text-danger">*</span></label>
															</div>

															<div class="col-sm-12 p-0">
																@if ($errors->has('birthday_month') ||
																$errors->has('birthday_day') ||
																$errors->has('birthday_year'))
																<p class="error-tag">{{ $errors->first('date_of_birth')
																	}}</p>
																@else
																<p class="error-tag">{{ $errors->first('date_of_birth')
																	}}</p>
																@endif
															</div>


															<div class="form-group col-sm-12 p-0">
																<div class="row">
																	<div class="col-sm-4 pl-0 mt-2">
																		<select name='birthday_month'
																			class='form-control text-14 p-2'
																			id='user_birthday_month'>
																			<option value=''>{{
																				trans('messages.sign_up.month') }}
																			</option>
																			@for($m=1; $m<=12; ++$m) <option
																				value="{{ $m }}"
																				{{old('birthday_month')==$m?'selected="selected"':''}}>{{date('
																				F', mktime(0, 0, 0, $m, 1))}}</option>
																				@endfor
																		</select>
																	</div>

																	<div class="col-sm-4 mt-2">
																		<select name='birthday_day'
																			class='form-control text-14'
																			id='user_birthday_day'>
																			<option value=''>
																				{{trans('messages.sign_up.day')}}
																			</option>
																			@for($m=1; $m<=31; ++$m) <option
																				value="{{$m}}"
																				{{old('birthday_day')==$m?'selected="selected"':''}}>{{$m}}</option>
																				@endfor
																			</select>
																		</div>
									
																		<div class="col-sm-4 pr-0 mt-2">
																			<select name=' birthday_year' class='form-control text-14' id='user_birthday_year'>
																				<option value=''>{{
																					trans('messages.sign_up.year') }}
																				</option>
																				@for($m=date('Y'); $m > date('Y')-100;
																				$m--)
																				<option value="{{ $m }}"
																					{{old('birthday_year')==$m?'selected="selected"':''}}>{{ $m }}</option>
																				@endfor
																			</select>
																		</div>
																	</div>
									
																<span class="text-danger text-13">
																	<label id=' dobError'></label>
																					</span>
																	</div>

																	<button type="submit" id="btn"
																		class="btn pb-3 pt-3 text-15 button-reactangular vbtn-outline-success w-100 ml-0 mr-0 mb-3">
																		<i
																			class="spinner fa fa-spinner fa-spin d-none"></i>
																		<span id="btn_next-text">{{
																			trans('messages.sign_up.sign_up') }}</span>
																	</button>
																</div>
																<div class="text-14">
																	Already an Darent member?
																	<a href="login?" class="font-weight-600">
																		{{ trans('messages.sign_up.login') }}
																	</a>
																</div><a href="login?" class="font-weight-600">
																</a></br>
																<input type="checkbox" required>
																<a href="{{route('privacyPolicy')}}">Accept Terms &
																	Policy</a>

													</form>


												</div>
											</div>
										</div>
									</div>
								</div>
								<!--<div class="nav-item">
          <a style="margin-left:30px" class="nav-link" href="{{ url('login') }}" aria-label="login">{{ trans('messages.sign_up.login') }}</a>
         </div>-->
								<!-- Button trigger modal -->
								<button type="button" class="head-btn h-login-btn" data-toggle="modal"
									data-target="#exampleModalCentertwo">
									{{ trans('messages.sign_up.login') }}
								</button>

								<!-- Modal -->
								<div class="modal fade over-hdn" id="exampleModalCentertwo" tabindex="-1" role="dialog"
									aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
									<div style="margin-top:65px;" class="modal-dialog modal-dialog-centered"
										role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLongTitle">Login </h5>
												<button type="button" class="close" data-dismiss="modal"
													aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div style="padding: 10px 19px;" class="modal-body">
												<div class=" ">
													{{-- <a href="http://360techsolution.com/darent/facebookLogin">
														<button class="btn btn-outline-primary pt-3 pb-3 text-16 w-100">
															<span><i class="fab fa-facebook-f mr-2 text-16"></i> Sign up
																with Facebook</span>
														</button>
													</a> --}}

													<a href="googleLogin">
														<button
															class="btn vbtn-danger mb-4 pt-3 pb-3 text-16 w-100 mt-3">
															<span><i class="fab fa-google-plus-g  mr-2 text-16"></i>
																Sign
																up with Google</span>
														</button>
													</a>


													<form id="login_form" method="post"
														action="{{ url('authenticate') }}" accept-charset="UTF-8"
														novalidate="novalidate">
														<input type="hidden" name="_token" value="{{ csrf_token() }}">
														<!--<input type="hidden" name="_token" value="tPDKJx8RvD57SVTdA6EFoShwtmF1kC6zpXIeHVQu">-->
														<div class="form-group col-sm-12 p-0">
															<label for="first_name">Email <span
																	class="text-13 text-danger">*</span></label>
															<input type="email" class="form-control text-14" value=""
																name="email" placeholder="Email">
														</div>

														<div class="form-group col-sm-12 p-0">
															<label for="first_name">Password <span
																	class="text-13 text-danger">*</span></label>
															<input type="password" class="form-control text-14" value=""
																name="password" placeholder="Password">
														</div>

														<div class="form-group col-sm-12 p-0 mt-3">
															<div class="d-flex justify-content-between">
																<div class="m-3 text-14">
																	<input type="checkbox" class="remember_me"
																		id="remember_me2" name="remember_me" value="1">
																	Remember me
																</div>

																<div class="m-3 text-14">
																	<a href="forgot_password"
																		class="forgot-password text-right">Forgot
																		password?</a>
																</div>
															</div>
														</div>

														<div class="form-group col-sm-12 p-0">
															<button type="submit" id="btn"
																class="btn pb-3 pt-3  button-reactangular text-15 vbtn-outline-success w-100 rounded">
																<i class="spinner fa fa-spinner fa-spin d-none"></i>
																<span id="btn_next-text">Login</span>
															</button>
														</div>
													</form>

													<div class="mt-3 text-14">
														Don’t have an account?
														<a href="signup" class="font-weight-600">
															Register
														</a>
													</div>
												</div>
											</div>

										</div>
									</div>
								</div>
								@else
								<div class="d-flex">
									<div>
										<!--<div class="nav-item mr-0">
           <img src="{{ Auth::user()->profile_src }}" class="head_avatar" alt="{{ Auth::user()->first_name }}">
          </div>-->
									</div>
									<div>
										<div class="nav-item ml-0 pl-0">
											<div class="dropdown">
												<a href="javascript:void(0)"
													class="nav-link dropdown-toggle text-15 user-name" type="button"
													id="dropdownMenuButton" data-toggle="dropdown"
													aria-label="user-profile" aria-haspopup="true"
													aria-expanded="false">
													{{ Auth::user()->first_name }}
												</a>
												<div class="dropdown-menu drop-down-menu-left p-0 drop-width text-14"
													aria-labelledby="dropdownMenuButton">
													<a class="vbg-default-hover border-0  font-weight-700 list-group-item vbg-default-hover border-0"
														href="{{ url('dashboard') }}" aria-label="dashboard">{{
														trans('messages.header.dashboard') }}</a>
													<a class="font-weight-700 list-group-item vbg-default-hover border-0 "
														href="{{ url('users/profile') }}" aria-label="profile">{{
														trans('messages.utility.profile') }}</a>
													<a class="font-weight-700 list-group-item vbg-default-hover border-0 "
														href="{{ url('logout') }}" aria-label="logout">{{
														trans('messages.header.logout') }}</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								@endif
							</div>
							<div class="dropdown">
								<button type="button" class="lang-btn btn" id="dropdownMenuButton"
									data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<span>
										EN
										<i class="fa fa-angle-down"></i>
									</span>
								</button>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									<a href="javascript:void(0)" class="language_footer " data-lang="en">
										<font style="vertical-align: inherit;">
											<font style="vertical-align: inherit;">EN</font>
										</font>
									</a><br>
									<a href="javascript:void(0)" class="language_footer " data-lang="ar">عربى</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Trigger Button -->
				<a href="#" aria-label="navbar" class="navbar-toggler ham-custom" data-toggle="modal"
					data-target="#left_modal">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
			</div>
		</nav>
	</div>
</header>

<!-- Modal Window -->
<div class="modal left fade" id="left_modal" tabindex="-1" role="dialog" aria-labelledby="left_modal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header border-0 secondary-bg">
				@if (Auth::check())
				<div class="row justify-content-center">
					<div>
						<img src="{{ Auth::user()->profile_src }}" class="head_avatar"
							alt="{{ Auth::user()->first_name }}">
					</div>

					<div>
						<p class="text-white mt-4"> {{ Auth::user()->first_name }}</p>
					</div>
				</div>
				@endif

				<button type="button" class="close text-28" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body text-right-rtl">
				<ul class="mobile-side">
					<div class="mbl-btn">
						<a href="{{ url('emails/contact_us') }}"
							class="btn vbtn-outline-success text-14 font-weight-700 header-btn">{{
							trans('messages.header.contact_us') }}</a>
						@if (Request::segment(1) != 'help')
						<a class="p-0" href="{{ url('property/create') }}">
							<button class="btn vbtn-outline-success text-14 font-weight-700 pl-5 pr-5 pt-3 pb-3">
								{{ trans('messages.header.list_space') }}
							</button>
						</a>
					</div>
					@endif
					@if (Auth::check())
					<li><a href="{{ url('dashboard') }}"><i class="fa fa-tachometer-alt mr-3"></i>{{
							trans('messages.header.dashboard') }}</a>
					</li>
					<li><a href="{{ url('inbox') }}" class="d-flex justify-content-between align-items-center">
							<div><i class="fas fa-inbox mr-3"></i>{{ trans('messages.header.inbox') }}</div>
							@php
							$count = getInboxUnreadCount();
							@endphp
							@if ($count)
							<span class="badge badge-danger rounded-circle mr-2 text-12">{{ $count }}</span>
							@endif
						</a></li>
					<li><a href="{{ url('properties') }}"><i class="far fa-list-alt mr-3"></i>{{
							trans('messages.header.your_listing') }}</a>
					</li>
					<li><a href="{{ url('my-bookings') }}"><i class="fa fa-bookmark mr-3"></i>{{
							trans('messages.booking_my.booking') }}</a>
					</li>
					<li><a href="{{ url('trips/active') }}"><i class="fa fa-suitcase mr-3"></i>
							{{ trans('messages.header.your_trip') }}</a></li>
					<li><a href="{{ url('user/favourite') }}"><i class="fas fa-heart mr-3"></i>
							{{ trans('messages.users_dashboard.favourite') }}</a></li>
					<li><a href="{{ url('users/payout-list') }}"><i class="far fa-credit-card mr-3"></i>
							{{ trans('messages.sidenav.payouts') }}</a></li>
					<li><a href="{{ url('users/transaction-history') }}"><i
								class="fas fa-money-check-alt mr-3 text-14"></i>
							{{ trans('messages.account_transaction.transaction') }}</a></li>
					<li><a href="{{ url('users/profile') }}"><i class="far fa-user-circle mr-3"></i>{{
							trans('messages.utility.profile') }}</a>
					</li>
					<a data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false"
						aria-controls="collapseExample">
						<li><i class="fas fa-user-edit mr-3"></i>{{ trans('messages.sidenav.reviews') }}</li>
					</a>

					<div class="collapse" id="collapseExample">
						<ul class="ml-4">
							<li><a href="{{ url('users/reviews') }}" class="text-14">{{
									trans('messages.reviews.reviews_about_you') }}</a></li>
							<li><a href="{{ url('users/reviews_by_you') }}" class="text-14">{{
									trans('messages.reviews.reviews_by_you') }}</a></li>
						</ul>
					</div>
					<li><a href="{{ url('logout') }}"><i class="fas fa-sign-out-alt mr-3"></i>{{
							trans('messages.header.logout') }}</a>
					</li>
					@else
					<li><a href="{{ url('signup') }}"><i class="fas fa-stream mr-3"></i>{{
							trans('messages.sign_up.sign_up') }}</a></li>
					<li><a href="{{ url('login') }}"><i class="far fa-list-alt mr-3"></i>{{
							trans('messages.header.login') }}</a></li>
					@endif


				</ul>
			</div>
		</div>
	</div>
</div>
<!--================Header Menu Area =================-->