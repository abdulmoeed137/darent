@extends('template')
@push('css')
<link rel="stylesheet" type="text/css"
	href="{{ asset('public/js/intl-tel-input-13.0.0/build/css/intlTelInput.min.css')}}">
@endpush
@section('main')
<div class="container mb-4 margin-top-85 min-height">
	<div class="d-flex justify-content-center">
		<div class="p-5 mt-5 mb-5 border w-450">
			{{-- @if($social['facebook_login'])
			<a href="{{ isset($facebook_url) ? $facebook_url:URL::to('facebookLogin') }}">
				<button class="btn btn-outline-primary pt-3 pb-3 text-16 w-100">
					<span><i class="fab fa-facebook-f mr-2 text-16"></i>
						{{trans('messages.sign_up.sign_up_with_facebook')}}</span>
				</button>
			</a>
			@endif --}}

			@if(isset($social['google_login']))
			<a href="{{URL::to('googleLogin')}}">
				<button class="btn btn-outline-danger pt-3 pb-3 text-16 w-100 mt-3">
					<span><i class="fab fa-google-plus-g  mr-2 text-16"></i>
						{{trans('messages.sign_up.sign_up_with_google')}}</span>
				</button>
			</a>
			@endif

			{{-- @if(isset($social['google_login']) || $social['facebook_login'])
			<p class="text-center font-weight-700 mt-1">{{trans('messages.login.or')}}</p>
			@endif --}}
			<form id="signup_form" name="signup_form" method="post" action="{{url('/userregister')}}"
				class='signup-form login-form' accept-charset='UTF-8' onsubmit="return ageValidate();">
				{{ csrf_field() }}
				<div class="row text-16">
					<input type="hidden" name='email_signup' id='form'>
					<input type="hidden" name="default_country" id="default_country" class="form-control">
					<input type="hidden" name="carrier_code" id="carrier_code" class="form-control">
					<input type="hidden" name="formatted_phone" id="formatted_phone" class="form-control">

					<div class="form-group col-sm-12 p-0">
						<label for="first_name">{{trans('messages.sign_up.first_name') }} <span
								class="text-13 text-danger">*</span></label>
						@if ($errors->has('first_name')) <p class="error-tag">{{ $errors->first('first_name') }}</p>
						@endif
						<input type="text" class='form-control text-14 p-2' value="{{ old('first_name') }}"
							name='first_name' id='first_name' placeholder="{{trans('messages.sign_up.first_name') }}">
					</div>

					<div class="form-group col-sm-12 p-0">
						<label for="last_name">{{trans('messages.sign_up.last_name') }} <span
								class="text-13 text-danger">*</span></label>
						@if ( $errors->has('last_name') ) <p class="error-tag">{{ $errors->first('last_name') }}</p>
						@endif
						<input type="text" class='form-control text-14 p-2' value="{{ old('last_name') }}"
							name='last_name' id='last_name' placeholder="{{trans('messages.sign_up.last_name') }}">
					</div>

					<div class="form-group col-sm-12 p-0">
						<label for="first_name">{{trans('messages.login.email') }} <span
								class="text-13 text-danger">*</span></label>
						<input type="text" class='form-control text-14 p-2' value="{{old('email')}}" name='email'
							id='email' placeholder="{{trans('messages.login.email') }}">
						@if ($errors->has('email'))
						<p class="error-tag">
							{{$errors->first('email') }}
						</p>
						@endif
						<div id="emailError"></div>
					</div>

					<div class="form-group col-sm-12 p-0">
						<label for="first_name">{{ trans('messages.users_profile.phone') }}</label>
						<input type="tel" class="form-control text-14 p-2" value="{{ old('phone') }}" id="phone"
							name="phone">
						<span id="tel-error" class="text-13 text-danger"></span>
						<span id="phone-error" class="text-13 text-danger"></span>
					</div>

					<div class="form-group col-sm-12 p-0">
						<label for="first_name">{{ trans('messages.login.password') }} <span
								class="text-13 text-danger">*</span></label>
						@if ( $errors->has('password') ) <p class="error-tag">{{ $errors->first('password') }}</p>
						@endif
						<input type="password" class='form-control text-14 p-2' name='password' id='password'
							placeholder="{{ trans('messages.login.password') }}">
					</div>

					<div class="col-sm-12 p-0">
						<label class="l-pad-none text-14">{{ trans('messages.sign_up.birth_day') }} <span
								class="text-13 text-danger">*</span></label>
					</div>

					<div class="col-sm-12 p-0">
						@if ($errors->has('birthday_month') || $errors->has('birthday_day') ||
						$errors->has('birthday_year'))
						<p class="error-tag">{{ $errors->first('date_of_birth') }}</p>
						@else
						<p class="error-tag">{{ $errors->first('date_of_birth') }}</p>
						@endif
					</div>


					<div class="form-group col-sm-12 p-0">
						<div class="row">
							<div class="col-sm-4 pl-0 mt-2">
								<select name='birthday_month' class='form-control text-14 p-2' id='user_birthday_month'>
									<option value=''>{{ trans('messages.sign_up.month') }}</option>
									@for($m=1; $m<=12; ++$m) <option value="{{ $m }}"
										{{old('birthday_month')==$m?'selected="selected"':''}}>{{date(' F', mktime(0, 0,
										0, $m, 1))}}</option>
										@endfor
								</select>
							</div>

							<div class="col-sm-4 mt-2">
								<select name='birthday_day' class='form-control text-14' id='user_birthday_day'>
									<option value=''>{{trans('messages.sign_up.day')}}</option>
									@for($m=1; $m<=31; ++$m) <option value="{{$m}}"
										{{old('birthday_day')==$m?'selected="selected"':''}}>{{$m}}</option>
											@endfor
										</select>
									</div>

									<div class="col-sm-4 pr-0 mt-2">
										<select name=' birthday_year' class='form-control text-14' id='user_birthday_year'>
										<option value=''>{{ trans('messages.sign_up.year') }}</option>
										@for($m=date('Y'); $m > date('Y')-100; $m--)
										<option value="{{ $m }}" {{old('birthday_year')==$m?'selected="selected"':''}}>{{ $m }}</option>
											@endfor
										</select>
									</div>
								</div>

							<span class="text-danger text-13">
								<label id=' dobError'></label>
											</span>
							</div>

							<button type='submit' id="btn"
								class="btn pb-3 pt-3 text-15 button-reactangular vbtn-success w-100 ml-0 mr-0 mb-3"> <i
									class="spinner fa fa-spinner fa-spin d-none"></i>
								<span id="btn_next-text">{{ trans('messages.sign_up.sign_up') }}</span>
							</button>
						</div>

						<div class="text-14">
							{{trans('messages.sign_up.already')}} {{ $site_name }} {{ trans('messages.sign_up.member')
							}}?
							<a href="{{URL::to('/')}}/login?" class="font-weight-600">
								{{trans('messages.sign_up.login')}}
							</a></br>
							<input type="checkbox" required>
							<a href="{{route('privacyPolicy')}}">Accept Terms & Policy</a>
						</div>
			</form>


		</div>
	</div>
</div>
@stop