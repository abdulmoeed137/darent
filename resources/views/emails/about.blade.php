@extends('template')
@section('main') 
@if(app()->getLocale() == "ar")
<div class="container mt-5 mb-5">
    <h1 class="mb-4 text-35 font-weight-700 policy-content-title">معلومات عنا</h1>
    <div class="policy-content">
        <h1 class="mb-0"></h1>
        <p>دارينت هي المنصة الوحيدة الرائدة في مجال تمكين المضيفين من الترويج لمساحاتهم، وتحقيق التجربة المحلية للزوّار.</p>
        <p>سماتنا العربية الطيّبة هي ركيزتنا، وتاريخنا الزاخر بعادات الضيافة السخية هو مصدر إلهامنا، ومنهما أنشأنا دارينت.</p>
        
        <p>نعود هنا إلى جذورنا العربية الكريمة، ونعيد إحياء أصول الحفاوة الأصيلة، فنحيي مفاهيم السفر التي تحمل في طيّاتها التجارب الشخصية وتنقل المعارف الإنسانية النابعة من التجوال، من خلال عرض مساحات سكنية تشاركية تبني جسورًا من التواصل والعلاقات الودية بين السكّان المحليين والزوّار</p>

        <p>
            نجمع في منصتنا مُختلف المصادر والإمكانات المتاحة لزوّار المنطقة، ونهدف إلى تمكين المُضيفين من الترويج لمساحاتهم، وذلك من خلال تقديم خدمات شاملة وتثقيفية مبنية على التوجيه المتكامل، حتى نبتكر في خلق الحلول السكنية للمستأجرين وفق احتياجاتهم والحفاظ على حقوقهم والالتزام بالمهنية في التعامل معهم</p>

        <p>نسعى أن نكون الوجهة الشاملة والدليل الملائم للرحالة والمسافرين الباحثين عن رحلة يعيشون عبرها تفاصيل الحياة اليومية للسكّان المحليين، بحيث يرتادون المقاهي ويتناولون الأطعمة الرائجة ويتجولون في الأحياء الشعبية متعمّقين في الثقافة المحلية ومبحرين في أسرار تاريخ سكّان المنطقة الأصليين.</p>

        <p> هنا نتعمق في جمال التفاصيل ومتعة التجربة، ونضيء على زوايا تنسج المودة في كل دار.</p>
    </div>
</div>
@else
<div class="container mt-5 mb-5">
    <h1 class="mb-4 text-35 font-weight-700">About Us</h1>
    <div class="policy-content">
        <h1 class="mb-0"></h1>
        <p>Darent is the only leading platform in enabling hosts to promote their properties and create authentic local experiences for guests.</p>

        <p>Our kind-hearted Arab values and our rich history of hospitality and generosity are our cornerstone and our inspiration. Upon them, we created Darent.</p>

        <p>Here, we return to our noble Arab roots, rekindle the heritage of generosity, and revive the concepts of travel that carry with it shared human experiences and knowledge inspired by wanderlust. To this end, we post shared residential properties that build bridges of communication and friendliness between locals and visitors.</p>

        <p>On our platform, we bring together the various resources and means available for the visitors in each region. We thrive to empower hosts in promoting their properties by providing them with holistic and informative services based on integrated guidance. In turn, we provide innovative residential solutions for tenants that are tailored to their needs, with a commitment to a professional and respectful service that certainly upholds their rights.</p>

        <p>We aspire to become the go-to destination and guide for explorers and travelers who seek rich journeys to experience of the day-to-day lives of the locals, allowing them to visit their cafés, taste their popular dishes, and wander around their neighborhoods. It is an invitation to dive into the local culture and discover the hidden alleys of the local history.</p>

        <p>Here, we delve into the beauty and joy of each experience and highlight the heartwarming details in every Dar.</p>
    </div>
</div>
@endif

@push('scripts')
<script type="text/javascript" src="{{ url('public/js/jquery.validate.min.js') }}"></script>
@endpush
@stop