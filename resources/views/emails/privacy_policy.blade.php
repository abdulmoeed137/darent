@extends('template')

@section('main') 
@if(app()->getLocale() == "ar")
<div class="container mt-5 mb-5">
    <h1 class="mb-0 text-35 font-weight-700 policy-content-title">الشروط والأحكام</h1>
    <p class="text-center mb-5">الخاصة بالموقع الإلكتروني "دارينت/Darent</p>
    <div class="policy-content">
        <p>مرحبًا بكم في الموقع الإلكتروني الخاص بـ " دارينت/Darent <br> ت ُمثل الشروط والأحكام الواردة هنا، الصياغة والإطار القانوني ال  تي تحكم
            العلاقة بين الموقع الإلكتروني الخاص بـ " دارينت/Darent " تحت نطــاق
            <a href="https://www.darent.com">(www.darent.com)</a> و المملوك لشركة/ دارك الديار للسفر
            والســياحة (شــركة شــخص واحــد)، ســجل تجــاري رقم: 4030459993
            عنوانها: المملكة العربية الســعودية مدينــة جــدو حي/ الروضــة  شــارع/
            الأمــير ســعود الفيصــل ،(يُشــار إليهــا هنــا فيمــا بعــد بــــ "الموقــع
            الإلكتروني" أو بضمير المتكلم "نحن" أو بضمير الملكية المتصــل "نــا")
            وبين المستخدم ، وتعتبر هي الأساس فيما ينظم العقد الإلكتروني بين الطرفين.
             وأنه بالضغط على (موافق) أو خلافه (مثال: التسجيل أو الولوج إلى الموقع
            الإلكتروني أو الب  دء ب  الحجز...إلخ) ف  إن المس  تخدم ي ُق  ر بقراءت  ه وفهم  ه للش  روط
            والأحكام الآتية ويواف  ق عليه  ا. ل  ذا؛ الرج  اء ق  راءة جمي  ع بنوده  ا قب  ل الش  روع في
            الاســتخدام ،وإن لم توافقــوا على شــروط وأحكــام الاســتخدام هــذه،
            يرجى منكم الامتناع عن اس  تخدام الموقع الإلكــتروني، إذ أن اســتخدامه
            يعني موافقتكم عليها، كما نذكّركم بأننا نقوم بتحــديث البنــود الــواردة
            هنــا بين الحين والآخــر، وبنــاء على ذلــك، نُوصــي بمراجعتهــا بشــكلٍ
            منتظم، لما أن اســتمرار الاســتخدام يعــني مــوافقتكم على التحــديثات
            الحاصلة.</p>

            <h1 class="mb-0">لبند (1)</h1>
            <p class="mb-0">لأغراض هذا الالتزام فإنه ي ُقصد بالعبارات والكلمات الآتية، المعاني المبينة قرين كل
                منها، ما لم يقتض السياق خلاف ذلك:</p>
            <ul>
                <li>الشــركة: / دارك الــديار للســفر والســياحة (شــركة شــخص واحــد)،
                    ســجل تجــاري رقم: 4030459993 عنوانهــا: المملكــة العربيــة
                    السعودية مدينة جدة حي/ الروضة, الأمير سعود الفيصل , 2823</li>
                <li> تطبيق دارينت/Darent  :"هو المنصة الإلكترونية الخاصة بتطبيق
                    بـ " دارينت/Darent " تحت نطـاق <a href="(https://www.darent.com)">www.darent.com</a> 
                    والمملوك لشــركة/ دارك الــديار للســفر والســياحة (شــركة شــخص
                    واحد)</li>
                <li>سياسة الخصوصية: هي سياسة الخصوصــية لمعلومــات المســتخدم
                    في الموقــــــــع الإلكــــــــتروني والــــــــتي تجــــــــدها هنــــــــا
                    <a href="https://darent.com/privacy-policy">(darent.com/privacy-policy)</a> </li>
                <li>شروط وأحكام الاستخدام: هي شروط وأحكام الاستخدام هذه</li>
                <li>المستخدم: يعني العميل، سواء شخص طــبيعي أو اعتبــاري، ســواء
                    أنشأ حساب عبر تطبيق دارينت/Darent أم لا (مجرد زائر)، والذي</li>
                <li>اطلع على أي من الخدمات المتوفرة على تطــبيق دارينت/Darent
                    ، أو تمكن من الولوج والحجز من تطبيق دارينت/Darent.</li>
                <li>الاسـتخدام: القيـام بتصـفح تطـبيق دارينت/Darent والاطلاع على
                    أي من العقــارات المتــوفرة للحجــز على تطــبيق دارينت/Darent
                    والولوج منه، أو التواصل معنا لأي سبب كان، بجميع الوسائل.</li>
                <li>العقــارات: هي جميــع العقــارات بأنواعهــا -على ســبيل المثــال لا
                    الحصــر- (غــرف – شـقق ســكنية – فلــل – شـاليهات – اســتراحات –
                    مــزارع – مخيمــات) والــتي تعــرض على تطــبيق دارينت/Darent
                    للإيجار بشكل إلكتروني.</li>
                <li>العقد الإلكتروني: يقصد به الاتفاق الذي يبرم إلكترونيًا بين تطبيق
                    دارينت/Darent والمستخدم.</li>
                <li>النظام: نظام التجــارة الإلكترونيــة الصــادر بتــاريخ 07/11/1440هـ
                    الموافق10/07/2019م ولائحته التنفيذية وأي تعديل يطرأ ويســري
                    عليه.</li>
                <li>القــوة القــاهرة: هي أي ظــرف طــارئ يخــرج عن إرادة تطــبيق
                    دارينت/Darent أو المســتخدم -على ســبيل المثــال-: العواصــف،
                    والفيضــانات الجويــة ومــا شــابهها من ظــروف طبيعيــة، وانتشــار
                    الأوبئة والأمراض التي تعطل تنفيذ العقــد الإلكــتروني، والقــرارات
                    الحكومية، الأعطال التقنية.</li>
            </ul>

            <h1 class="mb-0">البند (2)</h1>
            <p>الأهلية: <br> يجب على المستخدم أن يكون بعمر (18) عامًا على الأقل للتسجيل أو الاستفادة
                من الخدمات تطبيق دارينت/Darent ، أو العروض المقدمة، أو مج  رد الزي  ارة
                والتص  فح، أو اس  تخدامها ب  أي ش  كل من الأش  كال، ومن خلال التس  جيل وزي  ارة
                واس  تخدام تطــبيق دارينت/Darent ، ف  إن المســتخدم ي ُق  ر بقب  ول ش  روط
                وأحك  ام الاس  تخدام ه  ذه، وأن  ه بكام  ل الأهلي  ة والق  درة على اس  تخدام تطــبيق
                دارينت/Darent ، والخ   دمات والمنتج   ات المتاح   ة من خلال   ه، ويواف   ق على
                الشروط الواردة هنا ويلتزم بها.</p>

            <h1 class="mb-0">البند (3)</h1>
            <p>1.كج  زء من عملي  ة التس  جيل؛ وفي ح  ال إنش  اء حس  اب من قِب  ل المســتخدم في
                تطبيق دارينت/Darent ، فإن  ه يجب توف  ير البيان  ات الص  حيحة والحقيقي  ة
                بحسب الوثائق الرسمية.</p>
            <p>2.ي ُقر ويوافق المستخدم بأن البيانات المقدمة في حسابه خاضعة للتحقق والاعتماد
                من قِب   ل تطـــبيق دارينت/Darent /المؤسسة، ويج   وز لهم اس   تخدام
                المعلومات الشخصية المقدمة من المستخدم، وأن المستخدم يمنح  ه الح  ق
                بالاطلاع على تلك المعلومات، ويتن  ازل المســتخدم عن أي تبع  ات قانوني  ة، أو
                أضرار قد تنشأ من خلال هذا الكشف عن البيانات لأطراف خارجية، وأن  ه يج  وز
                لتطــبيق دارينت/Darent إنه  اء وص   ول المســتخدم إلى الخ   دمات أو استخدامها في أي وقت، و/أو إلغاء الحساب، سواء كلي ً  ا أو جزئي ً ا، إذا لم يتمكن
                تطبيق دارينت/Darent في أي وقت من تحديد  أو إثبات هوية المستخدم
                أو بياناته، لاعتماد صحة الحساب.</p>
            <p>3.ي ُقر المستخدم بعدم التسجيل في تطبيق دارينت/Darent و/أو الدخول عليه
                منتحلاً  اسم مستخدم آخر، أو شخصية  أخرى. </p>
            <p>4.ي ُقر المستخدم بأن ّه لن يقوم بتمثيل أي شخص طبيعي، أو أي شخص اعتباري، من
                دون أن يكون مخولاً  بذلك. </p>
            <p>5.ي ُقر المستخدم بإنشاء حساب واحد على تطــبيق دارينت/Darent ، ويحتف  ظ
                تطبيق دارينت/Darent بالحق المطلق في إغلاق أي حسابات أخرى لنفس
                المستخدم</p>
            <p>6.ي ُقر المستخدم  ب أن دخ  ول تطبيق دارينت/Darent واس  تخدامه  ل  ه س  يكون 
                لأغراض مشروعة فقط، ويلتزم بعدم استخدام تطــبيق دارينت/Darent أو
                ما يتوفر عليه من معلومات، أو خدمات، أو أدوات، للقي  ام ب  أي عم  ل ينتج عن  ه
                مخالفة أو جريمة، بموجب أي نظام ساري في المملكة العربية السعودية.</p>
            <p>7.ي ُقر المستخدم ب  أن أي معلوم  ات يق  دمها ع  بر تطــبيق دارينت/Darent هي
                معلومات كاملة ودقيقة ومُحدّثة، كما يتحمل المسؤولية عن محتوى أي معلومة،
                أو وثيقة يتم تقديمها من خلال بوابة تطبيق دارينت/Darent </p>
            <p>8.إن الخدمات المتوفرة على تطبيق دارينت/Darent مقدم  ة للمستخدم كم  ا
                هي، من دون ضمانات من أي نوع، سواء كانت صريحة أو ضمنية.</p>
            <p>9.ي ُقر المستخدم أن اس  تخدامه تطــبيق دارينت/Darent وف  ق ه  ذه الض  وابط
                والشروط، وأنه مسؤول أمام الجهات الرسمية عم  ا يق  وم ب  ه، وأن أي تص  رف
                من خلاله خاضع لمسؤوليته الخاصة، وأنه يحق للغير إقامة دعوى ضده في حال
                الإساءة المتعمدة أو التشهير.</p>
            <p>10.في ح  ال الإخلال به  ذه شــروط وأحكــام الاســتخدام، ف  إن تطــبيق دارينت/
                Darent لديه الحق في تعطيل، أو حذف حس  اب المستخدم، م  ع أو ب  دون
                إشعار.</p>
            <p>11.يحتفظ تطبيق دارينت/Darent ب  الحق في مراقب  ة أي محت  وى يتم إدخال  ه من
                قبل المستخدم </p>
            <p>12.يحتفظ تطبيق دارينت/Darent بالحق (من دون التزام) في ش  طب، أو إزال  ة،
                أو تحرير أي مواد مُدخلة، من شأنها انتهاك حق  وق تطــبيق دارينت/Darent
                وحقوق الغير، وانتهاك هذه شروط وأحكام الاستخدام</p>
            <p>13.يحتفظ تطبيق دارينت/Darent بالحق في حذف أي معلومات، يعتبره  ا انتهاك ً  ا
                لأي من شروط وأحكام الاستخدام الواردة هنا، دون إش  عار ودون إب  داء أي
                أسباب.</p>

            <h1 class="mb-0">البند (4)</h1>
            <p>1.سياسة الحجوزات: <br> عن  د حج  ز المس  تخدم لأي عق  ار عن طري  ق تطــبيق دارينت/Darent، ف  إن
                المستخدم يوافق ويلتزم بجميع شروط الحج  ز المفروض  ة من قب  ل ص  احب العق  ار
                والمنصوص عليها في صفحة العقار، والتي تضم قوانين الإلغاء وع  دم الحض  ور، وأي
                ش  روط أخ  رى ق  د يفرض  ها ص  احب العق  ار أثن  اء الإقام  ة أو الحج  ز، بم  ا في ذل  ك
                الخدمات المتاحة و/أو المراف  ق ال  تي يوفره  ا ص  احب العق  ار (من الممكن تحص  يل
                الشروط والأحكام الإضافية الخاصة من مكان الإقامة المحدد).</p>
            <p>2.سياسة الوصول المتأخر أو عدم الحضور: <br> فيم  ا يتعل  ق ب  الحجز (القاب  ل للإلغ  اء) فق  ط، ينبغي على المس  تخدم بالتواص  ل م  ع
                تطبيق دارينت/Darent في حال ما رغب المستخدم بإلغ  اء الحج  ز قب  ل موع  د
                الحجز المُختار. أما في حال بدء سريان موعد الحجز وقد تأخر المستخدم عن موعد
                تس  جيل ال  دخول أو في ع  دم إمكاني  ة التس  جيل في ي  وم الحج  ز فعلى المس  تخدم
                التواصل مباشرة مع صاحب العقار واشعاره بذلك.  وفي حال اخفق المس  تخدم في
                تسجيل الدخول للعقار في يوم الحجز دون إش  عار ص  احب العق  ار ب  ذلك ، فق  د يتم
                إلغاء الفترة المتبقية من الحجز وق  د لا يك  ون المس  تخدم مخ  ولا ً لاس  ترداد  أموال  ه،
                وذلك وفقا ً للشروط والاحكام الخاصة بالعقار</p>
            <p>3.سياسة تغيير أو إلغاء الحجز: <br> تختلف سياسة إلغاء الحج ز بين العق ارات. فبعض الحج وزات غ  ير قابل ة للتغي ير أو
                الإلغاء، وقد يفرض بعض أصحاب العقارات رس  وما ً معين  ة على إلغ  اء الحج  ز، بينم  ا
                ي  تيح البعض منهم اس  ترداد مبل  غ الحج  ز بالكام  ل. ي  رجى الملاحظ  ة ب  أن بعض
                الحجوزات بأسعار معينة أو عروض خاصة غير مؤهلة للتغيير أو الإلغاء. كما قد تقوم
                بعض العقارات بتطبيق رسوم إض  افية على المغ  ادرة المبك  رة أو المت  أخرة. تت  وفر
                سياسة تغيير الحجز أو الإلغاء الخاصة بالعقار على صفحة معلوم  ات العق  ار أو تحت
                بند "شروط الحجز"،  "سياسة الإلغاء" أو م  ا ش  ابه ذل  ك، وفي رس  الة تأكي  د الحج  ز
                الإلكترونية ووصل الحجز. يجب على المستخدم بالتأكد من وق  راءة سياس  ة الإلغ  اء
                وسياسة عدم الحضور الخاصة بالعق  ار قب  ل إتم  ام عملي  ة الحج  ز. بغض النظ  ر عن
                سياس  ة تغي  ير أو إلغ  اء الحج  ز للعق  ار، يح  ق لتطــبيق دارينت/Darent بف  رض
                رسوم معينة على تغيير أو إلغاء الحجز، وذلك كما هو مبين في الموقع. يرجى التأكد
                من تفاصيل الشروط والأحكام الخاصة بالعقار قبل القيام بالحجز. وفي ح  ال إذا م  ا
                رغب المس  تخدم باس  تعراض، أو تع  ديل، أو إلغ  اء الحج  ز يجب على المس  تخدم
                مراجع  ة إش  عار تأكيي  د الحج  ز الإلكتروني  ة، واتب  اع التعليم  ات المنص  وص عيه  ا أو
                التواصل مع خدمة العملاء الخاص بتطبيق دارينت/</p>
            <p>4.عدم توافر بعض المرافق: <br> ما أن تطبيق دارينت/Darent وسيط الكتروني بين المستخدم وص  احب العق  ار، لا
                يع  د بتط  بيق دارينت/Darent أو يض  من للمس  تخدم بت  وافر أي من المراف  ق ال  ذي
                سبق للمستخدم طلبها من صاحب العقار. لذا؛ إذا لم تتوفر بعض المراف  ق ال  تي تم
                حجزها لدى الوصول للعقار، يجب على المستخدم بالتواصل مع صاحب العقار ف  ورً ا
                وتقديم شكوى لدى تطبيق دارينت/Darent من خلال خدمة العملاء يقر المستخدم باطلاعه على تفاصيل العق  ار وجمي  ع الش  روط والمعلوم  ات ال  تي
                ترتبط باستئجاره وكيفية الاستفادة من منافعه من مواعي د وبيان ات الزامي ة وغيره ا
                حسب ما هو موضح في تطبيق دارينت/Darent والشروط والأحكام هذه</p>
            
            <h1 class="mb-0">البند (5)</h1>
            <p>الدفع: <br> 1.يعطي تطبيق دارينت/Darent الحق للمستخدم بحجز العقارات مؤقت ً  ا لم  دة
                10 دقائق قبل إتمام عملية السداد أو سيتم إلغاء الطلب بعد ذلك.</p>
            <p>2.تقبل وسائل الدفع الآتية لإتمام عملي  ات الحج  ز في تطبيق دارينت/Darent
                كالآتي: <br> بطاقة الفيزا، بطاقة ماستر كارد، مدى،،____  ويمكن للمستخدم اختي  ار أي
                منها، وستكون جمي  ع الفوات  ير موض  حة التفاص  يل الأساس  ية بحس  ب م  ا ورد
                نظامًا.</p>
            <p>3.عن  د قي  ام المس  تخدم باس  تخدام بطاق  ة دف  ع بعمل  ة مختلف  ة عن الري  ال
                السعودي، فقد تقوم جهة إصدار بطاقة الدفع بتحويل المبلغ الم  دفوع للري  ال
                السعودي بحسب سعر الصرف المحدد من قبلها عن  د إتم  ام عملي  ة ال  دفع. –
                ولا يعتبر تطبيق دارينت/Darent ملتزم بأي تغير في السعر يحدث بسبب
                ذلك-.</p>

            <h1 class="mb-0">البند (6)</h1>
            <p>الأسعار وتفاصيل المنتجات: <br> 1.تعرض تفاصيل أسعار حج  ز العق  ارات في تطبيق دارينت/Darent بالري  ال
                السعودي.</p>
            <p>2.يقوم تطبيق دارينت/Darent باستعراض سعر وتفاصيل العقارات وسياساته
                والخدمات المعروضة للانتفاع ب  ه في المس احة المخصص  ة ل ذلك في ص فحة
                استعراض العقار.</p>
            <p>3.يلتزم الموقع الإلكتروني بالسعر الذي يقوم بعرضه وتثبيت  ه لاس  تئجار العق  ار
                وقت الحج    ز، لكن من الممكن أن يح    دث خط    أ في تطـــبيق دارينت/
                Darent بشأن صحة الأسعار المعروض  ة؛ وعلي  ه يح  ق لتطـبيق دارينت/
                Darent تصحيح هذا الخطأ كالآتي <br> - أن يتم تصحيح الخطأ من قبل تطبيق دارينت/Darent وفق الض  وابط ال  تي
                نص عليها النظام <br> - إلغاء عملية الحج  ز وإع  ادة المبل  غ الم  دفوع للمس  تخدم طالم  ا ك  ان الخط  أ في
                المدة المقبولة ب  (يوم واحد) <br> - عندما يك  ون الس  عر الم  دفوع أعلى من س  عر الحج  ز، يل  تزم تطبيق دارينت/
                Darent بإعادة الفرق إلى المستخدم. <br> - في حال ك  ان الس  عر الم  دفوع أق  ل من س  عر الحج  ز يح  ق لتطبيق دارينت/
                Darent تخيير المستخدم بين الاستمرار في تنفيذ العقد بعد التص  حيح ب  دفع
                الفرق، أو فسخه واسترجاع ما دفعه من تكاليف مترتبة على هذا الخطأ. <br> - يح  ق لتطــبيق دارينت/Darent قب  ول عملي  ة الحج  ز عن  دما يك  ون الس  عر
                المدفوع أقل من سعر الحجز وذلك حسب تقديره  الخاص. <br> - يلتزم تطبيق دارينت/Darent باحتساب ض  ريبة القيم  ة المض  افة من س  عر
                الحجوزات.</p>
                <p>4.في حال تطبيق أي خص  ومات وع  روض يتم تطبيقه  ا على الحج  وزات كم  ا ينص
                    عليه البند( 7) أدناه.</p>
                
                <h1 class="mb-0">البند (7)</h1>
                <p>العروض والخصومات: <br> 1.يح  ق لتطــبيق دارينت/Darent تط  بيق أي ع  رض أو خص  م على أي من
                    العقارات المختارة، وله كامل الحرية في اختيار نسبة الخصم أو طريقته أو مدته
                    أو أي من تفاصيله وبما لا يخالف النظام.</p>
                <p>2.يلتزم تطبيق دارينت/Darent بقيمة العرض والخصم، حسب ما هو منش  ور
                    على الموقع الإلكتروني أو أي وسيلة إعلان أخرى.</p>
                <p>3.لا يحق للمستخدم طلب  الخصم على حجز أي عقار غير مشمول تحت الخص  م
                    أو العرض.</p>

                <h1 class="mb-0">البند (8)</h1>
                <p>إصلاح الأخطاء: <br> 1.يحق للمستخدم عند قيامه أو إدخاله أي خطأ على تطبيق دارينت/Darent
                    من تداركه كالآتي: <br> - أن يبلغ تطبيق دارينت/Darent خلال (24) س  اعة من  ذ وق  وع الخط  أ، عن طري                 ق الخان                 ة المخصص                 ة ل                 ذلك <a href="https://darent.com/emails/contact_us">(darent.com/emails/contact_us)</a> ، ويخضع تقدير الخطأ من عدمه
                    بشكل كامل لتطبيق دارينت/Darent</p>

                <h1 class="mb-0">البند (9)</h1>
                <p>مسؤولية تطبيق دارينت/Darent <br> 1.بالنسبة للحجوزات: يقع على عاتق المؤسسة اتب  اع جمي  ع الأنظم  ة الس  ارية في
                    المملك    ة المرتبط    ة بنش    اطها، ولا يعتــــبر تطــــبيق دارينت/Darent
                    أو/المؤسسة مسؤولين – في أي حال من الأحوال- عن التع ويض أو تحم ل أي
                    تبعات عن أي مم  ا يخ  رج عن نط  اق المس  ؤولية الواقع  ة عليهم حس  ب النظ  ام
                    وشروط وأحكام الاستخدام هذه.
                </p>
                <p>
                    2.ي ُقر المستخدم بإعفاء وإخلاء مسؤولية تطبيق دارينت/Darent كلي ًا من أي
                    مس  ؤولية، عن أي أض  رار و/أو خس  ائر مادي  ة أو معنوي  ة، و/أو ج  راء الانتف  اع أو
                    الاستفادة من تطبيق دارينت/Darent وخدماته.
                </p>
                <p>3.دون الحد أو التعطيل من تطبيق أي من البن  ود أعلاه، أو الالتزام  ات ذات الص  لة
                    بالس  رية، وباس  تثناء المس  ؤولية الناش  ئة عن ع  دم الامتث  ال لأي نظ  ام، يجب ألا
                    تتج  اوز ح  دود مس  ؤولية تط  بيق دارينت/Darent / أو المؤسس  ة ب  أي ح  ال من
                    الأحوال وعن أي ضرر أو خسائر تثبت مبلغ وقدره (1,000) ريال سعودي.</p>

                <h1 class="mb-0">البند (10)</h1>
                <p>1.يق  وم تطــبيق دارينت/Darent بتوف  ير إمكاني  ة ال  دخول الآمن إلى تطــبيق
                    دارينت/Darent وإلى الخدمات المقدمة من خلاله، لكن نتيجة لعوامل خارجة
                    عن س  يطرتنا، ف  إن تطــبيق دارينت/Darent لا يض  من إمكاني  ة ال  دخول
                    المس  تمرة بحري  ة ودون انقط  اع، وبش  كل آمن إلى تطبيق دارينت/Darent
                    و/أو أي من خدماته، كما لا يتحم  ل تطبيق دارينت/Darent المس  ؤولية عن
                    أي انقط  اع، أو ت  أخير، أو خل  ل في الخ  دمات المقدم  ة ع  بر تطــبيق دارينت/
                    Darent ، مع أحقية تطبيق دارينت/Darent في ملاحقة المُخترق قض  ائي ًا
                    أو من يحاول اختراق تطبيق دارينت/Darent ، حسب الأنظمة المعمول به  ا
                    بالمملكة العربية السعودية.</p>
                <p>2.لا ي ُوفر تطبيق دارينت/Darent ولا أي من القائمين عليه، ضمانة ب  أن تطــبيق
                    دارينت/Darent لن يتعرض للتوقف، أو أنه س  يكون خ  الي من المش  اكل، أو
                    الح  ذف أو الأخط  اء، كم  ا لا يوج  د ض  مانة بش  أن النتيج  ة ال  تي سيحص  ل  عليه  ا
                    المستخدم جراء استخدامه لتطبيق دارينت/Darent ، أو التسجيل فيه.</p>
                <p>3.لا يتحمل تطبيق دارينت/Darent المسؤولية عن أي خسارة في الأرب  اح، أو أي
                    خسارة من أي نوع، نتيجة للمعلومات أو الخدمات التي يقوم بتق  ديمها، أو نتيج  ة
                    معلومات قام بإضافتها. كم  ا لا يتحم  ل تطبيق دارينت/Darent المس  ؤولية
                    عن أي خس  ارة، أو تع  ديل، أو ض  رر في بيان  ات المس  تخدمين المخزن  ة على
                    تطبيق دارينت/Darent، مما ينشأ عنه حصول شخص غير مفوض، على حق
                    الدخول إلى بيانات المستخدم المخزنة لدى تطبيق دارينت/Darent </p>
                <p>4.مهما كانت الأحوال أو الظروف، فإن تطبيق دارينت/Darent غير مسؤول تجاه
                    أي من الأمور التالية على سبيل المثال لا الحصر: الإهم  ال ال  ذي يتس  بب في أي
                    أضرار، أو تلف من أي نوع، سواء كانت مباشرة أو عارضة، أو خاصة، أو لاحق   ة،
                    أو أي مصاريف أو خسائر قد تنجم بسبب استخدام المستخدم، أو عدم الق  درة
                    على استخدامه إياه، أو وقوع بعض الأخطاء، أو السهو، أو تأخر اس  تجابة النظ  ام
                    لأي س  بب ك  ان، أو إعاق  ة في التش  غيل، أو وق  وع أعط  ال، أو تع  رض أجه  زة
                    الحاسب الآلي للفيروسات، أو تعط  ل النظ  ام بالكام  ل، أو خس  ارة أي أرب  اح، أو
                    تعرض سمعة المستخدم للإس  اءة، ح  تى ل  و ج  رى الإش  عار ص  راحة باحتمالي  ة
                    وقوع مثل هذه الأمور، أو وق  وع مش  اكل ج  راء الوص  ول إلى تطبيق دارينت/
                    Darent، أو الدخول إليه، أو استخدامه،  أو الوصول من خلاله إلى مواقع أخرى.</p>

                <h1 class="mb-0">البند (13)</h1>
                <p>حق تطبيق دارينت/Darent في التعديل: <br> 1.يحق لتطبيق دارينت/Darent تعديل ه  ذه شروط وأحكــام الاســتخدام
                    و/أو سياسة الخصوصية، واستبدالها كلي ًا أو جزئي ًا، مع الإشعار خلال (7) أي  ام
                    على الأق  ل ع  بر البري  د الإلك  تروني، أو على حس  اب المســتخدم في تط  بيق
                    دارينت/Darent، أو أي إش   عارات ت ُفي   د ب   التغييرات من ه   ذا القبيل، وتص   بح
                    التعديلات نافذة فور نشرها على تطبيق دارينت/Darent، ويعتبر اس  تمرارية دخول المستخدم لتطــبيق دارينت/Darent أو اس  تخدامه للخ  دمات ال  تي
                    يوفرها تطبيق دارينت/Darent موافقة منه على هذه التعديلات.</p>
                <p>2.لتطبيق دارينت/Darent والقائمين عليه الحق في تعديل  ه، أو ج  زء من  ه، أو
                    إيقافه، أو تعليقه مؤقت ًا أو بشكل دائم، دون إشعار مسبق وفي أي وقت يراه، ولا
                    يعد تطبيق دارينت/Darent مسؤول تجاه المستخدم، أو تجاه أي أط  راف
                    أخرى، عن أي تعديل أو إيقاف أو تعليق.</p>

                <h1 class="mb-0">البند (14)</h1>
                <p>حقوق الملكية الفكرية لتطبيق دارينت/Darent:
                    تعود حقوق الطب  ع والنش  ر وحق  وق الملكي  ة الفكري  ة لتطبيق دارينت/Darent
                     ،________________________ /إلى مؤسســةوتع  ود ملكيته  ا إليهم، أو لأي من
                    مانحي الترخيص لتطبيق دارينت/Darent والمؤسسة، ويحظ  ر اس  تخدامها أو
                    نشرها دون أي موافقة كتابية مسبقة من تطــبيق دارينت/Darent ، ولا ي ُس  مح
                    بإعادة إنتاج أي جزء من تطبيق دارينت/Darent ، من دون إذن كت  ابي مس  بق
                    من تطبيق دارينت/Darent.</p>

                <h1 class="mb-0">البند (15)</h1>
                <p>سلوك المستخدم اتجاه معلوماته الشخصية: <br> 1.يتحمل المستخدم مسؤولية الحفاظ على سرية معلوماته، و منه  ا -على س  بيل
                    المثال -: (حساب المستخدم وكلمة المرور، رقم هاتفه...إلخ)، وذل  ك ح  ال ك  ان
                    المستخدم مسجلاً  بتطبيق دارينت/Darent ، ويكون المستخدم مس  ؤولاً
                    عن استخدامه  لحسابه الشخصي وكلمة المرور، ويجب على المستخدم إخطار
                    تطبيق دارينت/Darent فورً ا بأي استخدام فعلي أو مشتبه به، غير مصرح به
                    لحساب المستخدم، على أن تطبيق دارينت/Darent سوف يتعاون - دون
                    أي التزام - لاسترداد الحساب إلى مستخدمه الفعلي، بعد إتمام إجراءات التحقق
                    من ص  حة المعلوم  ات، ولن يك  ون تطــبيق دارينت/Darent مس  ؤول عن
                    الخسائر الناجمة عن أي استخدام غير مصرح به للحساب.</p>
                <p>.إذا قدّم المستخدم معلومات غير صحيحة، أو غير دقيق  ة، أو غ  ير كامل  ة، أو أن
                    تطبيق دارينت/Darent لديه أسباب معقولة للاشتباه في أن المعلومات غير
                    صحيحة، أو غير دقيقة، أو غير كاملة، يحق لتطــبيق دارينت/Darent إيق  اف
                    حساب المستخدم وفقًا لتقديره الخاص، ويس  قط ح  ق المســتخدم حي  ال أي
                    مطالبة.</p>

                <h1 class="mb-0">البند (16)</h1>
                <p>أحكام عامة: <br> 1.ي ُق  ر ويل  تزم المس  تخدم بالامتث  ال الكام  ل والت  ام، لكاف  ة الأنظم  ة والل  وائح
                    والتنظيمات والأعراف والقواعد التي تحكم وتنظم استخدامه لتطبيق دارينت/
                    Darent.</p>
                <p>2.يقر المستخدم ويلتزم بإخلاء ذمة تطبيق دارينت/Darent والقائمين عليه من أي
                    مس  ؤولية أو ض  رر وال  تزام ينش  أ نتيج  ة لحج  ز المس  تخدم لأي من العق  ارات
                    المعروضة للإيجار في تطبيق دارينت/Darent وأي مما قد يح  دث نتيج  ة لس  وء
                    اس  تخدامها س  واء داخ  ل مق  ر تطــبيق دارينت/Darent أو خارجها، ولا يع  د
                    تطبيق دارينت/Darent مسؤول عن ذلك بأي حال من الأحوال.</p>
                <p>3.القانون الواجب التطبيق والمرجعيــة القضــائية: ي ُق  ر المســتخدم ب  أن
                    شروط وأحكام الاستخدام هذه ت ُفسر وفقًا لأنظم  ة المملك  ة العربي  ة الس  عودية،
                    وي ُقر ويلتزم المستخدم بأنه في حال نشوء نزاع حول تنفي  ذ أو تفس  ير أو إلغ  اء
                    أو فسخ أو صحة أو بطلان الشروط والأحكام، أو يتف  رع عنه  ا أو يرتب  ط به  ا ب  أي
                    حال من الأحوال، يتم الفصل فيه عن طريق المحكمة المختصة في مدينة ج  دة،
                    وفق الأنظمة في المملكة العربية السعودية.</p>
                <p>4.المراســــــلات: تك      ون المراس      لات ع      بر البري      د الإلك      تروني <a href="INFO@DARENT.COM">(INFO@DARENT.COM)</a> ، أو عن طريق خدمة "تواصل معنا" بداخل تطبيق
                    دارينت/Darent <a href="https://darent.com/emails/contact_us">(darent.com/emails/contact_us)</a> ،كم ا
                    تكون المراسلات على عنوان المستخدم، المبين في معلوماته الشخص  ية عن  د
                    إنش  اء الحس  اب، ولا يعت  د بخلافه  ا وتك  ون المراس  لات ناف  ذة على المراس  لات
                    المسجلة بعلم وصول سواء المراسلات الورقية أو الالكترونية.</p>
                <p>5.تاريخ سريان الشروط والأحكام: تسري هذه الشروط والأحكام اعتبارً ا من
                    تاريخ ضغط المستخدم على خيار ("موافق").</p>
                <p>6.كامل الاتفاقية: الشروط والأحكام الحالية، ت ُشكل كامل الاتف   اق بين تطبيق
                    دارينت/Darent والمستخدم، فيما يتعلق بالعق  د الإلك  تروني بين الط  رفين،
                    في حال عدم نظامية أي بند من الشروط والأحكام، أو بطلانه أو عدم قابليته
                    للتنفيذ كلي ًا أو جزئي ًا، بمقتضى الأنظمة بالمملكة العربي  ة الس  عودية؛ فيتم اعتب  ار
                    هذا البند أو الجزء منه، ليس جزءًا من الشروط والأحكام، دون أي ت  أثير على
                    نظامية بقية البنود.</p>
                <p>7.بإمكانكم الاتصال بفري  ق تطبيق دارينت/Darent ع  بر البري  د الإلك  تروني  <a href="
                    https://darent.com/emails/contact_us">darent.com/emails/contact_us</a> ، أو عن طريق خدمة "تواصــل
                    معنا" ب  داخل تط  بيق دارينت/Darent ، وس  وف نق  وم بك  ل جهودن  ا لل  رد على
                    استفساراتك والشكاوى، كما نقدّر أي ملاحظ ات واقتراح  ات، ل  ذا لا ت  تردّدوا في
                    التواص  ل معن  ا على العن  وان الت  الي مق  ر شــركة/ دارك الــديار للســفر
                    والسياحة (شركة شخص واحــد) ، المملكــة العربيــة الســعودية جــدة،
                    حي الروضة.</p>
    </div>
</div>

@else
<div class="container mt-5 mb-5">
    <h1 class="mb-4 text-35 font-weight-700">Terms & Privacy</h1>
    <div class="policy-content">
        <p>At <a href="darent.com">darent.com</a>, accessible from <a href="darent.com">darent.com</a>, one of our main priorities is the privacy of our visitors. This Privacy Policy document contains types of information that is collected and recorded by <a href="darent.com">darent.com</a> and how we use it. <br>
        If you have additional questions or require more information about our Privacy Policy, do not hesitate to contact us. <br>
        This Privacy Policy applies only to our online activities and is valid for visitors to our website with regards to the information that they shared and/or collect in <a href="darent.com">darent.com</a>. This policy is not applicable to any information collected offline or via channels other than this website.</p>

        <h1 class="mb-0">Consent</h1>
        <p>By using our website, you hereby consent to our Privacy Policy and agree to its terms.</p>

        <h1 class="mb-0">Information we collect</h1>
        <p>The personal information that you are asked to provide, and the reasons why you are asked to provide it, will be made clear to you at the point we ask you to provide your personal information.<br>
        If you contact us directly, we may receive additional information about you such as your name, email address, phone number, the contents of the message and/or attachments you may send us, and any other information you may choose to provide.<br>
        When you register for an Account, we may ask for your contact information, including items such as name, company name, address, email address, and telephone number.</p>

        <h1 class="mb-0">How we use your information</h1>
        <p class="mb-0">We use the information we collect in various ways, including to:</p>
        <ul>
            <li>Provide, operate, and maintain our webste</li>
            <li>Improve, personalize, and expand our webste</li>
            <li>Understand and analyze how you use our webste</li>
            <li>Develop new products, services, features, and functionality</li>
            <li>Communicate with you, either directly or through one of our partners, including for customer service, to provide you with updates and other information relating to the webste, and for marketing and promotional purposes</li>
            <li>Send you emails</li>
            <li>Find and prevent fraud</li>
        </ul>

        <h1 class="mb-0">Log Files</h1>
        <p><a href="darent.com">darent.com</a> follows a standard procedure of using log files. These files log visitors when they visit websites. All hosting companies do this and a part of hosting services' analytics. The information collected by log files include internet protocol (IP) addresses, browser type, Internet Service Provider (ISP), date and time stamp, referring/exit pages, and possibly the number of clicks. These are not linked to any information that is personally identifiable. The purpose of the information is for analyzing trends, administering the site, tracking users' movement on the website, and gathering demographic information.</p>

        <h1 class="mb-0">Cookies and Web Beacons</h1>
        <p>Like any other website, <a href="darent.com">darent.com</a> uses 'cookies'. These cookies are used to store information including visitors' preferences, and the pages on the website that the visitor accessed or visited. The information is used to optimize the users' experience by customizing our web page content based on visitors' browser type and/or other information.<br>
        For more general information on cookies, please read "What Are Cookies" from Cookie Consent.</p>

        <h1 class="mb-0">Advertising Partners Privacy Policies</h1>
        <p>You may consult this list to find the Privacy Policy for each of the advertising partners of <a href="darent.com">darent.com</a>.<br>
        Third-party ad servers or ad networks uses technologies like cookies, JavaScript, or Web Beacons that are used in their respective advertisements and links that appear on <a href="darent.com">darent.com</a>, which are sent directly to users' browser. They automatically receive your IP address when this occurs. These technologies are used to measure the effectiveness of their advertising campaigns and/or to personalize the advertising content that you see on websites that you visit.
        Note that <a href="darent.com">darent.com</a> has no access to or control over these cookies that are used by third-party advertisers.</p>

        <h1 class="mb-0">Third Party Privacy Policies</h1>
        <p><a href="darent.com">darent.com</a> Privacy Policy does not apply to other advertisers or websites. Thus, we are advising you to consult the respective Privacy Policies of these third-party ad servers for more detailed information. It may include their practices and instructions about how to opt-out of certain options.<br>
        You can choose to disable cookies through your individual browser options. To know more detailed information about cookie management with specific web browsers, it can be found at the browsers' respective websites.</p>

        <h1 class="mb-0">CCPA Privacy Rights (Do Not Sell My Personal Information)</h1>
        <p>Under the CCPA, among other rights, California consumers have the right to:<br>
        Request that a business that collects a consumer's personal data disclose the categories and specific pieces of personal data that a business has collected about consumers.
        Request that a business delete any personal data about the consumer that a business has collected.<br>
        Request that a business that sells a consumer's personal data, not sell the consumer's personal data.<br>
        If you make a request, we have one month to respond to you. If you would like to exercise any of these rights, please contact us.</p>

        <h1 class="mb-0">GDPR Data Protection Rights</h1>
        <p>We would like to make sure you are fully aware of all of your data protection rights. Every user is entitled to the following:<br>
        The right to access – You have the right to request copies of your personal data. We may charge you a small fee for this service.<br>
        The right to rectification – You have the right to request that we correct any information you believe is inaccurate. You also have the right to request that we complete the information you believe is incomplete.<br>
        The right to erasure – You have the right to request that we erase your personal data, under certain conditions.<br>
        The right to restrict processing – You have the right to request that we restrict the processing of your personal data, under certain conditions.<br>
        The right to object to processing – You have the right to object to our processing of your personal data, under certain conditions.<br>
        The right to data portability – You have the right to request that we transfer the data that we have collected to another organization, or directly to you, under certain conditions.<br>
        If you make a request, we have one month to respond to you. If you would like to exercise any of these rights, please contact us.</p>

        <h1 class="mb-0">Children's Information</h1>
        <p>Another part of our priority is adding protection for children while using the internet. We encourage parents and guardians to observe, participate in, and/or monitor and guide their online activity.<br>
        <a href="darent.com">darent.com</a> does not knowingly collect any Personal Identifiable Information from children under the age of 13. If you think that your child provided this kind of information on our website, we strongly encourage you to contact us immediately and we will do our best efforts to promptly remove such information from our records.</p>
    </div>
</div>
@endif

@push('scripts')
<script type="text/javascript" src="{{ url('public/js/jquery.validate.min.js') }}"></script>
@endpush
@stop