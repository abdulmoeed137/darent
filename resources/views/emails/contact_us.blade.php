@extends('template')

@section('main') 
<form action="{{route('contactus')}}" mEthod='POST' class="message-form">
    @csrf
    <div class="container-fluid container-fluid-90 min-height">
        <div class="title">
            <h1 class="text-center mt-5 text-24 font-weight-700 warning-text">Contact Us</h1>
            <h3 class="text-center mt-4 mb-5 text-24 font-weight-700">Leave A Message</h3>

            @if (session('success'))
                <div class=" alert alert-success">
                    {!! session('success') !!}
                </div>
                    
                @elseif(session('error'))
                <div class=" alert alert-danger">
                    {!! session('error') !!}
                </div>
             @endif

        </div>
        <div class="row justify-content-center">
            <div class="col-lg-6 col-12">
                <div class="form-group">
                <input type="text" name="name" placeholder="Name" class="form-control hdnspin mb25" maxlength="16" required/>
                </div>
            </div>
            <div class="col-lg-6 col-12">
                <div class="form-group">
                <input type="number" name="phone" placeholder="Phone Number" class="form-control hdnspin mb20" maxlength="16" required/>
                </div>
            </div>
            <div class="col-lg-12 col-12">
                <div class="form-group">
                <input type="mail" name="email" placeholder="Email Address" class="form-control hdnspin mb20" maxlength="40" required/>
                </div>
            </div>
            <div class="col-lg-12 col-12 mt-3">
                <div class="form-group">
                    <textarea name="message" placeholder="Message" class="form-control mb20" rows="7" required></textarea>
                </div>
            </div>
    
    
            <div class="col-lg-12 text-center mt-3">
                <input type="submit" style="width: 230px" class="btn vbtn-outline-success text-16 font-weight-700 pl-5 pr-5 pt-3 pb-3">
                    <i class="spinner fa fa-spinner fa-spin d-none"></i>
            </div>
        </div>
    </div>
</form>
@push('scripts')
<script type="text/javascript" src="{{ url('public/js/jquery.validate.min.js') }}"></script>
@endpush
@stop
