@extends('template')

@section('main')
    <div class="">
        <div class="row m-0">
            @include('users.sidebar')
            <div class="col-lg-10">
                <div class="main-panel">
                    <div class="container-fluid min-height">
                        <div class="row">
                            <div class="col-md-12 p-0 mb-3">
                                <div class="list-bacground mt-4 rounded-3 p-4 border">
                                    <span class="text-18 pt-4 pb-4 font-weight-700 font-sm">
                                        {{ trans('messages.users_dashboard.my_trips') }}
                                    </span>

                                    <div class="float-right">
                                        <div class="d-flex">
                                            <div class="pr-4 pd-left-rtl">
                                                <span
                                                    class="text-14 pt-2 pb-2 font-weight-700">{{ trans('messages.users_dashboard.sort_by') }}</span>
                                            </div>

                                            <div>
                                                <form action="{{ url('/trips/active') }}" method="POST" id="my-trip-form">
                                                    {{ csrf_field() }}
                                                    <select class="form-control room-list-status text-14 minus-mt-6"
                                                        name="status" id="trip_select">
                                                        <option value="All"
                                                            {{ $status == 'All' ? ' selected="selected"' : '' }}>All
                                                        </option>
                                                        <option value="Current"
                                                            {{ $status == 'Current' ? ' selected="selected"' : '' }}>Current
                                                        </option>
                                                        <option value="Upcoming"
                                                            {{ $status == 'Upcoming' ? ' selected="selected"' : '' }}>
                                                            Upcoming</option>
                                                        <option value="Pending"
                                                            {{ $status == 'Pending' ? ' selected="selected"' : '' }}>Pending
                                                        </option>
                                                        <option value="Completed"
                                                            {{ $status == 'Completed' ? ' selected="selected"' : '' }}>
                                                            Completed</option>
                                                        <option value="Expired"
                                                            {{ $status == 'Expired' ? ' selected="selected"' : '' }}>Expired
                                                        </option>
                                                    </select>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if (Session::has('message'))
                            <div class="alert alert-success text-center" role="alert" id="alert">
                                <span id="messages">{{ Session::get('message') }}</span>
                            </div>
                        @endif
                        @forelse($bookings as $booking)
                            <?php
                            $bookingStatus = $booking->status;
                            if ($booking->created_at < $yesterday && $booking->status != 'Accepted') {
                                $bookingStatus = 'Expired';
                            } elseif ($booking->status == 'Pending' && $booking->payment_method_id == 4) {
                                $bookingStatus = 'Processing';
                            }
                            ?>

                            <div class="row border border p-2 rounded-3 mt-4 custom-shadow">
                                <div class="col-xl-4 col-lg-4 col-md-3 col-sm-12 p-2">
                                    <div class="img-event cust-booking-img">
                                        <a href="{{ url('/') }}/properties/{{ $booking->properties->slug }}">
                                            <img class="room-image-container200 rounded"
                                                src="{{ $booking->properties->cover_photo }}" alt="cover_photo">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-5 col-md-6 col-sm-8 pl-2 pd-right-rtl">
                                    <div class="d-flex justify-content-between align-items-center h-100">
                                        <div class="cust-booking-content text-right-rtl">
                                            <a href="{{ url('/') }}/properties/{{ $booking->properties->slug }}">
                                                <p
                                                    class="mb-1 text-18 text-color font-weight-700 text-color-hover pr-2 mb-3">
                                                    {{ $booking->properties->name }} </p>
                                            </a>
                                            <p class="text-14 mb-3">
                                                <i class="fas fa-map-marker-alt"></i>
                                                {{ $booking->properties->property_address->address_line_1 }}
                                            </p>
                                            <p class="text-14 mt-3">
                                                <i class="fas fa-calendar"></i>
                                                {{ date(' M d, Y', strtotime($booking->start_date)) }} -
                                                {{ date(' M d, Y', strtotime($booking->end_date)) }}
                                            </p>
                                            <p class="text-14 mt-3">
                                                @if ($booking->status == 'Accepted')
                                                    <span>
                                                        <a
                                                            href="{{ url('/') }}/booking/receipt?code={{ $booking->code }}">
                                                            <i class="fas fa-receipt"></i>
                                                            {{ trans('messages.trips_active.view_receipt') }}
                                                        </a>
                                                    </span>
                                                @elseif($booking->status == 'Processing' && $booking->payment_method_id != 4)
                                                    <span>
                                                        <a class="btn vbtn-outline-success text-14 font-weight-700 pl-4 pr-4 pt-3 pb-3 mt-4"
                                                            href="{{ url('/') }}/booking_payment/{{ $booking->id }}">
                                                            <i class="fab fa-cc-amazon-pay"></i> Make
                                                            {{ trans('messages.payment.payment') }}
                                                        </a>
                                                    </span>
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="col-xl-2 col-lg-3 col-md-3 col-2 col-sm-4 col-12 border-left bd-l-remove bd-right-rtl">
                                    <div class="custom-booking-user">
                                        <div class="custom-booking-user-inner">
                                            <div class='img-round'>
                                                <a href="{{ url('/') }}/users/show/{{ $booking->host_id }}">
                                                    <img src="{{ $booking->host->profile_src }}"
                                                        alt="{{ $booking->host->first_name }}"
                                                        class="rounded-circle img-70x70">
                                                </a>
                                            </div>
                                            <div class="custom-booking-user-detail">
                                                <p class="text-center font-weight-700">
                                                    <a href="{{ url('/') }}/users/show/{{ $booking->host_id }}"
                                                        class="text-color text-color-hover">
                                                        {{ $booking->host->first_name }}
                                                    </a>
                                                </p>
                                                <span
                                                    class="badge vbadge-success text-13 mt-3 p-2 {{ $bookingStatus }}">{{ $bookingStatus }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <div class="row jutify-content-center position-center w-100 p-4 mt-4 ">
                                <div class="text-center w-100">
                                    <img src="{{ url('public/img/unnamed.png') }}" alt="notfound" class="img-fluid">
                                    <p class="text-center"> {{ trans('messages.message.empty_tripts') }} </p>
                                </div>
                            </div>
                        @endforelse

                        <div class="row justify-content-between overflow-auto pb-3 mt-4 mb-5">
                            {{ $bookings->appends(request()->except('page'))->links('paginate') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@push('scripts')
    <script type="text/javascript">
        $(document).on('change', '#trip_select', function() {

            $("#my-trip-form").trigger("submit");

        });
    </script>
@endpush
