@extends('template')
@section('main')
<div class="">
    {{-- {{dd($msg)}} --}}
    <div class="row m-0">
        {{-- sidebar start --}}
        @include('users.sidebar')
        {{-- sidebar end --}}
        @if($users->isEmpty())
        <div class="col-lg-10">

            <style>
                .cust-no-msg {
                    width: 100%;
                    height: 100%;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    font-weight: 700;
                    color: #222325;
                }
            </style>
            <span class="text-24 cut-date cust-no-msg">Messages not Exist</span>

        </div>
        @else
        <section class="cust-inbox-main">
            <div class="container-fluid py-5">
                <div class="row mt-5">
                    <div class="col-md-6 col-lg-5 col-xl-4 mb-4 mb-md-0">
                        <h5 class="font-weight-bold mb-3 text-center text-lg-start cust-inbox-head">Member</h5>



                        <sidebar style="width: 100% !important;" class="cust-sidebar">
                            <div class="list-wrap overflow-hidden-x">

                                <?php foreach( $users as $user){ ?>
                                <a href="{{ url('/') }}/window/{{$user->host_id}}?propertyid={{$user->property_id}}">
                                    <div class="list p-2 conversassion cust-sidebar-list" data-id="{{$user->host_id}}">

                                        @if(isset($user->profile_src))
                                        <img src="{{$user->profile_src}}" alt="user" />
                                        @else
                                        <img src="{{asset('public/images/default-profile.png')}}" alt="" />

                                        @endif


                                        <div class="info cust-sidebar-list-detail text-right-rtl">
                                            <h3 class="font-weight-700 ">{{$user->name}}
                                                {{-- <h3 class="font-weight-700 ">{{$user->customer_id}} --}}
                                                    {{-- {{Auth::id()}} --}}
                                                    {{-- <span
                                                        class="text-muted text-12 cut-date">{{$user->created_at}}</span>
                                                    --}}
                                                </h3>


                                                <div class="d-flex justify-content-between">
                                                    @foreach( $messages as $msg)
                                                    <div>
                                                        <p class="text-muted text-14 mb-1 text pr-4">{{$msg->name}}</p>
                                                        {{-- @if($msg->receiver_id == Auth::id())

                                                        <p class="text-14 m-0  {{ $msg->read == 0 ? 'text-success font-weight-bold' : ''}} "
                                                            id="msg-{{ $msg->properties->host_id}}"><i
                                                                class="far fa-comment-alt"></i>{{
                                                            str_limit($msg->message,
                                                            20)}}
                                                        </p>
                                                        @else

                                                        <p class="text-14 m-0"><i class="far fa-comment-alt"></i> {{
                                                            str_limit($msg->message, 20)}} </p>
                                                        @endif --}}
                                                    </div>
                                                    @endforeach
                                                </div>
                                        </div>
                                    </div>
                                </a>
                                <?php	} ?>

                            </div>
                        </sidebar>
                    </div>
                    {{-- {{dd($msg)}} --}}

                    <?php if(isset($msg)){ 
                          //  dd(get_defined_vars()['__data']) ?>
                    <div class="col-md-6 col-lg-7 col-xl-8">
                        <ul class="list-unstyled cust-chat-box">
                            <div class="content-inbox container-fluid p-0" id="{{$msg->properties->host_id}}">

                                @if(isset($firstmsg))
                                @if(auth()->id()==$firstmsg->sender_id)
                                <header>
                                    <a href="{{ url('/') }}/users/show/<?= $msg->properties->host_id ?>">
                                        <img src="{{$msg->properties->users->profile_src}}" alt="img" class="img-40x40">
                                    </a>

                                    <div class="info">
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                <span class="user">{{$msg->properties->users->first_name}}</span>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="open cus-open">
                                        <i class="fas fa-inbox"></i>
                                        <a href="javascript:;">UP</a>
                                    </div>
                                </header>
                                @else
                                <header>
                                    <a href="{{ url('/') }}/users/show/<?= $firstmsg->sender_id ?>">
                                        <img src="{{$firstmsgsenderinfo->profile_src}}" alt="img" class="img-40x40">
                                    </a>

                                    <div class="info">
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                <span class="user">{{$firstmsgsenderinfo->first_name}}</span>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="open cus-open">
                                        <i class="fas fa-inbox"></i>
                                        <a href="javascript:;">UP</a>
                                    </div>
                                </header>
                                @endif
                                @endif


                                <div class="message-wrap">
                                    @foreach ($conversation as $con)
                                    <div
                                        class="{{ $con->receiver_id == Auth::id() ? 'message-list' : 'message-list me' }} message-list">
                                        <div class="msg pl-2 pr-2 pb-2 pt-2 mb-2">
                                            <p class="m-0">{{ $con->message }}</p>
                                        </div>
                                        <div class="time">{{ $con->created_at->diffForHumans() }}</div>
                                    </div>
                                    @endforeach
                                    <div class="message-list me">
                                        <div class="msg_txt mb-0"></div>
                                        <div class="time msg_time mt-0"></div>
                                    </div>
                                </div>
                                @if(isset($firstmsg))
                                <div class="message-footer">
                                    {{-- {{dd($id)}} --}}
                                    <input type="text" class="cht_msg" data-placeholder="Send a message to {0}" />
                                    @if(auth()->id()==$id)
                                    {{-- {{dd($id)}} --}}
                                    <a href="javascript:void(0)" class="btn btn-success chat text-18 send-btn"
                                        data-booking="<?= Auth::id() ?>" data-receiver="<?= $firstmsg->sender_id ?>"
                                        data-property="{{ $properties->id ?? ''}}"><i class="fa fa-paper-plane"
                                            aria-hidden="true"></i></a>
                                    @else
                                    {{-- {{dd($id)}} --}}
                                    <a href="javascript:void(0)" class="btn btn-success chat text-18 send-btn"
                                        data-booking="<?= Auth::id() ?>" data-receiver="<?= $id ?>"
                                        data-property="{{ $properties->id ?? ''}}"><i class="fa fa-paper-plane"
                                            aria-hidden="true"></i></a>
                                    @endif
                                </div>
                                @endif
                            </div>
                        </ul>

                    </div>
                    <?php	} 
else if(!empty($id)) { ?>

                    <div class="col-md-6 col-lg-7 col-xl-8">
                        <ul class="list-unstyled">
                            <div class="content-inbox container-fluid p-0" id="<?= $id ?>">


                                <header>
                                    <a href="{{ url('/') }}/users/show/<?= $id ?>">
                                        <img src="{{auth()->user()->profile_src}}" alt="img" class="img-40x40">
                                    </a>

                                    <div class="info">
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                @foreach ($man as $m)
                                                <span class="user">{{$m->first_name}}</span>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="open">
                                        <i class="fas fa-inbox"></i>
                                        <a href="javascript:;">UP</a>
                                    </div>
                                </header>

                                <div class="message-wrap">

                                    <div class="message-list">


                                    </div>

                                    <div class="message-list me">
                                        <div class="msg_txt mb-0"></div>
                                        <div class="time msg_time mt-0"></div>
                                    </div>
                                </div>
                                @if(isset($firstmsg))
                                <div class="message-footer">
                                    <input type="text" class="cht_msg" data-placeholder="Send a message to {0}" />
                                    @if(auth()->id()==$id)
                                    {{-- {{dd($id)}} --}}
                                    <a href="javascript:void(0)" class="btn btn-success chat text-18 send-btn"
                                        data-booking="<?= Auth::id() ?>" data-receiver="<?= $firstmsg->sender_id ?>"
                                        data-property="{{ $properties->id ?? ''}}"><i class="fa fa-paper-plane"
                                            aria-hidden="true"></i></a>
                                    @else
                                    {{-- {{dd($id)}} --}}
                                    <a href="javascript:void(0)" class="btn btn-success chat text-18 send-btn"
                                        data-booking="<?= Auth::id() ?>" data-receiver="<?= $id ?>"
                                        data-property="{{ $properties->id ?? ''}}"><i class="fa fa-paper-plane"
                                            aria-hidden="true"></i></a>
                                    @endif
                                </div>
                                @endif
                            </div>
                        </ul>

                    </div>
                    <?php } else { ?>
                    <div class="col-md-6 col-lg-7 col-xl-8">
                        <ul class="list-unstyled">
                            <div class="content-inbox container-fluid p-0" id="<?= Auth::id() ?>">

                                <header>
                                    <a href="{{ url('/') }}/users/show/<?= Auth::id() ?>">
                                        <img src="{{auth()->user()->profile_src}}" alt="img" class="img-40x40">
                                    </a>

                                    <div class="info">
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                @foreach($man as $m)
                                                <span class="user">{{$m->first_name}}</span>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="open">
                                        <i class="fas fa-inbox"></i>
                                        <a href="javascript:;">UP</a>
                                    </div>
                                </header>

                                <div class="message-wrap">

                                    <div class="message-list">
                                        <div class="msg pl-2 pr-2 pb-2 pt-2 mb-2">

                                        </div>

                                    </div>

                                    <div class="message-list me">
                                        <div class="msg_txt mb-0"></div>
                                        <div class="time msg_time mt-0"></div>
                                    </div>
                                </div>

                                <div class="message-footer">
                                    <input type="text" class="cht_msg" data-placeholder="Send a message to {0}" />
                                    @if(auth()->id()==$id)
                                    <a href="javascript:void(0)" class="btn btn-success chat text-18 send-btn"
                                        data-booking="<?= Auth::id() ?>" data-receiver="<?= $lastmsg->sender_id ?>"
                                        data-property="{{ $properties->id ?? ''}}"><i class="fa fa-paper-plane"
                                            aria-hidden="true"></i></a>
                                    @else
                                    <a href="javascript:void(0)" class="btn btn-success chat text-18 send-btn"
                                        data-booking="<?= Auth::id() ?>" data-receiver="<?= $id ?>"
                                        data-property="{{ $properties->id ?? ''}}"><i class="fa fa-paper-plane"
                                            aria-hidden="true"></i></a>
                                    @endif

                                </div>
                            </div>
                        </ul>

                    </div>
                    <?php } ?>
                </div>

            </div>
        </section>

        @endif

    </div>
</div>
@stop
@push('scripts')
<script type="text/javascript">
    const ls = localStorage.getItem("selected");
        let selected = false;
        var list = document.querySelectorAll(".list"),
            content = document.querySelector(".content-inbox"),
            input = document.querySelector(".message-footer input"),
            open = document.querySelector(".open a");
        //process
        function process() {
            if (ls != null) {
                selected = true;
                click(list[ls], ls);
            }
            if (!selected) {
                click(list[0], 0);
            }

            list.forEach((l, i) => {
                l.addEventListener("click", function() {
                    click(l, i);
                });
            });

            try {
                document.querySelector(".list.active").scrollIntoView(false);
            } catch {}

        }
        process();

        //list click
        function click(l, index) {
            list.forEach(x => {
                x.classList.remove("active");
            });
            if (l) {
                l.classList.add("active");
                document.querySelector("sidebar").classList.remove("opened");
                open.innerText = "UP";
                document.querySelector(".message-wrap").scrollTop = document.querySelector(".message-wrap").scrollHeight;
                localStorage.setItem("selected", index);
            }
        }

        open.addEventListener("click", (e) => {
            const sidebar = document.querySelector("sidebar");
            sidebar.classList.toggle("opened");
            if (sidebar.classList.value == 'opened')
                e.target.innerText = "DOWN";
            else
                e.target.innerText = "UP";
        });

        $(document).on('click', '.conversassion', function() {
            var id = $(this).data('id');
            var dataURL = APP_URL + '/messaging/booking';
            $.ajax({
                url: dataURL,
                data: {
                    "_token": "{{ csrf_token() }}",
                    'id': id,
                },
                type: 'post',
                dataType: 'json',
                success: function(data) {
                    $('#msg-' + id).removeClass('text-success');
                    $('#messages').empty().html(data['inbox']);
                    $('#booking').empty().html(data['booking']);
                }
            })
        });

        $(document).on('click', '.chat', function(e) {
            // e.preventDefault();
            var customer_id = $(this).data('customer');
            // alert(customer_id);
            var msg = $('.cht_msg').val();
            var booking_id = $(this).data('booking');
            var receiver_id = $(this).data('receiver');
            var property_id = $(this).data('property');

            var result = '<div class="msg pl-2 pr-2 pb-2 pt-2 mb-2">' +
                '<p class="m-0">' + sanitize(msg) + '</p>' +
                '</div>' +
                '<div class="time">just now</div>'

            var dataURL = APP_URL + '/messaging/reply';
            $.ajax({
                url: dataURL,
                data: {
                    "_token": "{{ csrf_token() }}",
                    'msg': msg,
                    'booking_id': booking_id,
                    'receiver_id': receiver_id,
                    'property_id': property_id,
                },
                type: 'post',
                dataType: 'json',
                success: function(data) {
                    $('.msg_txt').append(result);

                    $('.cht_msg').val("");
                }
            })
        });

        $(".cht_msg").on('keyup', function(event) {
            if (event.which === 13) {
                $('.chat').trigger("click");
            }
        });

        function sanitize(string) {
            const symbols = {
                '&': '&amp;',
                '<': '&lt;',
                '>': '&gt;',
                '"': '&quot;',
                "'": '&#x27;',
                "/": '&#x2F;',
            };
            const regex = /[&<>"'/]/ig;
            return string.replace(regex, (match) => (symbols[match]));
        }

        function myFunction() {
            alert("run");
        }
</script>
@endpush