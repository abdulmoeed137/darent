<nav class="navbar navbar-expand-lg navbar-light list-bacground border rounded-3 p-3 dp-none">
	<ul class="list-inline">
		<li class="list-inline-item p-2">
			<a class="text-color {{ (request()->is('users/profile')) ? 'secondary-text-color font-weight-700' : '' }} text-color-hover" href="{{ url('users/profile') }}">
				{{trans('messages.sidenav.edit_profile')}}
			</a>
		</li>

		<li class="list-inline-item p-2">
			<a class="text-color {{ (request()->is('users/profile/media')) ? 'secondary-text-color font-weight-700' : '' }} text-color-hover" href="{{ url('users/profile/media') }}">
				{{trans('messages.sidenav.photo')}}
			</a>
		</li>

		<li class="list-inline-item p-2">
			<a class="text-color {{ (request()->is('users/edit-verification')) ? 'secondary-text-color font-weight-700' : '' }} text-color-hover" href="{{ url('users/edit-verification') }}">
				{{trans('messages.sidenav.verification')}}
			</a>
		</li>

		<li class="list-inline-item p-2">
			<a class="text-color {{ (request()->is('users/security')) ? 'secondary-text-color font-weight-700' : '' }}   text-color-hover" href="{{ url('users/security') }}">
				{{trans('messages.account_sidenav.security')}}  
			</a>
		</li>
	</ul>
</nav>

<div class="custom-dropdown">
	<div class="selectBtn" data-type="firstOption">
		Edit Profile
	</div>
	<ul class="selectDropdown">
		<li class="option" data-type="firstOption">
			<a class="text-color {{ (request()->is('users/profile')) ? '' : '' }} text-color-hover" href="{{ url('users/profile') }}#firstOption">
				{{trans('messages.sidenav.edit_profile')}}
			</a>
		</li>
		<li class="option" data-type="secondOption ">
			<a class="text-color {{ (request()->is('users/profile/media')) ? '' : '' }} text-color-hover" href="{{ url('users/profile/media') }}#secondOption">
				{{trans('messages.sidenav.photo')}}
			</a>
		</li>
		<li class="option" data-type="thirdOption">
			<a class="text-color {{ (request()->is('users/edit-verification')) ? '' : '' }} text-color-hover" href="{{ url('users/edit-verification') }}#thirdOption">
				{{trans('messages.sidenav.verification')}}
			</a>
		</li>
		<li class="option" data-type="fourthOption">
			<a class="text-color {{ (request()->is('users/security')) ? '' : '' }}   text-color-hover" href="{{ url('users/security') }}#fourthOption">
				{{trans('messages.account_sidenav.security')}}  
			</a>
		</li>
	</ul>
</div>
<script>
	    const select = document.querySelectorAll('.selectBtn');
		const option = document.querySelectorAll('.option');
		let index = 1;
		
		window.addEventListener('load', b => {
			let hash = location.hash;
			if(hash){
				hash = hash.substring(1);
				let option = document.querySelector('li[data-type="'+hash+'"]');
				if(option){
				const parent = option.closest('.custom-dropdown').children[0];
				parent.setAttribute('data-type', option.getAttribute('data-type'));
				parent.innerText = option.children[0].textContent.replaceAll("\t", "").replaceAll("\n", "");
				}
			}
		});

		select.forEach(a => {
			a.addEventListener('click', b => {
				const next = b.target.nextElementSibling;
				next.classList.toggle('toggle');
				next.style.zIndex = index++;
			})
		})
		option.forEach(a => {
			a.addEventListener('click', b => {
				b.target.parentElement.classList.remove('toggle');
				
				const parent = b.target.closest('.custom-dropdown').children[0];
				parent.setAttribute('data-type', b.target.getAttribute('data-type'));
				parent.innerText = b.target.innerText;
			})
		})
</script>