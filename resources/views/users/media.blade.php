@extends('template')

@section('main')

<div class="">
    <div class="row m-0">
        <!-- sidebar start-->
        @include('users.sidebar')
        <!--sidebar end-->
        <div class="col-lg-10 p-0">
            <div class="container-fluid min-height">
                <div class="col-md-12 mt-5">
                    <div class="main-panel">
                        @include('users.profile_nav')

                        <!--Success Message -->
                        @if(Session::has('message'))
                            <div class="row pl-5 pr-5 mt-5">
                                <div class="col-md-12  alert {{ Session::get('alert-class') }} alert-dismissable fade in top-message-text opacity-1">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{ Session::get('message') }}
                                </div>
                            </div>
                        @endif

                        <div class="row mt-5 border pt-4 pb-4 rounded">
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 text-center">
                                @if($result->profile_image)
                                    <img width="100%" height="100%" class="cust-image-size" title="{{ Auth::user()->first_name }}" src="{{  url('public/images/profile').'/'.Auth::user()->id.'/'.$result->profile_image }}" alt="{{ $result->first_name }}">
                                @else
                                    <img width="100%" height="100%" class="cust-image-size" title="{{ Auth::user()->first_name }}" src="{{  \Auth::user()->profile_src }}" alt="{{ $result->first_name }}">
                                @endif

                            </div>
                            <div class="col-xl-9 col-lg-9 col-md-9 align-self-center">
                                <p class="text-16 mt-2 pd-sm-box text-right-rtl">{{trans('messages.users_media.photo_data')}}</p>
                                <form name="ajax_upload" method="post" id="ajax_upload" enctype="multipart/form-data" action="{{ url('/') }}/users/profile/media" accept-charset="UTF-8" >
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-6 col-sm-9 pd-sm-box p-0 mb-xs">
                                            <input type="file" name="photos[]" id="profile_image" class="form-control">
                                        </div>
                                        <div class="col-md-3 col-sm-3 pd-sm-box">
                                            <input type="submit" class="btn form-control vbtn-outline-success" value="upload">
                                        </div>
                                    </div>

                                    <iframe class="d-none" name="upload_frame" id="upload_frame"></iframe>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script src="{{ url('public/js/jquery.validate.min.js') }}"></script>
<script src="{{ url('public/js/additional-method.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#ajax_upload').validate({
            rules: {
                'photos[]': {
                    accept: "image/jpg,image/jpeg,image/png,image/gif"
                }
            },
            messages: {
            'photos[]': {
                    accept: "{{ __('messages.jquery_validation.image_accept') }}",
                    }
            },
            errorElement : 'div',
            errorLabelContainer: '.errorTxt_p'
        });
    });
</script>
@endpush
@stop
