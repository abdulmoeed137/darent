@extends('template')

@section('main')
<div style="padding-top: 100px">
    {{-- <form action="{{route('hyperpay-callback')}}" class="paymentWidgets" data-brands="VISA MASTER AMEX"></form> --}}
</div>
    
    
    {{-- @push('scripts')
    <script src="https://eu-test.oppwa.com/v1/paymentWidgets.js?checkoutId={{{$responseData->id}}}"></script>
    @endpush --}}

    @push('scripts')
    <script src="https://eu-test.oppwa.com/v1/paymentWidgets.js?checkoutId={{{$responseData->id}}}"></script>
    @endpush

<form name='' action='{{$responseData->redirect->url}}' method='POST'>
    @foreach ($responseData->redirect->parameters as $para)
        
    <INPUT type='text' name='{{$para->name}}' value='{{$para->value}}'>
    @endforeach
</form>
<script>
    window.onload = submitForm;
    function submitForm() { downloadForm.submit(); }
</script>

@stop