@extends('template')

@section('main')
<div class="container-fluid container-fluid-90 margin-top-85 min-height">
	@if(Session::has('message'))
		<div class="row mt-5">
			<div class="col-md-12 text-13 alert mb-0 {{ Session::get('alert-class') }} alert-dismissable fade in  text-center opacity-1">
				<a href="#"  class="close " data-dismiss="alert" aria-label="close">&times;</a>
				{{ Session::get('message') }}
			</div>
		</div>
	@endif
<div class="payment-form">
@php
	if(isset($_GET['debug'])){
		dd($responseData);
	}
@endphp

	<div class="row justify-content-center">
		<div class="col-md-8 mb-5 mt-3 main-panel p-5 border rounded">
			<form action="{{ url('payments/create_booking') }}" method="post" id="checkout-form">
				{{ csrf_field() }}
				<div class="row justify-content-center">
				<input name="property_id" type="hidden" value="{{ $property_id }}">
				<input name="checkin" type="hidden" value="{{ $checkin }}">
				<input name="checkout" type="hidden" value="{{ $checkout }}">
				<input name="number_of_guests" type="hidden" value="{{ $number_of_guests }}">
				<input name="nights" type="hidden" value="{{ $nights }}">
				<input name="currency" type="hidden" value="{{ $result->property_price->code }}">
				<input name="booking_id" type="hidden" value="{{ $booking_id }}">
				<input name="booking_type" type="hidden" value="{{ $booking_type }}">
				<input name="paymentid_hyperpay" type="hidden" value="{{ $responseData->id }}">

				@if($status == "" && $booking_type == "request")
					<div class="h2 pb-4 m-0 text-24">{{ trans('messages.listing_book.request_message') }}</div>
				@endif
				@if($booking_type == "instant"|| $status == "Processing" )
				{{-- <div class="col-lg-12 col-12">
					<div class="form-group">
					  <label for="exampleInputEmail1">{{ trans('messages.payment.country') }}</label>
					   <span class="text-danger">*</span>
					<select name="payment_country" id="country-select" data-saving="basics1" class="form-control mb20">
						@foreach($country as $key => $value)
						<option value="{{ $key }}" {{ ($key == $default_country) ? 'selected' : '' }}>{{ $value }}</option>
						@endforeach
					</select>
					</div>
				</div> --}}
				{{-- <div class="col-lg-12 col-12">
					<div class="form-group">
					  <label for="exampleInputEmail1">Payment Gateway</label>
					   <span class="text-danger">*</span>
					   <select name="payment_method" id="payment_method" class="form-control mb20">
						   <option value="hyperpay">Hyper Pay</option>
						<option value="myfatoorah">My Fatoorah</option>
					</select>

					</div>
				</div> --}}
				{{-- <div id="myfatoorah_cardfields" class="row"> --}}
					{{-- <div class="col-lg-6 col-12">
						<div class="form-group">
						<label>Card number</label>
						<span class="text-danger">*</span>
						<input type="number" name="cardNumber" placeholder="Card number" class="form-control hdnspin mb20" maxlength="16" required/>
					    </div>
					</div>
					<div class="col-lg-6 col-12">
						<div class="form-group">
							<label>Expiry Month</label>
						<span class="text-danger">*</span>
						<input type="number" name="ExpiryMonth" placeholder="Expiry Month" class="form-control mb20" required/>
					   </div>
					</div>
					<div class="col-lg-6 col-12">
						<div class="form-group">
							<label for="cvv">CVV</label>
						<span class="text-danger">*</span>
						<input type="number" name="cvv" placeholder="CVV" class="form-control mb20" required/>
					</div>
					</div>
					<div class="col-lg-6 col-12">
						<div class="form-group">
							<label>Expiry Year</label>
						<span class="text-danger">*</span>
						<input type="number" name="ExpiryYear"  placeholder="Expiry Year" class="form-control mb20" required/>
					</div>
					</div> --}}
				{{-- </div> --}}


				@endif
				
				@if($status == "" && $booking_type == "request")

					<div class="col-lg-12 col-12 mt-3">
						<div class="form-group">
							<textarea name="message_to_host" placeholder="{{ trans('messages.trips_active.type_message') }}" class="form-control mb20" rows="7" required></textarea>
						</div>
					</div>

					<div class="col-lg-12 text-right mt-3">
						<button id="payment-form-submit" type="submit" class="btn vbtn-outline-success text-16 font-weight-700 pl-5 pr-5 pt-3 pb-3">
							<i class="spinner fa fa-spinner fa-spin d-none"></i>
							{{ ($booking_type == 'instant') ? trans('messages.listing_book.book_now') : trans('messages.property.continue') }}
						</button>
					</div>
				@endif
				</div>
			</form>
			@if ($booking_type == 'instant' || ($status == "Processing" && $booking_type == "request"))
			<div id="hyperpay_cardfields">
				<form action="{{ route('hyperpay-callback') }}" id='hyperpay_form' class="paymentWidgets" data-brands="VISA MASTER AMEX MADA"></form>
			</div>		
			@endif
		</div>
		<div class="col-md-4  mt-3 mb-5">
				<div class="card p-3">
					<a href="{{ url('/') }}/properties/{{ $result->slug}}">
						<img class="card-img-top p-2 rounded" src="{{ $result->cover_photo }}" alt="{{ $result->name }}" height="180px">
					</a>

					<div class="card-body p-2">
						<a href="{{ url('/') }}/properties/{{ $result->slug}}">
							<p class="text-16 font-weight-700 mb-0">{{ $result->name }}</p>
						</a>

						<p class="text-14 mt-2 text-muted mb-0">
							<i class="fas fa-map-marker-alt"></i>
							{{$result->property_address->address_line_1}}, {{ $result->property_address->state }}, {{ $result->property_address->country_name }}
						</p>
						<div class="border p-4 mt-4 text-center rounded-3">
							<p class="text-16 mb-0">
								<strong class="font-weight-700 secondary-text-color">{{ $result->property_type_name }}</strong>
								{{trans('messages.payment.for')}}
								<strong class="font-weight-700 secondary-text-color">{{ $number_of_guests }} {{trans('messages.payment.guest')}}</strong>
							</p>
							<div class="text-16"><strong>{{ date('D, M d, Y', strtotime($checkin)) }}</strong> to <strong>{{ date('D, M d, Y', strtotime($checkout)) }}</strong></div>
						</div>

						<div class="border p-4 rounded-3 mt-4">

							@foreach( $price_list->date_with_price as $date_price)
							<div class="d-flex justify-content-between text-16">
								<div>
									<p class="pl-4">{{ $date_price->date }}</p>
								</div>
								<div>
									<p class="pr-4">{!! $date_price->price !!}</p>
								</div>
							</div>
							@endforeach
							<hr>
							<div class="d-flex justify-content-between text-16">
								<div>
									<p class="pl-4">{{trans('messages.payment.night')}}</p>
								</div>
								<div>
									<p class="pr-4">{{ $nights }}</p>
								</div>
							</div>

							<div class="d-flex justify-content-between text-16">
								<div>
									<p class="pl-4">{!! $price_list->per_night_price_with_symbol !!} x {{ $nights }} {{trans('messages.payment.nights')}}</p>
								</div>
								<div>
									<p class="pr-4">{!! $price_list->total_night_price_with_symbol !!}</p>
								</div>
							</div>

							@if($price_list->service_fee)
								<div class="d-flex justify-content-between text-16">
									<div>
										<p class="pl-4">{{trans('messages.payment.service_fee')}}</p>
									</div>

									<div>
										<p class="pr-4">{!! $price_list->service_fee_with_symbol !!}</p>
									</div>
								</div>
							@endif

							@if($price_list->additional_guest)
								<div class="d-flex justify-content-between text-16">
									<div>
										<p class="pl-4">{{trans('messages.payment.additional_guest_fee')}}</p>
									</div>

									<div>
										<p class="pr-4">{!! $price_list->additional_guest_fee_with_symbol !!}</p>
									</div>
								</div>
							@endif

							@if($price_list->security_fee)
								<div class="d-flex justify-content-between text-16">
									<div>
										<p class="pl-4">{{trans('messages.payment.security_deposit')}}</p>
									</div>

									<div>
										<p class="pr-4">{!! $price_list->security_fee_with_symbol !!}</p>
									</div>
								</div>
							@endif

							@if($price_list->cleaning_fee)
								<div class="d-flex justify-content-between text-16">
									<div>
										<p class="pl-4">{{trans('messages.payment.cleaning_fee')}}</p>
									</div>

									<div>
										<p class="pr-4">{!! $price_list->cleaning_fee_with_symbol !!}</p>
									</div>
								</div>
							@endif

							@if($price_list->iva_tax)
								<div class="d-flex justify-content-between text-16">
									<div>
										<p class="pl-4">{{trans('messages.property_single.iva_tax')}}</p>
									</div>

									<div>
										<p class="pr-4">{!! $price_list->iva_tax_with_symbol !!}</p>
									</div>
								</div>
							@endif

							@if($price_list->accomodation_tax)
								<div class="d-flex justify-content-between text-16">
									<div>
										<p class="pl-4">{{trans('messages.property_single.accommodatiton_tax')}}</p>
									</div>

									<div>
										<p class="pr-4">{!! $price_list->accomodation_tax_with_symbol !!}</p>
									</div>
								</div>
							@endif
							<hr>

							<div class="d-flex justify-content-between font-weight-700">
								<div>
									<p class="pl-4">{{trans('messages.payment.total')}}</p>
								</div>

								<div>
									<p class="pr-4">{!! $price_list->total_with_symbol !!}</p>
								</div>
							</div>
						</div>
					</div>
					<div class="card-body">
						<p class="exfont text-16">
							{{trans('messages.payment.paying_in')}}
							<strong><span id="payment-currency">{!! moneyFormat($currencyDefault->org_symbol,$currencyDefault->code) !!}</span></strong>.
							{{trans('messages.payment.your_total_charge')}}
							<strong><span id="payment-total-charge">{!! moneyFormat($currencyDefault->org_symbol, $price_eur) !!}</span></strong>.
							{{trans('messages.payment.exchange_rate_booking')}} {!! moneyFormat($currentCurrency->symbol, 1) !!} {!! $currentCurrency->code !!} to {!! moneyFormat($result->property_price->currency->org_symbol, $price_rate ) !!} {{ $result->property_price->currency_code }} ( {{trans('messages.listing_book.host_currency')}} ).
						</p>
					</div>
				</div>


		</div>
	</div>
</div>
</div>
@push('scripts')
<script src="{{$apikey}}v1/paymentWidgets.js?checkoutId={{$responseData->id}}"></script>


<script type="text/javascript" src="{{ url('public/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript">

// $('#myfatoorah_cardfields').addClass('display-off');
// $("#myfatoorah_cardfields input").prop("disabled", true);


// $('#hyperpay_cardfields').addClass('display-off');

let bookingtype = '{{$booking_type}}';
let bookingstatus = '{{$status}}';

console.log('BTYPE - |'+bookingtype, 'STATUS - |'+bookingstatus);
// if(bookingtype && bookingstatus){

// 	if(bookingtype == 'request' && bookingstatus == 'Pending'){
// 		$('#hyperpay_cardfields').removeClass('display-off');
	
// 	}else{
// 		$('#payment-form-submit').addClass('display-off');
// 	}

// }



// $('#payment_method').on('change', function(){
// 		var method = $(this).val();
// 		console.log(method);
		

// 	if(method == 'myfatoorah'){
// 		$('#myfatoorah_cardfields').removeClass('display-off');
//     	$('#hyperpay_cardfields').addClass('display-off');
//     	$('#payment-form-submit').removeClass('display-off');

// 		$("#myfatoorah_cardfields input").prop("disabled", false);
// 	}

// 	else {
// 		$('#hyperpay_cardfields').removeClass('display-off');
//     	$('#myfatoorah_cardfields').addClass('display-off');
//     	$('#payment-form-submit').addClass('display-off');

// 		$("#myfatoorah_cardfields input").prop("disabled", true);
//   	}

// });


$(document).ready(function() {
	setTimeout(function() {

		// $('.wpwl-button-pay').text('Book Now');
		$('.wpwl-button-pay').addClass('btn vbtn-outline-success text-16 font-weight-700 pl-5 pr-5 pt-3 pb-3');
		$('.wpwl-form').removeClass('wpwl-form-card');

		$('.wpwl-form').addClass('form-cust');
		$('.wpwl-control').addClass('form-control');
		$('.wpwl-control').removeClass('wpwl-control');
		$('.wpwl-label-brand').addClass('form-cust-label-brand');
		$('.wpwl-wrapper-brand').addClass('form-cust-brand');
		$('.wpwl-brand-card').addClass('wpwl-brand-card');
		$('.wpwl-label').addClass('form-cust-label');
		$('.wpwl-group').addClass('form-cust-group');
		$('.wpwl-group-cardNumber,.wpwl-group-expiry,.wpwl-group-cardHolder,.wpwl-group-cvv').addClass('form-cust-w');
		$('iframe').addClass("cust-form-border");

    }, 500);

	// $(document).on("click",".wpwl-button-pay", function(e) {
	// 	e.preventDefault();
	// 	// console.log('test event click');
	// 	var data = {
	// 		'property_id' : $("input[name=property_id]").val(),
	// 		'checkin' : $("input[name=checkin]").val(),
	// 		'checkout' : $("input[name=checkout]").val(),
	// 		'number_of_guests' : $("input[name=number_of_guests]").val(),
	// 		'nights' : $("input[name=nights]").val(),
	// 		'booking_id' : $("input[name=booking_id]").val(),
	// 		'booking_type' : $("input[name=booking_type]").val(),
	// 		'paymentid_hyperpay' : $("input[name=paymentid_hyperpay]").val()
	// 	};
	// 	// console.log(data);
	// 	$.ajax({
	// 		url: "{{route('data_store')}}",
	// 		data: data,
	// 		cache: false,
	// 		type: 'GET',
	// 		success: function (response) {
	// 			if(response){
	// 				$('.wpwl-form').submit();
	// 			}
	// 		},
	// 		error:function (response){
	// 			console.log(response);

    //     	}
        
    // 	});

	// });


	// $(document).on('submit','form.wpwl-form',function(){

	// 	var data = {
	// 		'property_id' : $("input[name=property_id]").val(),
	// 		'checkin' : $("input[name=checkin]").val(),
	// 		'checkout' : $("input[name=checkout]").val(),
	// 		'number_of_guests' : $("input[name=number_of_guests]").val(),
	// 		'nights' : $("input[name=nights]").val(),
	// 		'booking_id' : $("input[name=booking_id]").val(),
	// 		'booking_type' : $("input[name=booking_type]").val(),
	// 		'paymentid_hyperpay' : $("input[name=paymentid_hyperpay]").val()
	// 	};
	// 	// console.log(data);
	// 	$.ajax({
	// 		url: "{{route('data_store')}}",
	// 		data: data,
	// 		cache: false,
	// 		type: 'GET',
	// 		success: function (response) {
	// 			if(response){
	// 				console.log('response' + response)
	// 				// $('form.wpwl-form').submit();
	// 			}
	// 		},
	// 		error:function (response){
	// 			console.log(response);

    //     	}
		
	// });

	// $('input').keypress(function (e) {
	// if (e.which == 13) {
	// 	ajaxstore();
	// }
	// });

    // $('#checkout-form').validate({
    //     submitHandler: function(form)
    //     {
 	// 		$("#payment-form-submit").on("click", function (e)
    //         {
    //         	$("#payment-form-submit").attr("disabled", true);
    //             e.preventDefault();
    //         });


    //         $(".spinner").removeClass('d-none');
    //         $("#save_btn-text").text("{{trans('messages.users_profile.save')}} ..");
    //         return true;
    //     }
    // });
});


$('#country-select').on('change', function() {
  var country = $(this).find('option:selected').text();
  $('#country-name-set').html(country);
})
</script>
@endpush
@stop
