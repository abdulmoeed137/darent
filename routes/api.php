<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix'=>'v1','middleware' => ['locale']],function(){
    Route::post('userregister','Api\LoginController@register');
    Route::post('login','Api\LoginController@login');
    Route::get('search','Api\SearchController@index');
	Route::post('search/result', 'Api\SearchController@searchResult');
	Route::match(array('GET', 'POST'),'properties/{slug}', 'Api\PropertyController@single')->name('property.single');
    Route::get('manualmsg','Api\InboxController@MuanualMessage')->name('sendmsg');
});
// Auth::routes();
Route::group(['prefix'=>'v1','middleware' => ['auth:api', 'json.response','locale']],function(){
    Route::get('dashboard', 'Api\UserController@dashboard')->name('dashboard');
    Route::get('user/favourite', 'Api\PropertyController@userBookmark');
    Route::match(['get', 'post'], 'my-bookings', 'Api\BookingController@myBookings');
    Route::match(['get', 'post'], 'trips/active', 'Api\TripsController@myTrips');
    Route::post('users/profile', 'Api\UserController@profile');
    Route::match(array('GET', 'POST'),'users/profile/media', 'Api\UserController@media');
    Route::match(['get', 'post'], 'users/security', 'Api\UserController@security');

    //Reviews
    Route::match(array('GET', 'POST'),'users/reviews', 'Api\UserController@reviews');
	Route::match(array('GET', 'POST'),'users/reviews_by_you', 'Api\UserController@reviewsByYou');


    // User verification
    Route::get('users/edit-verification', 'Api\UserController@verification');
    Route::get('users/confirm_email/{code?}', 'Api\UserController@confirmEmail');
    Route::get('users/new_email_confirm', 'Api\UserController@newConfirmEmail');

    //user Properties
    Route::match(array('GET', 'POST'),'properties', 'Api\PropertyController@userProperties');

    //Create Property
    Route::get('create', 'Api\PropertyController@create');
    Route::post('store', 'Api\PropertyController@store');
    Route::match(array('GET', 'POST'),'listing/{id}/{step}', 'Api\PropertyController@listing')->where(['id' => '[0-9]+','page' => 'basics|description|location|amenities|photos|pricing|calendar|details|booking']);


    //inbox of User Dashboard
    Route::get('inbox', 'Api\InboxController@index');

    Route::match(['get', 'post'], 'window/{id}', 'Api\InboxController@window');
    Route::post('messaging/booking/', 'Api\InboxController@message');
    Route::post('messaging/reply/', 'Api\InboxController@messageReply');

    //Add-Edit-Bookmark
    Route::post('add-edit-book-mark', 'Api\PropertyController@addEditBookMark');


    //Contact Us
    Route::post('emails/contact_us', 'Api\EmailController@contactUs');

    //Logout
    Route::get('logout','Api\LoginController@logout');

});
